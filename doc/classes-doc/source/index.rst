.. tomo documentation master file, created by
   sphinx-quickstart on Tue Jul 23 10:52:50 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Full Field and Half Field Tomography 
====================================

ID19 Optics Classes
-------------------

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
Tomo Optic Base Class
---------------------
.. automodule:: TomoOptic
    :members:
   
TwinMic
-------
.. automodule:: TwinOptic
    :members:
    
TripleMic
---------
.. automodule:: TripleOptic
    :members:
    
OptiquePeter
------------
.. automodule:: PeterOptic
    :members:
    
Hasselblad
----------
.. automodule:: HasselbladOptic
    :members:
    
Standard Optics
---------------
.. automodule:: StandardOptic
    :members:
    
User Defined
------------
.. automodule:: UserOptic
    :members:

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
