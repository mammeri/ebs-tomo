import sys
import gevent
import numpy as np
import time

import PyTango
import bliss

from bliss import setup_globals, global_map
from bliss.common.scans import ascan
from bliss.common.logtools import log_info,log_debug
from bliss.common import session
from bliss.scanning.acquisition.motor import MotorMaster, SoftwarePositionTriggerMaster, SweepMotorMaster, LinearStepTriggerMaster
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster, RoiProfileAcquisitionSlave
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.channel import AcquisitionChannel
from bliss.scanning.scan import Scan

import tomo
from tomo.TomoShutter import ShutterType
from tomo.Tomo import Tomo, ScanType


class TomoScan:
    """
    Class for tomo scan object.
    The class implements methods to handle scans in tomo acquisition.
    Defines acquisition chain corresponding to each tomo scan type: 
        - step scan
        - continuous scan synchronized by musst
        - continuous scan synchronized by software
        - continuous scan without gap (sweep scan)
        
    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    in_pars : dict
        calculated scan parameters
    watchdog_trig_difference : int
        difference tolerated between triggers sent and images acquired.
    """
    
    def __init__(self, tomo, *args, **kwargs):
        # init logging
        self.name = tomo.name+".tomoscan"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")
        self.tomo = tomo
        self.in_pars = {}
        self.watchdog_trig_difference = 1000
        self.last_motor_pos = None
        log_info(self,"__init__() leaving")

    def calculate_parameters(self, start_pos, end_pos, nb_points, exposure_time, latency_time=0):
        """
        Calculates scan parameters:
        - scan_step_size : angle in degrees between each image
        - scan_point_time : time in seconds between each image 
         (exposure time + readout time + shutter time + latency time)
        - scan_speed : rotation speed in degrees per seconds
        - integration_ratio : ratio between exposure time and scan point time
        """
        log_info(self,"calculate_parameters() entering")
        
        self.tomo.tomo_ccd.calculate_parameters(exposure_time)
        
        acc_expo_time = self.tomo.tomo_ccd.in_pars['acc_expo_time']
        acc_nb_frames = self.tomo.tomo_ccd.in_pars['acc_nb_frames']
        readout_time = self.tomo.tomo_ccd.parameters.readout_time
        extra_time = self.tomo.tomo_ccd.parameters.extra_time
        if acc_nb_frames == 1: 
            extra_time = 0.0

        shutter_time = 0.0
        if self.tomo.shutter.shutter_used == ShutterType.CCD:
            shutter_time = self.tomo.shutter.fast_shutter_closing_time()

        step_time = (acc_expo_time + readout_time + extra_time + shutter_time) * acc_nb_frames + latency_time
        integ_ratio = self.tomo.parameters.exposure_time / step_time
        
        if self.tomo.parameters.scan_type == ScanType.SWEEP:
            step_time = exposure_time
            integ_ratio = 1

        self.in_pars['scan_step_size'] = float(end_pos - start_pos) / nb_points
        self.in_pars['scan_point_time'] = step_time
        self.in_pars['scan_speed'] = abs(self.in_pars['scan_step_size']/self.in_pars['scan_point_time'])
        self.in_pars['integration_ratio'] = integ_ratio
                                                                   
                                                                
    def step_scan(self, start_pos, end_pos, nb_points, exposure_time,
                      title, scan_info={}, save=True, run=False):
        """
        StepMotorMaster moves rotation axis step by step, i.e stops at each projection angle to trigger 
        SoftTriggerMusstAcquisitionMaster (musst device) and LimaAcquisitionMaster (detector device) 
        and waits until the end of image acquisition before going to next projection angle.
            
        SoftTriggerMusstAcquisitionMaster runs the RUNCT musst command.
            
        LimaAcquisitionMaster waits the external musst triggers and takes one image at each trigger.
        """
        
        # add one more trigger to have the same positioning as for the continuous scans,
        # but one image more!
        nb_points = nb_points + 1
        
        # create the acquisition chain
        chain = AcquisitionChain()
        
        # Motor master 
        motor_master = LinearStepTriggerMaster (nb_points, self.tomo.rotation_axis, start_pos, end_pos)
        
        timer_master = SoftwareTimerMaster(count_time=exposure_time, npoints=nb_points, sleep_time=0)
        chain.add(motor_master, timer_master)
        
        # prepare MUSST
        # acquisition device equivalent to musst.ct(exposure_time)
        musst_master = self.tomo.tomo_musst.prepare_soft(exposure_time, nb_points)
        chain.add(motor_master, musst_master)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = nb_points
        
        # prepare Lima
        acq_params = {'acq_nb_frames'       :   nb_points,
                      'acq_trigger_mode'    :   'EXTERNAL_TRIGGER_MULTI',
                      'acq_expo_time'       :   exposure_time,
                      'prepare_once'        :   True,
                      'start_once'          :   True,
                      'wait_frame_id'       :   range(nb_points)
        }

        if self.tomo.tomo_ccd.detector.acquisition.mode == 'ACCUMULATION':
            acq_params.update({'acq_mode'           :   'ACCUMULATION'})
        else:
            if self.tomo.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo.tomo_ccd.detector.camera.cam_name.lower():
                acq_params.update({'acq_mode'           :   'ACCUMULATION',
                                   'acc_max_expo_time'  :   0.04})
  
        # prepare LIMA
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        chain.add(musst_master,lima_acq)
        
        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_master, chain, self.tomo.tomo_ccd.detector.name, 
                                            exposure_time, nb_points, timer=timer_master)

        
        # create the scan object
        step_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=f'axis:{self.tomo.rotation_axis.name}',
                                                              motors= self.tomo.scan_motors,
                                                              limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.tomo.tomo_musst.card,
                                                             trigger_name=f'axis:{self.tomo.rotation_axis.name}',
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference),     
        )
                                                                
        print (step_scan.acq_chain._tree)
        if run == True:
            step_scan.run()
            
        return step_scan
    
    def continuous_scan(self, start_pos, end_pos, nb_points, exposure_time, latency_time, scan_step_size, scan_point_time, 
                        trigger_type, title, scan_info={}, save=True, run=False):
        """
        MotorMaster moves rotation axis in one motion from start position to end position at constant speed (related to scan point time). 
            
        MusstAcquisitionSlave runs 'ftomo_acc' program and receives encoder and timer data.
        Musst triggers are synchronized by rotation axis encoder.
        Triggers can be equally spaced in time or position.
            
        LimaAcquisitionMaster waits the external musst triggers and takes one image at each trigger. 
        """
        
        # create the acquisition chain
        chain = AcquisitionChain()
        
        acc_margin = self.acc_margin
        
        if self.use_step_size:
            # undershoot start margin is  added to be sure motor will reach scan speed   
            acc_margin = abs(scan_step_size)

        # time regulates motor speed, corresponds to time needed by the motor to go from start position to end position  
        motor_master = MotorMaster(self.tomo.rotation_axis, start_pos, end_pos, time=scan_point_time*nb_points, 
                                   undershoot_start_margin = acc_margin, undershoot_end_margin = acc_margin)

        scan_time = self.tomo.tomo_tools.continuous_scan_time(self.tomo.rotation_axis, start_pos, end_pos)
        
        # prepare MUSST
        accumulation = self.tomo.tomo_ccd.detector.accumulation.nb_frames
        if accumulation < 0:
            accumulation = 1
        musst_master, musst_slave = self.tomo.tomo_musst.prepare_continuous(start_pos, exposure_time, latency_time, nb_points, 
                                                                            scan_step_size, scan_point_time, scan_time, accumulation, trigger_type)
        
        chain.add(motor_master, musst_master)
        chain.add(musst_master, musst_slave)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = nb_points
        
        acq_params = {'acq_nb_frames'       :   nb_points,
                      'acq_trigger_mode'    :   'EXTERNAL_TRIGGER_MULTI',
                      'acq_expo_time'       :   exposure_time,
        }

        if self.tomo.tomo_ccd.detector.acquisition.mode == 'ACCUMULATION':
            acq_params.update({'acq_mode'           :   'ACCUMULATION'})
        else:
            if self.tomo.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo.tomo_ccd.detector.camera.cam_name.lower():
                acq_params.update({'acq_mode'           :   'ACCUMULATION',
                                   'acc_max_expo_time'  :   0.04})
  
        # prepare LIMA
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        
        if self.tomo.parameters.sinogram_active:
            
            for roi_profile in self.tomo.tomo_ccd.detector.roi_profiles.counters:
                #prepare roi spectrums acquisition
                spectrum_acq = RoiProfileAcquisitionSlave(roi_profile,
                                                          count_time = exposure_time,
                                                          npoints = nb_points)
                chain.add(lima_acq,spectrum_acq)
            
        chain.add(musst_master,lima_acq)
        
        # calculation devices are used to convert musst data (encoder + timer) in appropriate units (degree and seconds)
        calc_musst = self.tomo.tomo_musst.prepare_calculation(musst_slave,data_per_line=int(nb_points))
        calc_musst_timer = calc_musst[0]
        calc_musst_pos = calc_musst[1]
        for calc in calc_musst:
            chain.add(musst_master,calc)

        # Add data channel to motor master 
        motor_master.add_external_channel(calc_musst_pos, calc_musst_pos.channels[0].short_name,
                                          rename=self.tomo.rotation_axis.name,
                                          dtype=np.float)


        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_master, chain, self.tomo.tomo_ccd.detector.name, exposure_time, nb_points)

        # create the scan object
        cont_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=calc_musst_timer.channels[0].name,
                                                              motors= self.tomo.scan_motors,
                                                              limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.tomo.tomo_musst.card,
                                                             trigger_name=calc_musst_timer.channels[0].name,
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference),
        )
                                                                
        print (chain._tree)
        if run == True:
            cont_scan.run()
            
        return cont_scan
    
    
    def soft_scan(self, start_pos, end_pos, nb_points, exposure_time, scan_step_size, scan_point_time, 
                  title, scan_info={}, save=True, run=False):
        """
        SoftwarePositionTriggerMaster moves rotation axis from start position to end position at constant speed (related to scan point time)
        and triggers by soft, musst and detector devices. Triggers are equally spaced in time. 
            
        SoftTriggerMusstAcquisitionMaster runs the RUNCT musst command.
            
        LimaAcquisitionMaster waits the external musst triggers and takes one image at each trigger. 
        """
        
        # create the acquisition chain
        chain = AcquisitionChain()
        
        acc_margin = self.acc_margin
        
        if self.use_step_size:
            # undershoot start margin is  added to be sure motor will reach scan speed   
            acc_margin = abs(scan_step_size)
        
        # time regulates motor speed, corresponds to time needed by the motor to go from start position to end position for each reference group  
        # self.tomo.parameters.ref_on indicates the required number of soft triggers 
        motor_master = SoftwarePositionTriggerMaster(self.tomo.rotation_axis, start_pos, end_pos, nb_points, time=scan_point_time*nb_points, 
                                                     undershoot_start_margin = acc_margin, undershoot_end_margin = acc_margin)
        
        timer_master = SoftwareTimerMaster(count_time=0, npoints=nb_points, sleep_time=0,
                                           name="fast_timer")
        chain.add(motor_master, timer_master)
        
        scan_time = self.tomo.tomo_tools.continuous_scan_time(self.tomo.rotation_axis, start_pos, end_pos)
        
        # prepare MUSST
        # acquisition device equivalent to musst.ct(exposure_time)
        musst_master = self.tomo.tomo_musst.prepare_soft(exposure_time, nb_points, scan_time)
        chain.add(motor_master, musst_master)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = nb_points
        
        acq_params = {'acq_nb_frames'       :   nb_points,
                      'acq_trigger_mode'    :   'EXTERNAL_TRIGGER_MULTI',
                      'acq_expo_time'       :   exposure_time,
        }

        if self.tomo.tomo_ccd.detector.acquisition.mode == 'ACCUMULATION':
            acq_params.update({'acq_mode'           :   'ACCUMULATION'})
        else:
            if self.tomo.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo.tomo_ccd.detector.camera.cam_name.lower():
                acq_params.update({'acq_mode'           :   'ACCUMULATION',
                                   'acc_max_expo_time'  :   0.04})
  
        # prepare LIMA
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        chain.add(musst_master,lima_acq)
        
        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_master, chain, self.tomo.tomo_ccd.detector.name, exposure_time, nb_points)
        
        # create the scan object
        soft_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=f'axis:{self.tomo.rotation_axis.name}',
                                                              motors= self.tomo.scan_motors,
                                                              limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.tomo.tomo_musst.card,
                                                             trigger_name=f'axis:{self.tomo.rotation_axis.name}',
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference),
        )
                                                                
        print (soft_scan.acq_chain._tree)
        if run == True:
            soft_scan.run()
            
        return soft_scan
    
    
    
    def sweep_scan(self, start_pos, end_pos, nb_points, exposure_time, scan_step_size, scan_point_time, 
                        title, scan_info={}, save=True, run=False):
        """
        SweepMotorMaster moves rotation axis from start position to end position in several motions. One motion corresponds to one image acquisition.
            
        MusstAcquisitionSlave runs 'ftomo_wo_gap' program and receives encoder and timer data.
        Musst gates are synchronized by rotation axis encoder.
        Gates are equally spaced in position.
            
        LimaAcquisitionMaster waits the external musst gates and acquires one image per gate. 
        """
        
        # create the acquisition chain
        chain = AcquisitionChain()
        
        acc_margin = self.acc_margin
        
        if self.use_step_size:
            # undershoot start margin is  added to be sure motor will reach scan speed   
            acc_margin = abs(scan_step_size)
            
        npoints = nb_points
        time = exposure_time
        data_per_point = 2
        
        # npoints corresponds to number of sweep movements required for each reference group
        # time regulates motor speed, corresponds to time needed by the motor to do one sweep move 
        motor_master = SweepMotorMaster(self.tomo.rotation_axis, start_pos, end_pos, npoints = npoints, 
                                        time = time,
                                        undershoot_start_margin = acc_margin, undershoot_end_margin = acc_margin)
        
        # get full calculate undershoot
        undershoot = motor_master._undershoot
        
        # With the Hr tomo stage rotation!!!!!!!!!!!!!
        # At very low speed undershoot automatically calculated by motor master device is to small for the motor to reach scan speed 
        
        #self.tomo.scan_par['undershoot'] = motor_master._undershoot
        #min_undershoot_steps = 60
        #if motor_master._undershoot*self.tomo.rotation_axis.steps_per_unit < min_undershoot_steps:
            #self.tomo.scan_par['undershoot'] = 0.0075
            #motor_master = SweepMotorMaster(self.tomo.rotation_axis, start, self.tomo.parameters.end_pos, 
                                            #npoints = self.tomo.parameters.ref_on, 
                                            #time=self.tomo.scan_par['scan_point_time'], undershoot = 0.0075)
        
        scan_time = self.tomo.tomo_tools.sweep_scan_time(self.tomo.rotation_axis, start_pos, end_pos, npoints)
        
        # prepare MUSST
        musst_master, musst_slave = self.tomo.tomo_musst.prepare_sweep(start_pos, nb_points, scan_step_size, undershoot, scan_time)
        chain.add(motor_master, musst_master)
        chain.add(musst_master, musst_slave)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = nb_points
        
        acq_params = {'acq_nb_frames'       :   nb_points,
                      'acq_trigger_mode'    :   'EXTERNAL_GATE',
                      'acq_expo_time'       :   exposure_time,
        }
        
        # prepare Lima
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        chain.add(musst_master,lima_acq)
        
        # calculation devices are used to convert musst data (encoder + timer) in appropriate units (degree and seconds)
        calc_musst = self.tomo.tomo_musst.prepare_calculation(musst_slave,data_per_point=data_per_point)
        calc_musst_timer = calc_musst[0]
        calc_musst_pos = calc_musst[1]
        for calc in calc_musst:
            chain.add(musst_master,calc)
        
        motor_master.add_external_channel(calc_musst_pos, calc_musst_pos.channels[0].short_name,
                                          rename=self.tomo.rotation_axis.name,
                                          dtype=np.float)
        
        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_master, chain, self.tomo.tomo_ccd.detector.name, exposure_time, nb_points)
        
        # create the scan object
        sweep_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=calc_musst_timer.channels[0].name,
                                                              motors= self.tomo.scan_motors,
                                                              limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.tomo.tomo_musst.card,
                                                             trigger_name=calc_musst_timer.channels[0].name,
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference),
        )
                                                                
        print (sweep_scan.acq_chain._tree)
        if run == True:
            sweep_scan.run()
            
        return sweep_scan
        
        
        
