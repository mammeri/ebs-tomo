__all__=["CScanMusstChanCalc", "_step_per_unit"]

import numpy
import gevent
import sys
import string
import datetime
from bliss.scanning.chain import AcquisitionChannel
from bliss.controllers.motor import CalcController
from bliss import setup_globals


from tomo.Tomo import ScanType

_step_per_unit = lambda mot: mot.encoder.steps_per_unit if mot.encoder else mot.steps_per_unit 

class MusstConvDataCalc(object):
    """ 
    Class to convert musst data channels into:
        - user position for encoder
        - seconds from timer
        
    ***Attributes***
    name : str
        The Bliss object name
    musst_card : Bliss musst object
        musst controller
    source_name : str
        name of the incoming data channel
    data_per_point : int
        number of data associated to one trigger
    data_per_line : int
        number of data associated to one scan
    factor : int
        used to convert data to the desired unit
    dest_name : str
        name of the outcoming data channel
    raw_data : numpy array
        array containing raw data coming from musst acquisition device
    data : numpy array
        array containing raw data currently treated
    max_idx : int
        ending index array value used to remove duplicated data for topotomo sequence
    index : int
        starting index array value used to filter raw_data 
    overflow : int
        used to handle overflow on musst channel value
    last_data : float
        variable to store last received musst data
    """
    def __init__(self, name, musst_card, source_name, data_per_point, data_per_line, factor, dest_name):
        self.name = name
        self._musst_card = musst_card
        self._source_name = source_name
        self._data_per_point = data_per_point
        self._data_per_line = data_per_line
        self._factor = factor
        self._dest_name = dest_name
        self._raw_data = numpy.array([],dtype=numpy.int32)
        self._data = numpy.array([],dtype=numpy.int32)
        self._max_idx = self._data_per_line
        self._index = 0
        self._overflow = 0
        self._last_data = None
    
    def __call__(self, sender, data_dict):
        """
        Function called at each new data coming from source channel.
        Removes duplicated data.
        Handles data overflow. 
        Converts raw data into user data. 
        """
        data = data_dict.get(self._source_name, None)
        
        if data is None:
            return {}

        self._raw_data = numpy.append(self._raw_data,data)
        
        if self._data_per_line is not None:
            if len(self._raw_data) > self._max_idx:
                idx_to_rm = [idx for idx in range(self._max_idx, len(self._raw_data), self._data_per_line+1)]
                self._raw_data = numpy.delete(self._raw_data, idx_to_rm)
                self._max_idx += len(idx_to_rm)*self._data_per_line
        
        self._data =  self._raw_data[self._index::self._data_per_point]
        
        if self._last_data is None:
           self._last_data = self._data[0] 
        
        raw_data = numpy.append(self._last_data,self._data)
        abs_data = raw_data.astype(numpy.float)
        
        if len(self._raw_data) >= 2:
            over_idxs = numpy.where(numpy.diff(abs_data) != numpy.diff(raw_data))[0]
            abs_data += self._overflow * 2**32
            for idx in over_idxs:
                ovr_sign = raw_data[idx] > 0 and 1. or -1.
                abs_data[idx+1:] += ovr_sign * 2**32
                self._overflow += ovr_sign
        
        self._last_data = raw_data[-1]
        
        calc_data = abs_data[1:] / self._factor

        self._index += len(calc_data)*self._data_per_point
        
        return {f'{self._musst_card.name}:{self._dest_name}' : calc_data}


    @property
    def acquisition_channels(self):
        """
        Returns AcquisitionChannel associated to calculation device
        """
        return [AcquisitionChannel(f'{self._musst_card.name}:{self._dest_name}',numpy.float,())]
    
    
class TomoTools():
    """
    Class for tomo tools object.
    The class implements methods to verify consistency of tomo scan parameters 
    and estimate tomo acquisition duration.
    
    ***Attributes***
    tomo_ccd : Tomo ccd object
        contains all methods to handle detectors in tomo acquisition
    parameters : Tomo object parameters
        parameters associated to Tomo object
    reference : Tomo reference object
        contains all methods to handle reference in tomo acquisition
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    """
    
    def __init__(self, tomo):
        self.tomo_ccd = tomo.tomo_ccd 
        self.parameters = tomo.parameters
        self.reference = tomo.reference
        self.tomo = tomo
    
    def check_params(self,musst):
        """
        Checks if scan parameters are consistant.
        Checks for dimax camera that memory is sufficient for number of tomo projections asked by user.
        """
                
        if self.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo_ccd.detector.camera.cam_name.lower():
            self.tomo_ccd.detector.proxy.image_roi = [self.tomo_ccd.detector.image.roi.x,self.tomo_ccd.detector.image.roi.y,self.tomo_ccd.detector.image.roi.width,self.tomo_ccd.detector.image.roi.height]
            self.tomo_ccd.detector.prepareAcq()
            self.tomo_ccd.detector.startAcq()
            self.tomo_ccd.detector.stopAcq()
            max_img = self.tomo_ccd.detector.camera.max_nb_images
            if max_img > 0 and self.parameters.tomo_n > max_img:
                raise Exception(f'Error, you cannot record more than {max_img} with this camera!')
            if round(self.tomo_ccd.detector.acquisition.expo_time) > 0.04:
                raise Exception('Error, exposure time must be < 40 ms')
            if round(self.tomo_ccd.detector.accumulation.max_expo_time) > 0.04:
                raise Exception('Error, maximum accumulated exposure time must be < 40 ms')
        
        # get the minimum latency time for the tomo scan
        min_latency_time = self.calculate_latency_time(musst)
        
        if self.parameters.scan_type == ScanType.CONTINUOUS_SOFT and self.parameters.latency_time < min_latency_time:
            self.parameters.latency_time = min_latency_time
        # if the given latency time is equal to 0, set it to the minimum calculated value
        elif self.parameters.latency_time == 0:
            self.parameters.latency_time = min_latency_time
        print(f'Latency time set to {self.parameters.latency_time} seconds')     
            
        # Print the camera acquisition mode: SINGLE or ACCUMULATION
        print(f"The actual camera acquisition mode is set to: {self.tomo_ccd.detector.proxy.acq_mode}")


        print("<<<Parameters are consistant !>>>")

    def calculate_latency_time(self,musst):
        """
        Calculates and returns the minimum latency time for the tomo scan configuration.
        """
        latency_time = 0
        
        if musst.enc_channel is not None and musst.mot_chan is not None:
            latency_time = 0.002
            if 'elmo' in musst.motor.controller.get_class_name().lower():
                readout_time = self.parameters.tomo_n*self.parameters.exposure_time/60
                if readout_time >= 5.0:
                    latency_time = 0.005
        
        # for soft scan, latency time needs to be >= 0.05 otherwise motor master is not able 
        # to send soft position triggers at the right time
        if self.parameters.scan_type == ScanType.CONTINUOUS_SOFT:
            latency_time = 0.05
        
        return latency_time
        

    def estimate_ref_scan_duration(self):
        """
        Estimates reference scan duration.
        """    
        # ref motor
        ref_mot = self.reference.ref_motors[0]
        if isinstance(ref_mot.controller, CalcController):
            unit_time = 0.0
        else:
            init_pos = self.reference.parameters.in_beam_position[0]
            target_pos = init_pos+self.reference.parameters.out_of_beam_displacement[0]
            # back and forth
            unit_time = 2 * self.mot_disp_time(ref_mot,target_pos-init_pos,ref_mot.velocity)
            unit_time += 2 * self.reference.settle_time

        motor_time = unit_time * (self.tomo.in_pars['nb_groups'] - 1)
        ref_img = self.parameters.ref_n * (self.tomo.in_pars['nb_groups'] - 1)
            
        if self.parameters.ref_images_at_start:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
            
        if self.parameters.ref_images_at_end:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
                
        image_time = ref_img * self.tomo.tomo_scan.in_pars['scan_point_time']
            
        return motor_time + image_time


    def estimate_dark_scan_duration(self):
        """
        Estimates dark scan duration. 
        """    
        dark_img = 0
        
        if self.parameters.dark_images_at_start:
            dark_img += self.parameters.dark_n
            
        if self.parameters.dark_images_at_end:
            dark_img += self.parameters.dark_n
        
        image_time = dark_img * self.tomo.tomo_scan.in_pars['scan_point_time']
            
        return image_time
    
    def estimate_return_scan_duration(self,motor):
        """
        Estimates return scan duration. 
        """    
        
        scan_range = self.parameters.end_pos - self.parameters.start_pos
        
        if self.parameters.return_images:
            if not self.parameters.return_images_aligned_to_refs:
                return_img = int(scan_range/90)+1
                nr_moves = int(scan_range/90)
                one_move_disp = 90
            else:
                return_img = self.tomo.in_pars['nb_groups']+1
                nr_moves = self.tomo.in_pars['nb_groups']
                step_size = scan_range/self.parameters.tomo_n 
                one_move_disp = step_size*self.parameters.ref_on
        else:
            acc_margin = self.tomo.tomo_scan.acc_margin    
            if self.tomo.tomo_scan.use_step_size:
                acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
            undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
            return_img = 0
            nr_moves = 1 
            one_move_disp = scan_range + undershoot + acc_margin

        
        one_move_time = self.mot_disp_time(motor,one_move_disp,motor.velocity)
        motor_time = (nr_moves-1)*one_move_time
        
        # if last reference group needs less projections, motor displacement will be reduced 
        if self.parameters.return_images and self.parameters.return_images_aligned_to_refs:
            if self.parameters.reference_groups and self.parameters.tomo_n % self.parameters.ref_on != 0:
                one_move_disp = abs(self.parameters.end_pos-self.tomo.in_pars['start_group_pos'][-1])
                one_move_time = self.mot_disp_time(motor,one_move_disp,motor.velocity)
        
        motor_time += one_move_time
        
        image_time = return_img * self.tomo.tomo_scan.in_pars['scan_point_time']
        
        return motor_time + image_time

    def step_scan_time(self,motor,start_pos,nb_points):
        """
        Estimates step scan duration. 
        """
        # one_move_time represents one step
        one_move_disp = self.tomo.tomo_scan.in_pars['scan_step_size']
        one_move_time = self.mot_disp_time(motor,one_move_disp,motor.velocity)
        scan_time = one_move_time*nb_points

        image_time = (nb_points+1) * self.tomo.tomo_scan.in_pars['scan_point_time']
        
        return scan_time + image_time
        
    def sweep_scan_time(self,motor,start_pos,end_pos,nb_points):
        """
        Estimates sweep scan duration. 
        """
        tomo_end = end_pos
        nb_points = nb_points * abs(self.tomo.tomo_ccd.detector.accumulation.nb_frames)
        tomo_n = nb_points
        if self.tomo.in_pars['nb_groups'] > 1:
            end_pos = self.tomo.in_pars['start_group_pos'][1]
            nb_points = nb_points / self.tomo.in_pars['nb_groups'] 
        
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size']) / abs(self.tomo.tomo_ccd.detector.accumulation.nb_frames)
            
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = (end_pos - start_pos) / nb_points
        start_disp = cst_disp + acc_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * tomo_n
        first_prepare_disp = undershoot + acc_margin
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        else:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,motor.velocity)
        prepare_disp = acc_disp
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time += self.mot_disp_time(motor,prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * (tomo_n-1)
        else:
            prepare_time += self.mot_disp_time(motor,prepare_disp,motor.velocity) * (tomo_n-1)   
        scan_time = start_time + prepare_time 
        
        # time to return to scan end position before return images acquisition 
        if self.parameters.return_images:
            scan_time += self.mot_disp_time(motor,undershoot+acc_margin,motor.velocity)
            
        return scan_time
        
    def continuous_scan_time(self,motor,start_pos,end_pos):
        """
        Estimates continuous scan duration. 
        """
        end_tomo = end_pos
        if self.tomo.in_pars['nb_groups'] > 1:
            end_pos = self.tomo.in_pars['start_group_pos'][1]
        
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
        
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = end_pos - start_pos
        start_disp = acc_disp + cst_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        prepare_time = self.mot_disp_time(motor,acc_disp/2,motor.velocity)
        
        # one_move_time represents one group
        one_move_time = start_time + prepare_time
        scan_time=one_move_time
        
        if self.tomo.in_pars['nb_groups'] > 1:
            
            prepare_disp = acc_disp
            prepare_time = self.mot_disp_time(motor,prepare_disp,motor.velocity)
            move_time = (start_time+prepare_time) * (self.tomo.in_pars['nb_groups']-1) 

            # if last reference group needs less projections, scan duration will be decreased 
            if self.parameters.tomo_n % self.parameters.ref_on != 0:
                start_pos = self.tomo.in_pars['start_group_pos'][-1]
                end_pos = end_tomo
                cst_disp = end_pos - start_pos
                start_disp = acc_disp + cst_disp
                start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
                prepare_time = self.mot_disp_time(motor,prepare_disp,motor.velocity)
                move_time = start_time + prepare_time
                
            scan_time += move_time
        
        # time to return to scan end position before return images acquisition 
        if self.parameters.return_images:
            scan_time += self.mot_disp_time(motor,undershoot+acc_margin,motor.velocity)
            
        return scan_time

    def mot_disp_time(self,mot,disp,speed):
        """
        Estimates motor displacement time.
        """
        half_disp = abs(disp/2)
        acc_disp = speed**2  / (2 * mot.acceleration)
        if acc_disp <= half_disp:
            return 2 * (speed / mot.acceleration + (half_disp - acc_disp) / speed)
        else:
            max_speed = numpy.sqrt(2 * mot.acceleration * half_disp)
            acc_time = max_speed / mot.acceleration
            return 2 * acc_time
            
class TopoTools(TomoTools):
    """
    Class for topo tools object.
    The class implements methods to verify consistency of topotomo scan parameters 
    and estimate topotomo acquisition duration.
    
    ***Attributes***
    tomo_ccd : Tomo ccd object
        contains all methods to handle detectors in tomo acquisition
    parameters : Tomo object parameters
        parameters associated to Tomo object
    reference : Tomo reference object
        contains all methods to handle reference in tomo acquisition
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    """
    def sweep_scan_time(self,motor,start_pos,end_pos,nb_points):
        """
        Estimates sweep scan duration. 
        """
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
        
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = (end_pos - start_pos) / nb_points
        start_disp = acc_disp + cst_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * nb_points
        
        first_prepare_disp = undershoot + acc_margin
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        else:
            prepare_time = self.mot_disp_time(motor,first_prepare_disp,motor.velocity)
        prepare_disp = acc_disp
        if self.tomo.tomo_scan.in_pars['scan_speed'] > motor.velocity:
            prepare_time += self.mot_disp_time(motor,prepare_disp,self.tomo.tomo_scan.in_pars['scan_speed']) * (nb_points-1)
        else:
            prepare_time += self.mot_disp_time(motor,prepare_disp,motor.velocity) * (nb_points-1)
            
        scan_time = start_time + prepare_time 
        
        return scan_time
        
    def continuous_scan_time(self,motor,start_pos,end_pos):
        """
        Estimates continuous scan duration.  
        """
        acc_margin = self.tomo.tomo_scan.acc_margin    
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars['scan_step_size'])
        
        undershoot = self.tomo.tomo_scan.in_pars['scan_speed']**2 / (2*motor.acceleration)
        acc_disp = 2 * (undershoot + acc_margin)
        cst_disp = end_pos - start_pos
        start_disp = acc_disp + cst_disp
        start_time = self.mot_disp_time(motor,start_disp,self.tomo.tomo_scan.in_pars['scan_speed'])
        prepare_time = self.mot_disp_time(motor,acc_disp,motor.velocity)
        
        one_move_time = start_time + prepare_time
        scan_time=one_move_time
        
        return scan_time
       

        
        
