import sys
import gevent
import numpy as np
import time

import bliss
from bliss import global_map, current_session
from bliss.common import session
from bliss import setup_globals
from bliss.common.logtools import log_info, log_debug


class TomoMetaData:
    """
    Class for tomo metadata object.
    The class implements methods to handle metadata in tomo acquisition.
    
    The metadata will be returned as a nested dictionary.
    For every tomo scan the dictionary gets injected as scan info in the Bliss scan object.
    Metadata can be retrieve in the HDF5 data file under 'technique' section.
    
    ***Attributes***
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)

    """

    def __init__(self, tomo_name, tomo):
        # init logging
        self.name = tomo_name + ".metadata"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")
        self.tomo = tomo

    def tomo_scan_info(self):
        """
        Builds and returns the nested metadata dictionary.
        Metadata are divided into several categories: scan, scan flags (for options), 
        detector, optic, saving, reference and dark. 
        """

        scan_info = {}

        # Basic tomo scan parameters
        scan = {}
        scan["name"] = current_session.scan_saving.dataset.name
        alias_name = "None"
        if setup_globals.ALIASES.get_alias(self.tomo.rotation_axis.name) is not None:
            alias_name = setup_globals.ALIASES.get_alias(self.tomo.rotation_axis.name)
        scan["motor"] = (self.tomo.rotation_axis.name, alias_name)
        scan["scan_type"] = self.tomo.parameters.scan_type.name
        scan["scan_range"] = (
            self.tomo.parameters.end_pos - self.tomo.parameters.start_pos
        )
        scan["tomo_n"] = self.tomo.parameters.tomo_n

        ### for step scan one additional image is taken for each ref group
        ### asked by DAU to adapt tomo_n value for coherency
        if self.tomo.parameters.scan_type.name == "STEP":
            self.tomo.active_tomo.calculate_parameters()
            scan["tomo_n"] += self.tomo.in_pars["nb_groups"]
        ###
        scan["ref_on"] = self.tomo.parameters.ref_on
        scan["exposure_time"] = self.tomo.parameters.exposure_time * 1000.0
        scan["exposure_time@units"] = "ms"
        scan["latency_time"] = self.tomo.parameters.latency_time * 1000.0
        scan["latency_time@units"] = "ms"
        if self.tomo.shutter.sync_shutter:
            scan["shutter_time"] = self.tomo.shutter.sync_shutter_time
        else:
            scan["shutter_time"] = (
                self.tomo.shutter.fast_shutter_closing_time() * 1000.0
            )
        scan["shutter_time@units"] = "ms"
        scan["energy"] = self.tomo.parameters.energy
        scan["energy@units"] = "keV"
        scan["source_sample_distance"] = self.tomo.parameters.source_sample_distance
        scan["source_sample_distance@units"] = "mm"
        scan["sample_detector_distance"] = self.tomo.parameters.sample_detector_distance
        scan["sample_detector_distance@units"] = "mm"
        scan["sequence"] = self.tomo.sequence
        scan["field_of_view"] = self.tomo.field_of_view

        # Missing
        # comment
        scan["comment"] = self.tomo.saving.parameters.comment

        scan_info["scan"] = scan

        # Scan configuration flags
        scan_flags = {}
        scan_flags["half_acquisition"] = self.tomo.parameters.half_acquisition
        scan_flags["dark_images_at_start"] = self.tomo.parameters.dark_images_at_start
        scan_flags["dark_images_at_end"] = self.tomo.parameters.dark_images_at_end
        scan_flags["ref_images_at_start"] = self.tomo.parameters.ref_images_at_start
        scan_flags["ref_images_at_end"] = self.tomo.parameters.ref_images_at_end
        scan_flags["no_reference_groups"] = not self.tomo.parameters.reference_groups
        scan_flags["no_return_images"] = not self.tomo.parameters.return_images
        scan_flags[
            "return_images_aligned_to_refs"
        ] = self.tomo.parameters.return_images_aligned_to_refs
        scan_flags["return_to_start_pos"] = self.tomo.parameters.return_to_start_pos
        scan_info["scan_flags"] = scan_flags

        # Detector image parameters

        # read image parameters
        image_params = self.tomo.tomo_ccd.get_image_parameters()
        scan_info["detector"] = image_params

        # optic parameters
        optic = {}
        optic["name"] = self.tomo.optic.description
        optic["type"] = self.tomo.optic.type
        optic["magnification"] = self.tomo.optic.magnification
        optic["sample_pixel_size"] = self.tomo.optic.image_pixel_size
        optic["sample_pixel_size@units"] = "um"
        optic["scintillator"] = self.tomo.optic.scintillator
        scan_info["optic"] = optic

        # saving parameters
        saving = {}
        saving["path"] = current_session.scan_saving.get_path()
        saving["image_file_format"] = self.tomo.tomo_ccd.detector.saving.file_format
        saving["frames_per_file"] = self.tomo.tomo_ccd.detector.saving.frames_per_file
        scan_info["saving"] = saving

        # dark parameters
        dark = {}
        dark["dark_n"] = self.tomo.parameters.dark_n
        scan_info["dark"] = dark

        # reference displacement

        # get motor names
        names = []
        for i in self.tomo.reference.ref_motors:
            names.append(i.name)

        ref = {}
        ref["ref_n"] = self.tomo.parameters.ref_n
        ref["motors"] = names
        ref["displacement"] = self.tomo.reference.parameters.out_of_beam_displacement
        ref["displacement@units"] = "mm"
        scan_info["reference"] = ref

        # Create the entry in the HDF5 file
        meta_data = {"type2": "tomo", "technique": scan_info}

        return meta_data
