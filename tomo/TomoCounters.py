import time
import logging

from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug

from bliss.scanning.toolbox import ChainBuilder
from bliss.scanning.acquisition.timer import SoftwareTimerMaster
from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.counter import SamplingCounterController, IntegratingCounterController
from bliss.controllers.ct2.client import CT2Controller
from bliss.controllers.ct2.device import AcqMode


class TomoCounters:
    """
    Class for tomo counters object.
    The class implements methods to handle counters in tomo acquisition.
    
    ***Attributes***
    name : string
        class name
    config : dict
        tomo configuration 
    """
    
    def __init__(self, name, config):
        
         # init logging
        self.log_name = name+'.add_counters'
        global_map.register(self, tag=self.log_name)
        log_info(self,"__init__() entering")
        
        self.name = name
        self.config = config
        
        log_info(self,"__init__() leaving")
        
    
    def add_p201_counters(self, master, chain, count_time, npoints):
        """
        Adds p201 counters in fast acquisition chain. 
        They will be triggered by musst.
        """
        # analyse the active measurement group (ACTIVE_MG)
        builder = ChainBuilder([])
        
        # set-up CT2 parameters
        ct2_params = {
            'npoints': npoints,
            'acq_expo_time': count_time,
            'acq_mode': AcqMode.ExtTrigMulti,
            'acq_point_period': None,
            "prepare_once": True,
            "start_once": False,
        }
        
        # add all CT2 counters to the master
        for node in builder.get_nodes_not_ready():
            if isinstance(node.controller, CT2Controller):
                node.set_parameters(acq_params=ct2_params)
                
                # add p201 counters under p201 master
                for child_node in node.children:
                    # p201 cousnters are integration counters
                    child_node.set_parameters(acq_params={'count_time':count_time})
                
                chain.add(master, node)
            
            
    def add_sampling_counters(self, master, chain, count_time, npoints):
        """
        Adds sampling counters in slow acquisition chain. 
        They will be triggered by timer if no specific master is given.
        """
        # analyse the active measurement group (ACTIVE_MG)
        builder = ChainBuilder([])
        
        for node in builder.get_nodes_not_ready():
            if isinstance(node.controller, SamplingCounterController):
                # set-up sampling counter parameters
                sampling_params = {
                    'npoints': 0,
                    'count_time': count_time,
                }
                
                node.set_parameters(acq_params=sampling_params)
                chain.add(master, node)
     
                
    def add_integrating_counters(self, master, chain, count_time, npoints):
        """
        Adds integrating counters in slow acquisition chain. 
        They will be triggered by timer if no specific master is given.
        """
        # analyse the active measurement group (ACTIVE_MG)
        builder = ChainBuilder([])
        
        for node in builder.get_nodes_not_ready():
            if isinstance(node.controller, IntegratingCounterController):
                # set-up integration counter parameters
                integration_params = {
                    'count_time': count_time,
                }
                
                node.set_parameters(acq_params=integration_params)
                chain.add(master, node)
                
    
    def add_slow_lima_counters(self, master, chain, count_time, npoints, fast_detector):
        """
        Adds lima counters in slow acquisition chain. 
        They will be triggered by timer if no specific master is given.
        """
        # analyse the active measurement group (ACTIVE_MG)
        builder = ChainBuilder([])
        
        for node in builder.get_nodes_not_ready():
            if isinstance(node.controller, Lima):
                
                if node.controller.name != fast_detector:
                    
                    # prepare all other Lima devices in the measurement groups as slow, software triggered detectors
                    # set-up Lima parameters
                    lima_acq_params = {
                        "acq_nb_frames": 1,
                        "acq_expo_time": count_time,
                        "acq_mode": "SINGLE",
                        "acq_trigger_mode": "INTERNAL_TRIGGER", 
                        "prepare_once": False,
                        "start_once": False,
                    }
                    
                    node.set_parameters(acq_params=lima_acq_params)
                    chain.add(master, node)
      
      
                    
    # Entry point for tomo framework. This method will be called to add counters!
    def add_counters(fast_master, chain, tomo_detector_name, count_time, npoints, timer=None):
        """
        Adds p201, sampling, integrating and lima counters into tomo acquisition.
        """
        # Add fast counter channels from P201 card
        self.add_p201_counters(fast_master, chain, count_time, npoints)
        
        # use an external timer master or create a slow time master
        slow_master = timer
        if slow_master == None:
            # create slow master (SotwareTimerMaster) with a frequency of 0.5Hz
            slow_master = SoftwareTimerMaster(count_time=2.0, npoints=0, sleep_time=0)
            
        self.add_slow_lima_counters(slow_master, chain, count_time, npoints, tomo_detector_name)
        self.add_sampling_counters(slow_master, chain, count_time, npoints)
        self.add_integrating_counters(slow_master, chain, count_time, npoints)
        
        # return the chain
        return chain
        
