from bliss.scanning.scan import ScanPreset
from bliss.scanning.chain import ChainPreset,ChainIterationPreset
from bliss.controllers.tango_shutter import TangoShutterState
from tomo.TomoShutter import ShutterType
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.common.logtools import log_info, log_debug, log_warning

class DarkShutterPreset(ScanPreset):
    """
    Class used to close shutter (beam or fast shutter) before taking dark images
    and reopen it after acquisition.
    
    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    sh_was_open : boolean
        flag used to check if shutter is opened before closing it 
        in order to reopens it after acquisition if it is the case  
    """
    def __init__(self, tomo_shutter):
        self.shutter = tomo_shutter
        self.shutter.init()
        self.sh_was_open = False
        
    def prepare(self,scan):
        """
        Closes shutter
        """
        self.sh_was_open = False
        
        print(f"\nPreparing shutter for {scan.name}\n")
        print(f"Closing the dark shutter")
        
        if self.shutter.dark_shutter_state in (TangoShutterState.OPEN, TangoShutterState.RUNNING):
            self.sh_was_open = True
            self.shutter.dark_shutter_close()
        
    def start(self,scan):
        pass
        
    def stop(self,scan):
        """
        Opens shutter
        """
        print(f"Opening the dark shutter")
        if self.sh_was_open == True:
            self.shutter.dark_shutter_open()
        
        
class FastShutterPreset(ScanPreset):
    """
    Class used to open fast shutter before taking reference, static or projection images
    and close it after acquisition.
    
    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    """
    def __init__(self, tomo_shutter):
        self.shutter = tomo_shutter
        self.shutter.init()
        
    def prepare(self,scan):
        """
        Opens shutter if shutter controlled by detector
        """
        if self.shutter.shutter_used != ShutterType.NONE:
            if self.shutter.shutter_used == ShutterType.CCD:
                self.shutter.fast_shutter_open()
        
    def start(self,scan):
        """
        Opens shutter if shutter controlled by soft
        """
        if self.shutter.shutter_used == ShutterType.SOFT:
            self.shutter.fast_shutter_open()
        
    def stop(self,scan):
        """
        Closes shutter if shutter controlled by soft
        """
        if self.shutter.shutter_used == ShutterType.SOFT:
            self.shutter.fast_shutter_close()
        

class DefaultChainImageCorrOnOffPreset(ChainPreset):
        
    def prepare(self,acq_chain):
        
        lima_acq_obj = [acq_obj for acq_obj in acq_chain.nodes_list if type(acq_obj) == LimaAcquisitionMaster]
        
        self.image_corr_was_on = {}
        self.detectors = []

        for acq_obj in lima_acq_obj:
            if acq_obj.acq_params["saving_mode"] != "MANUAL":
                self.detectors.append(acq_obj.device)
                self.image_corr_was_on[acq_obj.device] = False
            else:
                if acq_obj.device.processing.use_background_substraction == 'enable_file':
                    log_warning(self,f'background_substraction is activated on {acq_obj.device.name}\n')
                if acq_obj.device.processing.use_flatfield:
                    log_warning(self,f'flatfield is activated on {acq_obj.device.name}\n')
                

    def start(self,acq_chain):
        for detector in self.detectors:
            if detector.processing.use_background_substraction == 'enable_file' or detector.processing.use_flatfield:
                self.image_corr_was_on[detector] = True
                detector.processing.use_background_substraction = 'disable'
                detector.processing.use_flatfield = False
                
            
    def stop(self,acq_chain):
        for detector in self.detectors:
            if self.image_corr_was_on[detector]:
                detector.processing.use_background_substraction = 'enable_file'
                detector.processing.use_flatfield = True  

class DefaultChainFastShutterPreset(ChainPreset):
    """
    Class used for all bliss common scans (ct, ascan, timescan,...) to open fast shutter before taking each image 
    and close it after acquisition.
    
    ***Attributes***
    shutter : Tomo shutter object
        contains methods to control shutters
    """
    def __init__(self, tomo_shutter):
        self.shutter = tomo_shutter
        
    class Iterator(ChainIterationPreset):
        def __init__(self,shutter,iteration_nb):
            self.shutter = shutter
            self.iteration = iteration_nb
            
        def prepare(self):
            """
            Opens shutter if shutter controlled by detector
            """
            if self.shutter.shutter_used != ShutterType.NONE:
                if self.shutter.shutter_used == ShutterType.CCD:
                    self.shutter.fast_shutter_open()
        
        def start(self):
            """
            Opens shutter if shutter controlled by soft
            """
            if self.shutter.shutter_used == ShutterType.SOFT:
                self.shutter.fast_shutter_open()
    
        def stop(self):
            """
            Closes shutter if shutter controlled by soft
            """
            if self.shutter.shutter_used == ShutterType.SOFT:
                self.shutter.fast_shutter_close()
                
    def get_iterator(self,acq_chain):
        """
        Generates fastshutter preset for each acquisition chain iteration. 
        It means at each image for bliss common scans.
        """
        iteration_nb = 0
        while True:
            yield DefaultChainFastShutterPreset.Iterator(self.shutter,iteration_nb)
            iteration_nb += 1


class CommonHeaderPreset(ScanPreset):
    """
    Class used by reference, projection, dark and static images to set image header.  
    
    ***Attributes***
    tomo_ccd : Tomo ccd object
        contains methods to control detector
    header : dict
        contains info that will appear in image header
    """
    def __init__(self, tomo_ccd, header):
        self.tomo_ccd = tomo_ccd
        self.header = header
        
    def prepare(self,scan):
        """
        Resets detector image header if header has been provided.
        Sets detector image header with header provided.
        """
        if not self.header:
            self.tomo_ccd.reset_image_header()
        else:
            self.tomo_ccd.image_header(self.header)
        
    def start(self,scan):
        pass
        
    def stop(self,scan):
        """
        Resets detector image header
        """
        self.tomo_ccd.reset_image_header()


class ReferenceMotorPreset(ScanPreset):
    """
    Class used to move sample out of the beam before taking reference images 
    and move sample back in beam after acquisition.
    
    ***Attributes***
    reference : Tomo reference object
        contains methods to control reference motors
    """
    def __init__(self, reference):
        self.reference= reference
        
    def prepare(self,scan):
        """
        Moves sample out of the beam
        """
        print(f"Move sample out of beam for {scan.name}\n")
        self.reference.move_out()
        print ("moved out")
        
    def start(self,scan):
        pass
        
    def stop(self,scan):
        """
        Moves sample back in beam
        """
        print(f"Move sample back in beam")
        self.reference.move_in()
        print ("moved in")
