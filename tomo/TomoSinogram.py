from bliss.setup_globals import *
from bliss import global_map, current_session
from bliss.common.logtools import log_info
import numpy 
from bliss.common.scans.scan_info import ScanInfoFactory
from bliss.scanning.scan_info import ScanInfo
from bliss.scanning.chain import AcquisitionChannel
import gevent
from bliss.config.streaming import DataStreamReaderStopHandler
from bliss.data.node import get_node
from silx.io.nxdata import save_NXdata

class TomoSinogram:
    """
    Class for tomo sinogram object.
    The class implements methods to handle sinogram in tomo acquisition.
    
    ID19 sinograms corresponds to one image slice taken at each rotation angle.
    
    **Attributes**
    channelmap : dict
        associates an acquisition channel name to a function.
        specify how redis data shall be treated when reemitted (flattened, replicate, ...)
    """
    
    def __init__(self,name):
        
         # init logging
        self.log_name = name+'.add_sinogram'
        global_map.register(self, tag=self.log_name)
        log_info(self,"__init__() entering")
        self.channelmap = {}
        self.dark = None
        self.ref = None
        log_info(self,"__init__() leaving")
        
    def configure_plot(self, scan_info, start_x, stop_x, npoints_x, start_y, stop_y, npoints_y):
        """
        Configures sinogram display by contructing a scatter plot.
        X axis corresponds to image pixel indices in width.
        Y axis corresponds to rotation angle.
        """
        scan_info = ScanInfo.normalize(scan_info)

        # Create data group for each extra data
        scan_info.set_channel_meta(
            "rotation",
            start=start_y,
            stop=stop_y,
            points= npoints_y * npoints_x,
            axis_points=npoints_y,
            axis_id=1,
            axis_kind="forth",
            group="sinogram",
        )
        scan_info.set_channel_meta(
            "translation",
            start=start_x,
            stop=stop_x,
            points= npoints_y * npoints_x,
            axis_points=npoints_x,
            axis_id=0,
            axis_kind="forth",
            group="sinogram",
        )
        scan_info.set_channel_meta("sinogram", group="sinogram")

        # Define a default plot
        scan_info.add_scatter_plot(x="translation", y="rotation", value="sinogram")
        
        return scan_info

    def configure_data_reemission(self, sequence, rotation, spectrum_counters, npixels):
        """
        Adds acquisition channels to tomo sequence object to emit sinogram 
        data (axis X, axis Y and spectrum values).
        """
        
        # Prepare group channels
        def replicate(angles,event):
            return numpy.repeat(angles, npixels)

        def flatten(proj,event):
            dark = self.dark.get_data()[event.node.name][0].astype(int)
            ref = self.ref.get_data()[event.node.name][0].astype(int)
            spectra = numpy.abs(proj-dark).astype(numpy.uint32)/numpy.abs(ref-dark).astype(numpy.uint32)
            return numpy.array(spectra).flatten()

        def translations(spectra,event):
            return flatten([numpy.arange(npixels)] * len(spectra),event)

        sequence.add_custom_channel(
            AcquisitionChannel("translation", numpy.float, (), unit="px")
        )
        sequence.add_custom_channel(
            AcquisitionChannel("rotation", numpy.float, (), unit="degree")
        )
        sequence.add_custom_channel(
            AcquisitionChannel("sinogram", numpy.float, ())
        )
        self.channelmap[f"{rotation.name}"] = [{"name": "rotation", "process": replicate}]
        
        for counter in spectrum_counters:
            
            self.channelmap[counter.fullname] = [
                {"name": "sinogram", "process": flatten},
                {"name": "translation", "process": translations},
            ]
        
    def create_data_reemitter(self, db_name, sequence):
        """
        Creates object that will reemit sinogram data from redis in order
        to build the scatter plot.
        """
        return ScanReEmitter(db_name, sequence, self.channelmap)
        
    def save(self, tomo, sequence_nb):
        
        motor_name = getattr(tomo.rotation_axis, 'original_name', tomo.rotation_axis.name)
        if type(tomo.get_projection_scan()) == list:
            y_data = []
            for proj_scan in tomo.get_projection_scan():
                numpy.append(y_data,proj_scan.get_data()[f'{motor_name}'])
        else:
            y_data = tomo.get_projection_scan().get_data()[f'{motor_name}']
        y_data = numpy.repeat(y_data,tomo.tomo_ccd.detector.image.width)  
        x_data = numpy.arange(tomo.tomo_ccd.detector.image.width)
        x_data = numpy.tile(x_data,tomo.parameters.tomo_n)
        sino_data = current_session.scans[-1].get_data()['sinogram']
        save_NXdata(filename=f"{current_session.scan_saving.get_path()}/sinogram.h5",
                    signal=sino_data,
                    signal_name="values",
                    axes=[x_data,
                        y_data],
                    axes_names=["x", "y"],
                    nxentry_name=f"sinogram_{sequence_nb}",
                    nxdata_name=f"data_{sequence_nb}")

class ScanReEmitter(gevent.Greenlet):
    """
    Class for scan data reemission.
    The class implements methods to listen data events on redis, catch
    desired data and reemit then. 
    
    **Attributes**
    db_name : string
        full name of redis data node. 
        Reflects the position of the node in the tree of nodes.
    sequence : Bliss sequence object
        contains a group of scans
    channelinfo : dict
        contains mapping between data channel name and data processing function.
    stop_handler : Bliss data stream object
        used to stop data stream reading 
    filter : filter
        used to filter redis events
    """
    def __init__(self, db_name, sequence, channelinfo, filter=None):
        self.db_name = db_name
        self.sequence = sequence
        self.channelinfo = channelinfo
        self.stop_handler = None
        self.filter = filter
        super().__init__()

    @property
    def custom_channels(self):
        """
        Returns channels attached to the sequence.
        """
        return self.sequence.custom_channels

    def stop(self, timeout=None):
        """
        Stops data reemission task
        """
        try:
            self.stop_handler.stop()
        except AttributeError:
            pass
        self.join(timeout=timeout)

    def _run(self):
        """
        Starts data reemission task
        """
        self.stop_handler = DataStreamReaderStopHandler()
        try:
            it = get_node(self.db_name).iterator
            for event in it.walk_events(
                filter=self.filter, stop_handler=self.stop_handler
            ):
                if event.type == event.type.END_SCAN:
                    break
                elif event.type == event.type.NEW_DATA:
                    infos = self.channelinfo.get(event.node.name, [])
                    for info in infos:
                        self.reemit(event, info.get("name"), info.get("process"))
        finally:
            self.stop_handler = None

    def reemit(self, event, name, process=None):
        """
        Reemits data corresponding to channels name belonging to mapping dictionary 
        according to processing function.
        """
        try:
            channel = self.custom_channels.get(name)
            if channel is None:
                return
            data = event.data.data
            if callable(process):
                data = process(data,event)
            channel.emit(data)
        except Exception as e:
            raise ValueError(f"Error re-emitting {name}") from e        
        
