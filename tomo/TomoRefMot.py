import sys
import gevent
import numpy as np
import time

from bliss import global_map
from bliss.common.logtools import log_info,log_debug
from bliss.common import session
from bliss.common.motor_group import Group
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.shell.cli.user_dialog import Validator

from tomo.TomoParameters import TomoParameters

class TomoRefMot(TomoParameters):
    """
    Class for tomo reference object.
    The class implements methods to handle reference in tomo acquisition.
    
    ***Attributes***
    name : string
        class name
    tomo_name : string
        name of tomo object
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    ref_motors : list
        list of axis used for reference scans
    settle_time : float
        time in seconds used for motor stabilization at the end of in and out movement
    power_onoff : Boolean
        flag to activate power on and off on reference motors before in and after out movements
    always_update_pos : Boolean
        flag to activate the update of reference motors in beam position before each out movement
    parameters : Tomo parameters object
        parameters associated to tomo reference object
    ref_mot_group : Bliss axis group object
        axis group containing reference motors 
    """
    def __init__(self, tomo_name, tomo_config, tomo, *args, **kwargs):
        # init logging
        self.name = tomo_name+".reference"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")

        config = tomo_config["reference"]
        self.tomo_name = tomo_name
        self.tomo      = tomo
       
        self.ref_motors  = config["motors"]
        self.settle_time = config["settle_time"]
        self.power_onoff = config.get("power_onoff", False)
        self.always_update_pos = config.get("always_update_pos", False)
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        param_name = self.tomo_name+':reference_parameters'
        ref_defaults = {}
        ref_defaults['in_beam_position']         = [0.0]
        ref_defaults['out_of_beam_displacement'] = [0.0]
        
        # Initialise the TomoParameters class
        super().__init__(param_name, ref_defaults)
        
        parameters_dict = self.parameters.to_dict()
        for k,v in ref_defaults.items():
            if k == 'in_beam_position' or k == 'out_of_beam_displacement':
                if len(parameters_dict[k]) != len(self.ref_motors):
                    parameters_dict[k] = [0.0] * len(self.ref_motors)
                    self.parameters.from_dict(parameters_dict)
        
        #
        # test the initialisation of the motors
        #
        for m in self.ref_motors:
            print ("Reference axis used: %s" % m.name)
            print ("Reference axis position: %f" % m.position)
            
        #
        # Create the motor group
        #
        self.ref_mot_group = Group(*self.ref_motors)
        
        log_info(self,"__init__() leaving")

    
    def setup(self):
        """
        Set-up reference motors displacement.
        """
        def_ref_disp = self.tomo.def_ref_disp()
        dlg_ref_disp = UserMsg(label=(f"The default out of beam displacement for motor {self.ref_motors[0].name} is: " + str(def_ref_disp)))
        disp_val = Validator(self.disp_inside_limits,0)
        dlg_out_disp_mot1 = UserInput(label=f"Out of beam displacement for motor {self.ref_motors[0].name}: ", defval=self.parameters.out_of_beam_displacement[0], validator=disp_val)

        if len(self.ref_motors) > 1:
            list_dlg_out_disp_mot = []
            for m in range(1,len(self.ref_motors)):
                disp_val = Validator(self.disp_inside_limits,m)
                dlg_out_disp_mot = UserInput(label=f"Out of beam displacement for motor {self.ref_motors[m].name}: ", defval=self.parameters.out_of_beam_displacement[m], validator=disp_val)
                list_dlg_out_disp_mot.append(dlg_out_disp_mot)
            
            ct = Container( [dlg_ref_disp,dlg_out_disp_mot1,*list_dlg_out_disp_mot], title="")
        else:
            ct = Container( [dlg_ref_disp,dlg_out_disp_mot1], title="")
        
        ret = BlissDialog( [ [ct] ], title='Reference Displacement Setup').show()
        
        if ret != False:
            out_disp = self.parameters.out_of_beam_displacement
            out_disp[0] = float(ret[dlg_out_disp_mot1])
            if len(self.ref_motors) > 1:
                i=0
                
                for dlg_out_disp in list_dlg_out_disp_mot:
                    i+=1
                    out_disp[i] = float(ret[dlg_out_disp])
            
            self.set_out_of_beam_displacement(*out_disp)
        
            i=0
            for m in self.ref_motors: 
                print(f'Out of beam displacement for reference motor {m.name}: ' + str(self.parameters.out_of_beam_displacement[i]) + ' mm')
                i+=1   

    def disp_inside_limits(self, str_input, m):
        """
        Verifies displacement value is inside axis limits.
        """
        
        disp = float(str_input)
        
        target = self.ref_motors[m].position + disp
        
        motion = self.ref_motors[m]._get_motion(target)
    
    def __info__(self):
        info_str  = f"Reference displacement info:\n"
        info_str += f"  motors                   = {[m.name for m in self.ref_motors]}\n"
        info_str += f"  in_beam_position         = {self.parameters.in_beam_position} \n"
        info_str += f"  out_of_beam_displacement = {self.parameters.out_of_beam_displacement} \n"
        info_str += f"  settle_time              = {self.settle_time} \n"
        info_str += f"  power_onoff              = {self.power_onoff} \n"
        info_str += f"  always_update_pos        = {self.always_update_pos} \n"
        return info_str
        
        
    def move_in(self):
        """
        Moves sample to in-beam position.
        """
        log_info(self,"move_in() entering")
        
        motion = []

        in_beam_position = self.parameters.in_beam_position
        
        #
        # check coherence
        #
        if len(self.ref_motors) != len(in_beam_position):
            raise ValueError ("Number of in-beam positions does not correspond to the number\
                              of reference motors!")
        
        for i in range(len(self.ref_motors)):
            motion.append(self.ref_motors[i])
            motion.append(in_beam_position[i])
        
        
        #
        # move all reference motors in parallel
        #
        self.ref_mot_group.move(*motion)
        
        #
        # power off the motors
        #
        if self.power_onoff is True:
            for m in self.ref_motors:
                m.off()
        
        #
        # wait for motor stabilization
        #
        time.sleep(self.settle_time)
        
        log_info(self,"move_in() leaving")
        
            
        
    def move_out(self):
        """
        Moves sample to out-beam position.
        """
        log_info(self,"move_out() entering")
        
        motion = []

        if self.always_update_pos is True:
            self.update_in_beam_position()
        
        in_beam_position = self.parameters.in_beam_position
        out_of_beam_displacement = self.parameters.out_of_beam_displacement
        
        #
        # check coherence
        #
        if len(self.ref_motors) != len(in_beam_position):
            raise ValueError ("Number of in-beam positions does not correspond to the number\
                              of reference motors!")  
        if len(self.ref_motors) != len(out_of_beam_displacement):
            raise ValueError ("Number of displacement positions does not correspond to the number\
                              of reference motors!")
                              
        #
        # calculate out of beam positions
        #                      
        for i in range(len(self.ref_motors)):
            motion.append(self.ref_motors[i])
            motion.append(in_beam_position[i] + out_of_beam_displacement[i])
        
        log_debug(self,motion)
        
        #
        # power on the motors
        #
        if self.power_onoff is True:
            for m in self.ref_motors:
                m.on()
            
        #
        # move all reference motors in parallel
        #
        self.ref_mot_group.move(*motion)
      
        #
        # wait for motor stabilization
        #
        time.sleep(self.settle_time)
        
        log_info(self,"move_out() leaving")
        
   
    def update_in_beam_position(self):
        """
        Updates in-beam position with actual motor position.
        """
        log_info(self, "update_in_beam_position() entering")

        in_beam_pos = [ motor.position for motor in self.ref_motors ]
        self.parameters.in_beam_position = in_beam_pos
            
        log_info(self, "update_in_beam_position() leaving")

    def set_in_beam_position(self, *positions):
        """
        Defines in-beam position.
        """
        log_info(self,"set_in_beam_position() entering")
        
        if len(self.ref_motors) == len(positions):
            self.parameters.in_beam_position = list(positions)
        else:
            raise ValueError ("Number of positions does not correspond to the number\
                              of reference motors")
                              
        log_info(self,"set_in_beam_position() leaving")
        
        
    def set_out_of_beam_displacement(self, *distances):
        """
        Defines out-beam position.
        """
        log_info(self,"set_out_of_beam_displacement() entering")
        
        
        if len(self.ref_motors) == len(distances):
            self.parameters.out_of_beam_displacement = list(distances)
        else:
            raise ValueError ("Number of positions does not correspond to the number\
                              of reference motors")
    
        log_info(self,"set_out_of_beam_displacement() leaving")
