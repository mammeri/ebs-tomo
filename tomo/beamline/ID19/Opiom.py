from bliss import setup_globals


class OpiomSetup:
    
    def __init__(self, name, config):
        
        mux = config['multiplexer']
        
        values = [(0,"CAM_HR"),(1,"CAM_MR")
        dlg_camera    = UserChoice(label="CAMERA", values=values, defval=mux.getOutpuStat("CAMERA"))
        values = [(0,"MUSST_TRIG"),(1,"MUSST_GATE")
        dlg_trigger    = UserChoice(label="TRIGGER", values=values, defval=mux.getOutpuStat("TRIGGER"))
        values = [(0,"CCD"),(1,"MUSST"),(2,"SOFT")]
        dlg_shmode    = UserChoice(label="SHMODE", values=values, defval=mux.getOutpuStat("SHMODE"))

        ret = BlissDialog( [[dlg_camera],[dlg_trigger],[dlg_shmode]], title='Opiom Setup').show()
            
        # returns False on cancel
        if ret != False:
            mux.switch("CAMERA",self.mux.getPossibleValues("CAMERA")[ret[dlg_camera]])
            mux.switch("TRIGGER",self.mux.getPossibleValues("TRIGGER")[ret[dlg_trigger]])
            mux.switch("SHMODE",self.mux.getPossibleValues("SHMODE")[ret[dlg_camera]])
    
    
