# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import Timeout, sleep


from bliss.shell.cli.user_dialog import UserChoice, Container
from bliss.shell.cli.pt_widgets import BlissDialog

from bliss.config.settings import ParametersWardrobe
from bliss.common.logtools import log_info,log_debug

from tomo.TomoOptic import TomoOptic

class StandardOptic(TomoOptic):
    """
    Class to handle standard optics with one objective.
    The class has two parts: 
    The standart methods implemented for every optic and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:
    
    names : list of string
        Ordered list of optic names.
    magnifications : list of float
        Ordered list of magnifications. For every optic name a magnification must be given. 
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective
    
    focus_motor : Bliss Axis objects of focus motor
        The focus motor for the optics
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The range of the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
    
    **Parameters**:
    
    mounted_optic : int
        The index of the specified optic in the names list. Optic to be 
        chosen in the setup()  
    
    **Example yml file**::
    
        name:  stdoptic
        plugin: bliss
        class: StandardOptics
        package: id19.tomo.beamline.ID19.StandardOptics
        
        names: ["BM05  5.4um",
              "ID17  8um",
              "ID19  3.5um",
              "ID19  5.02um",
              "ID19  6.5um",
              "ID19  7.5um",
              "ID19 13um",
              "ID19 20um",
              "ID19 30um",
              "ID19 50um"]
              
        magnifications: [14 /  5.41796,
                       14 /  8.05987,
                       19 /  4.75,
                       19 /  6.85921,
                       19 /  8.8214,
                       19 / 10.1279,
                       19 / 17.6428,
                       19 / 27.1428,
                       19 / 40,
                       19 / 67.857]
        
        image_flipping_hor:  False
        image_flipping_vert: False
        
        focus_motors: [$hrfocus]
        focus_type: "rotation"     # translation or rotation
        focus_scan_range: 20
        focus_scan_steps: 10
        focus_lim_pos:  1000
        focus_lim_neg: -1000
"""


    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        config : dictionary
            The Bliss configuration dictionary
        """
        
        self.__name = name
        self.__config = config
        
        self.names                = config["names"]
        self.magnifications       = config["magnifications"]
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.__name+':parameters'
        param_defaults = {}
        param_defaults ['mounted_optic']  = 0 
        
        super().__init__(name, config, param_name, param_defaults)
        
        
    @property
    def description(self):
        """
        The name string the current optics 
        """
        mag = '{:.2f}'.format(eval(self.magnifications[self.parameters.mounted_optic]))
        name = self.names[self.parameters.mounted_optic]+"_"+str(mag)
        return name
        
        
    #
    # standart otics methods every otics has to implement
    #
    
    @property
    def type(self):
        """
        Returns the class name as the optics type
        """
        return self.__class__.__name__
    
    @property
    def magnification(self):
        """
        Returns the magnification of the current objective used
        """
        magnification = eval(self.magnifications[self.parameters.mounted_optic])
        return magnification
            

    #
    # Specific objective handling
    #
    
    def setup(self):
        """
        Set-up the optic by chosing from the list of available standard optics
        """
        value_list = []
        for i in range (0, len(self.names)):
            value_list.append( (i, self.names[i]) )
            
        # get the actual magnification values as default
        default1 = 0
        
        for i in range (0, len(value_list)):
            if self.names[self.parameters.mounted_optic] == value_list[i][1]:
                default1 = i
            
        dlg1 = UserChoice(values=value_list, defval=default1)
        
        ct1 = Container( [dlg1], title="Optic" )
        ret = BlissDialog( [ [ ct1] ] , title='Standard Optic Setup').show()
        
        # returns False on cancel
        if ret != False:
            # magnification top lens
            self.parameters.mounted_optic = ret[dlg1]
    
    
    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        print ("Standard objective [%s] : magnification = X%.4f" % (self.names[self.parameters.mounted_optic], self.magnification))
        
