import sys
import gevent
import numpy as np
import time
import click

from bliss.setup_globals import *
from bliss.common import scans
from bliss.common.scans import ascan, pointscan
from bliss.common.cleanup import error_cleanup, capture_exceptions, axis as cleanup_axis
from bliss.scanning.scan_tools import peak, goto_peak
from bliss.controllers.lima.roi import Roi
from bliss.shell.standard import umv, mv, umvr, mvr, plotselect
from bliss.config.static import get_config

from nabu.preproc.alignment import CenterOfRotation
from nabu.preproc.alignment import DetectorTranslationAlongBeam
from nabu.preproc.alignment import CameraFocus
from nabu.preproc.alignment import CameraTilt

from bliss.common.plot import plot_image


class TomoAlign():
    
    def __init__(self, name, config):
        self.__name = name
        self.__config = config
        
        self.__tomo        = config.get("tomo", None)
        self.__paper       = config.get("paper_motor", None)
        self.__slits_vg    = config.get("slits_vgap", None)
        self.__slits_hg    = config.get("slits_hgap", None)
        self.__det_vtilt   = config.get("detector_vtilt", None)
        self.__det_htilt   = config.get("detector_htilt", None)
        

    @property
    def name(self):
        """
        A unique name for the Bliss object
        """
        return self.__name

    @property
    def config(self):
        """
        The configuration for Tomo alignment
        """
        return self.__config
                 
    def __info__(self):
        info_str  = f"{self.name} info:\n"
        if self.__tomo != None:
            info_str += f"  tomo           = {self.__tomo.name} \n"
            info_str += f"  detector       = {self.__tomo.tomo_ccd.detector.name}\n"
            info_str += f"  optic          = {self.__tomo.optic.description}\n"
            info_str += f"  pixel size     = {self._pixel_size()} \n"
            info_str += f"  exposure time  = {self.__tomo.parameters.exposure_time} \n\n"
        if self.__paper != None:
            info_str += f"  alignment paper= {self.__paper.name} \n"
        if self.__slits_vg != None:
            info_str += f"  slits vgap     = {self.__slits_vg.name} \n"
        if self.__slits_hg != None:
            info_str += f"  slits hgap     = {self.__slits_hg.name} \n"
        if self.__det_vtilt != None:
            info_str += f"  detector htilt = {self.__det_vtilt.name} \n"
        if self.__det_htilt != None:
            info_str += f"  detector htilt = {self.__det_htilt.name} \n"
        return info_str

    def _pixel_size(self):
        # pixel size returned by tomo is in um
        pixel_size = self.__tomo.tomo_ccd.calculate_image_pixel_size(self.__tomo.optic.magnification)
        #print ("pixel_size in um = %f" % pixel_size)
        
        # pixel size for Nabu caluculations must be in mm
        pixel_size = pixel_size / 1000.0
        return pixel_size
        
    def _get_images(self, scan, detector):
        # read back the scan images aquired
        scan_images=[]
        lima_data = scan.get_data()[detector.image]
        npoints = lima_data.last_index
            
        if npoints < lima_data.buffer_max_number:
            for i in range(npoints):
                scan_images.append(lima_data.get_image(i))
            
            return scan_images
        
        else:
            ValueError("Cannot read images! Not enough images in the Lima image buffer") 
    
    #
    # Detector focus alignment
    #
    
    def focus(self, save=False):
        # prepare the focus scan parameters
        scan_pars = __tomo.optic.focus_scan_parameters()
        
        scan_range = scan_pars["focus_scan_range"]
        scan_steps = scan_pars["focus_scan_steps"]
        focus_type = scan_pars["focus_type"]
        focus_motor = __tomo.optic.focus_motor()
        
        if focus_type == "Rotation" and __tomo.optic.magnification < 0.7:
            scan_range /= 2
            scan_steps *= 2
        
        start = focus_motor.position - scan_range
        stop  = focus_motor.position + scan_range
        
        #pixel size in mm
        pixel_size = _pixel_size()
        
        # move in paper
        print ("Move in paper")
        __paper.IN
        
        # Do the focus scan
        focus_scan = self.focus_scan(focus_motor, start, stop, scan_steps, 
                                     __tomo.parameters.exposure_time , 
                                     __tomo.tomo_ccd.detector, save=save)
        
        if focus_scan != None:
            self.focus_calc(self, focus_scan, focus_motor, __tomo.tomo_ccd.detector, pixel_size):
        
        ct(__tomo.parameters.exposure_time)
        print (f"Focus motor position = {focus_motor.position}")
        
        # Move out paper 
        if click.confirm("Move out paper?", default=True):
            __paper.OUT
    
        
    def focus_scan(self, focus_motor, start, stop, steps, expo_time, detector, save=False):
        # prepare roi counters for statistics
        image_sizes = detector.image.sizes
        focus_roi = Roi(0,0, image_sizes[2], image_sizes[3])
        
        det_rois = detector.roi_counters
        det_rois["focus_roi"] = focus_roi
        
        std = detector.roi_counters.get_single_roi_counters("focus_roi").std
        counters = (std, detector.image)
        
        # clean-up: move back focus motor, delete roi counters, move out paper
        restore_list = (cleanup_axis.POS,)
        with cleanup(*[focus_motor, ], restore_list=restore_list):
            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    # execute focus scan
                    scan = ascan(focus_motor, start, stop, steps, expo_time, counters, title="focus scan", save=save)
                        
                    # get the position on the maximum of the standard deviation
                    pos_std_max = peak()
                    
                    # delete roi counters
                    del det_rois["focus_roi"]
                    
                    return scan
                
                # test if an error has occured
                if len(capture.failed) > 0:
                    # delete roi counters
                    del det_rois["focus_roi"]
                    
                    return None
        
    
    def focus_calc(self, focus_scan, focus_motor, detector, pixel_size):
        if focus_scan != None:
            # read back the aquired images
            foc_images = self._get_images(focus_scan, detector)
            #convert to numpy array
            foc_array= np.array(foc_images)
            
            # read back the aquired positions
            foc_pos = focus_scan.get_data()[focus_motor.name]
            
            focus_calc = CameraFocus()
            focus_pos, focus_ind, tilts_vh = focus_calc.find_scintillator_tilt(foc_array, foc_pos)
            #print (focus_pos, focus_ind, tilts_vh)
            
            tilts_corr_vh_deg = - np.rad2deg(np.arctan(tilts_vh / pixel_size))
            #tilts_corr_vh_rad = - tilts_vh / pixel_size
            
            print (f"\nBest focus position:         {focus_pos}")
            print (f"Tilts vert. and hor. in deg: {tilts_corr_vh_deg}\n")
    
            if click.confirm("Move to the best focus position?", default=True):
            # Always move to the best focus from the bottom
            mv(focus_motor, foc_pos[0])
            # move focus motor to maximum
            mv(focus_motor, focus_pos)
            
            
    #
    # Lateral alignment and camera tilt
    # 
    
    def align(self, save=False):
        # move rotation axis to 0
        umv (__tomo.rotation_axis, 0)
        
        # take dark image
        dark_n = 1
        dark_scan = __tomo.dark_scan(dark_n, __tomo.parameters.exposure_time, save=save)
        # read back the aquired images
        dark_images = get_images(dark_scan, __tomo.tomo_ccd.detector)
        dark_image  = dark_images[0]
        
        # update reference motor position to current values
        __tomo.update_reference_positions()
        
        # take reference image
        ref_n = 1
        ref_scan = __tomo.ref_scan(ref_n, __tomo.parameters.exposure_time, save=save)
        # read back the aquired images
        ref_images = get_images(ref_scan, __tomo.tomo_ccd.detector)
        ref_image  = ref_images[0]
        
        #pixel size in mm
        pixel_size = _pixel_size()
        
        # do alignment scan
        al_scan = self.align_scan(__tomo.rotation_axis,
                                  __tomo.parameters.exposure_time, 
                                  __tomo.tomo_ccd.detector, save=save)
        
        # works only with on motor today!
        lateral_motor = __tomo.reference.ref_motors[0]
        
        self.align_calc(self, al_scan, lateral_motor, __tomo.rotation_axis, 
                        __tomo.tomo_ccd.detector, pixel_size,
                        __tomo.parameters.source_sample_distance, 
                        __tomo.parameters.sample_detector_distance,
                        dark_image, ref_image)
        
        #update reference motor position to current values
        __tomo.update_reference_positions()
        
        ct(tomo.parameters.exposure_time)
        print (f"Lateral alignment motor position = {lateral_motor.position}")
        print (f"Camera tilt position (rotc)      = {rotc.position}")
    
    def align_calc(self, align_scan, lateral_motor, rotc_motor, 
                   detector, pixel_size,
                   source_sample_distance, 
                   sample_detector_distance,
                   dark_image, ref_image):
        
        # read back the aquired images
        al_images = get_images(align_scan, detector)
            
        # prepare the images
        radio0   = (al_images[0].astype(float) - dark_image.astype(float)) / (ref_image.astype(float)- dark_image.astype(float))
        radio180 = (al_images[1].astype(float) - dark_image.astype(float)) / (ref_image.astype(float)- dark_image.astype(float))
        
        # flip the radio180 for the calculation
        radio180_flip = np.fliplr(radio180.copy())
        
        #plot_image(radio0)
        #plot_image(radio180_flip)
        
        # calculate the lateral correction for the rotation axis 
        # and the camera tilt with line by line correlation
        tilt_calc = CameraTilt()
        pixel_cor, camera_tilt = tilt_calc.compute_angle(radio0, radio180_flip)
        print("CameraTilt: pixel_cor = %f" % pixel_cor)

        #distance from source to sample   = L1(mm)
        #distance from source to detector = L2(mm)
        #size of the image pixel          = s(mm/pixel)
        #dmot(mm) = L1/L2*s*pixel_cor
        cor_factor = source_sample_distance / \
                     (source_sample_distance + sample_detector_distance) * \
                      pixel_size
        pos_cor = cor_factor * pixel_cor
        
        print (f"\nLateral alignment position correction in pixel: {pixel_cor}")
        print (f"Lateral alignment position correction in mm:    {pos_cor}")
        print (f"Camera tilt in deg: {camera_tilt}\n")   
        
        # apply lateral correction
        if click.confirm("Apply the lateral position correction?", default=True):
            umvr (lateral_motor, pos_cor)
            
        # apply tilt correction
        if click.confirm("Apply the camera tilt?", default=True):
            umvr (rotc_motor, -1.0*camera_tilt)
        
    
    def align_scan(self, rotation_motor, expo_time, detector, save=False):
        # clean-up: move back rot
        restore_list = (cleanup_axis.POS,)
        with cleanup(rotation_motor, restore_list=restore_list):
            # scan rot
            rot_pos_list=[0.0, 180.0]
            scan = pointscan(rotation_motor, rot_pos_list, expo_time, detector.image, title="align scan", save=save)
            
            return scan
            
    #
    # Camera stage tilt alignment
    # 
    
    def alignxc(self, start=100, stop=800, steps=10, save=False):
        
        #pixel size in mm
        pixel_size = _pixel_size()
        
        # slit gap to use
        if pixel_size < 0.003:      #3um
            slit_gap = 0.2
        else:
            slit_gap = 1.0
            
        self.alignxc_scan(__tomo.detector_axis, start, stop, steps, 
                          __tomo.parameters.exposure_time, 
                          __tomo.tomo_ccd.detector, 
                          self.__slits_vg, self.__slits_hg, slit_gap, 
                          save=False):
        
        calculation to be added!!!!!!!!!
    
    
    def alignxc_scan(self, xc, start, stop, steps, expo_time, detector, 
                     svg, shg, slit_gap, save=False):  
        
        # save current slit positions
        vg_pos = svg.position
        hg_pos = shg.position
        print (f"Current slit gap positions: vertical={vg_pos}, horizontal={hg_pos}")
        print (f"Current xc positions:       xc={xc.position}")
        
        # clean-up: move back xc motor, open_slits
        restore_list = (cleanup_axis.POS,)
        with error_cleanup(*[xc, svg, shg], restore_list=restore_list):
            # close the slits
            umv(svg, slit_gap, shg, slit_gap)
        
            # scan xc
            scan = ascan(xc, start, stop, steps, expo_time, detector.image, title="align xc scan", save=save)
        
            # open the slits
            umv(svg, vg_pos, shg, hg_pos)

            # return images
            return scan
