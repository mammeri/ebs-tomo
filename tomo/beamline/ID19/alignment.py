import sys
import gevent
import numpy as np
import time
import click

from bliss.setup_globals import *
from bliss.common import scans
from bliss.common.scans import ascan, pointscan
from bliss.common.cleanup import error_cleanup, capture_exceptions, axis as cleanup_axis
from bliss.scanning.scan_tools import peak, goto_peak
from bliss.controllers.lima.roi import Roi
from bliss.shell.standard import umv, mv, umvr, mvr, plotselect
from bliss.config.static import get_config

from nabu.preproc.alignment import CenterOfRotation
from nabu.preproc.alignment import DetectorTranslationAlongBeam
from nabu.preproc.alignment import CameraFocus
from nabu.preproc.alignment import CameraTilt

from bliss.common.plot import plot_image

import matplotlib.pyplot as plt    


def tomo_alignxc(tomo, svg="ssvg", shg="sshg", xc_start=100, xc_end=900, save=False, plot=False):
    
    pixel_size = tomo.optic.image_pixel_size
    print ("pixel_size in um = %f" % pixel_size)
    
    # do alignment scan
    al_scan = alignxc_scan(tomo.tomo_ccd.detector, tomo.parameters.exposure_time,
                         pixel_size,    # should be in um
                         tomo.detector_axis,
                         svg=svg, shg=shg, xc_start=xc_start, xc_end=xc_end, save=save)
    # read back the aquired images
    al_images = get_images(al_scan, tomo.tomo_ccd.detector)
    #print(al_images)
    
    # read back the motor positions 
    al_pos = al_scan.get_data()[tomo.detector_axis.name]
    #print(al_pos)
    
    # take dark image
    dark_n = 1
    dark_scan = tomo.dark_scan(dark_n, tomo.parameters.exposure_time, save=save)
    # read back the aquired images
    dark_images = get_images(dark_scan, tomo.tomo_ccd.detector)
    
    # substract dark from images
    for i in al_images:
        i = i.astype(float) - dark_images[0].astype(float)
    #convert to numpy array
    al_array= np.array(al_images)
        
    # calculate XC tilt shifts
    # the scan positions and the pixel_size must have the same units!
    
    tr_calc = DetectorTranslationAlongBeam()
    # enable calculation results plotting
    if plot == True:
        tr_calc.verbose=True
    
    shifts_v, shifts_h = tr_calc.find_shift(al_array, al_pos)
    
    # pixel size should be im mm
    tilt_v_deg = - np.rad2deg(np.arctan(shifts_v * pixel_size / 1000.0))
    tilt_h_deg = - np.rad2deg(np.arctan(shifts_h * pixel_size / 1000.0))
    print (f"\nVertical   tilt in deg (thy): {tilt_v_deg}")
    print (f"Horizontal tilt in deg (thz): {tilt_h_deg}\n")
    
    # apply to the motors
    # needs a real object to configure motors!!!!!
    
    # Kill the plot window when it was requested
    if plot == True:
        if click.confirm("Close the plot window?", default=True):
            # workaround for nabu plotting problem
            plt.close('all')
    
    
def tomo_align(tomo, save=False, plot=False):
    
    # move rotation axis to 0
    umv (tomo.rotation_axis, 0)
    
    # take dark image
    dark_n = 1
    dark_scan = tomo.dark_scan(dark_n, tomo.parameters.exposure_time, save=save)
    # read back the aquired images
    dark_images = get_images(dark_scan, tomo.tomo_ccd.detector)
    
    # update reference mptpr position to current values
    tomo.reference.update_in_beam_position()
    
    # take reference image
    ref_n = 1
    ref_scan = tomo.ref_scan(ref_n, tomo.parameters.exposure_time, save=save)
    # read back the aquired images
    ref_images = get_images(ref_scan, tomo.tomo_ccd.detector)
    
    # do alignment scan
    al_scan = align_scan(tomo.tomo_ccd.detector, tomo.parameters.exposure_time, 
                         tomo.rotation_axis, save=save)
    # read back the aquired images
    al_images = get_images(al_scan, tomo.tomo_ccd.detector)
        
    # prepare the images
    radio0   = (al_images[0].astype(float) - dark_images[0].astype(float)) / ref_images[0].astype(float)
    radio180 = (al_images[1].astype(float) - dark_images[0].astype(float)) / ref_images[0].astype(float)
    
    # flip the radio180 for the calculation
    radio180_flip = np.fliplr(radio180.copy())
    
    #plot_image(radio0)
    #plot_image(radio180_flip)
    
    # calculate the lateral correction for the rotation axis with 2D correlation
    #cor = CenterOfRotation()
    #pixel_cor = cor.find_shift(radio0, radio180_flip) 
    #print("CenterOfRotation: pixel_cor = %f" % pixel_cor)
    
    # calculate the lateral correction for the rotation axis 
    # and the camera tilt with line by line correlation
    tilt_calc = CameraTilt()
    # enable calculation results plotting
    if plot == True:
        tilt_calc.verbose=True
    
    pixel_cor, camera_tilt = tilt_calc.compute_angle(radio0, radio180_flip)
    print("CameraTilt: pixel_cor = %f" % pixel_cor)

    #distance from source to sample   = L1(mm)
    #distance from source to detector = L2(mm)
    #size of the image pixel          = s(mm/pixel)
    #dmot(mm) = L1/L2*s*pixel_cor
    
    pixel_size = tomo.optic.image_pixel_size
    print ("pixel_size in um = %f" % pixel_size)
    
    cor_factor = tomo.parameters.source_sample_distance / \
                 (tomo.parameters.source_sample_distance + tomo.parameters.sample_detector_distance) * \
                 (pixel_size / 1000.0)
    pos_cor = cor_factor * pixel_cor
    
    print (f"\nLateral alignment position correction in pixel: {pixel_cor}")
    print (f"Lateral alignment position correction in mm:    {pos_cor}")
    print (f"Camera tilt in deg: {camera_tilt}\n")   
    
    # apply correction
    if click.confirm("Apply the lateral position correction?", default=True):
        lateral_al_mot = tomo.reference.ref_motors[0]
        umvr (lateral_al_mot, pos_cor)
        
        # update reference mptpr position to current values
        tomo.reference.update_in_beam_position()
        
        ct(tomo.parameters.exposure_time)
        print (f"Moved lateral alignment motor to: {lateral_al_mot.position}")
        
    # apply correction
    if click.confirm("Apply the camera tilt?", default=True):
        rotc = tomo.optic.rotc_motor()
        umvr (rotc, camera_tilt)
    
        ct(tomo.parameters.exposure_time)
        print (f"Moved camera tilt to: {rotc.position}")
    
    # Kill the plot window when it was requested
    if plot == True:
        # workaround for nabu plotting problem
        plt.close('all')


def tomo_dofocus(tomo, save=False, plot=False):
    
    # prepare the focus scan parameters
    scan_pars = tomo.optic.focus_scan_parameters()
    
    scan_range = scan_pars["focus_scan_range"]
    scan_steps = scan_pars["focus_scan_steps"]
    focus_type = scan_pars["focus_type"]
    focus_motor = tomo.optic.focus_motor()
    
    if focus_type == "Rotation" and tomo.optic.magnification < 0.7:
        scan_range /= 2
        scan_steps *= 2
    
    start = focus_motor.position - scan_range
    stop  = focus_motor.position + scan_range
    
    #pixel size in mm
    pixel_size = tomo.optic.image_pixel_size
    print ("pixel_size in um = %f" % pixel_size)
    pixel_size = pixel_size / 1000.0
    
    # Do the focus scan
    focus_scan = dofocus(focus_motor, start, stop, scan_steps, tomo.parameters.exposure_time , tomo.tomo_ccd.detector, save=save)
    
    if focus_scan != None:
        # read back the aquired images
        foc_images = get_images(focus_scan, tomo.tomo_ccd.detector)
        #convert to numpy array
        foc_array= np.array(foc_images)
        
        # read back the aquired positions
        foc_pos = focus_scan.get_data()[focus_motor.name]
        #print (foc_pos)
        
        focus_calc = CameraFocus()
        # enable calculation results plotting
        if plot == True:
            focus_calc.verbose = True
        
        focus_pos, focus_ind, tilts_vh = focus_calc.find_scintillator_tilt(foc_array, foc_pos)
        #print (focus_pos, focus_ind, tilts_vh)
        
        tilts_corr_vh_deg = - np.rad2deg(np.arctan(tilts_vh / pixel_size))
        #tilts_corr_vh_rad = - tilts_vh / pixel_size
        
        print (f"\nBest focus position:  {focus_pos}")
        print (f"Tilts vert. and hor.: {tilts_corr_vh_deg}\n")
        
        if click.confirm("Move to best focus position?", default=True):
            # Always move to the best focus from the bottom
            mv(focus_motor, foc_pos[0])
            # move focus motor to maximum
            mv(focus_motor, focus_pos)
            
        if click.confirm("Move out paper?", default=True):
            scr = get_config().get("scr")
            umv(scr, -130.0)
    
        # Kill the plot window when it was requested
        if plot == True:
            # workaround for nabu plotting problem
            plt.close('all')


def dofocus(focus_motor, start, stop, steps, expo_time, detector, save=False):
    # get motor to move paper
    scr = get_config().get("scr")
    
    # prepare roi counters for statistics
    #image_sizes = detector.image.sizes
    #focus_roi = Roi(0,0, image_sizes[2], image_sizes[3])
    focus_roi = Roi(0,0, detector.image.roi[2], detector.image.roi[3])
    
    det_rois = detector.roi_counters
    det_rois["focus_roi"] = focus_roi
    
    std = detector.roi_counters.get_single_roi_counters("focus_roi").std
    counters = (std, detector.image)
    
    # clean-up: move back focus motor, delete roi counters, move out paper
    restore_list = (cleanup_axis.POS,)
    with error_cleanup(*[focus_motor, scr], restore_list=restore_list):
        with capture_exceptions(raise_index=0) as capture:
            with capture():
                # move in the paper
                print ("Move in paper")
                umv(scr, -68.0)
                
                # execute focus scan
                scan = ascan(focus_motor, start, stop, steps, expo_time, counters, title="focus scan", save=save)
                    
                # get the position on the maximum of the standard deviation
                pos_std_max = peak()
                
                # move out paper
                #print ("Move out paper")
                #umv(scr, -130.0)
                
                # delete roi counters
                del det_rois["focus_roi"]
                
                return scan
            
            # test if an error has occured
            if len(capture.failed) > 0:
                # delete roi counters
                del det_rois["focus_roi"]
                
                return None


    
    
    
def alignxc_scan(detector, expo_time, image_pixel_size, xc="xc", svg="ssvg", shg="sshg", xc_start=100, xc_end=800, save=False):
    if isinstance(xc, str):
        xc = get_config().get(xc)
    if isinstance(svg, str):
        svg = get_config().get(svg)
    if isinstance(shg, str):
        shg = get_config().get(shg)
    
    # save current slit positions
    vg_pos = svg.position
    hg_pos = shg.position
    print (f"Current slit gap positions: vertical={vg_pos}, horizontal={hg_pos}")
    print (f"Current xc positions:       xc={xc.position}")
    
    # clean-up: move back xc motor, open_slits
    restore_list = (cleanup_axis.POS,)
    with error_cleanup(*[xc, svg, shg], restore_list=restore_list):
        # close the slits
        if image_pixel_size < 3:
            umv(svg, 0.2,
                shg, 0.2)
        else:
            umv(svg, 1.0,
                shg, 1.0)
    
        # scan xc
        npoints = 10
        scan = ascan(xc, xc_start, xc_end, npoints, expo_time, detector.image, title="align xc scan", save=save)
    
        # open the slits
        umv(svg, vg_pos,
            shg, hg_pos)

        # return images
        return scan



def align_scan(detector, expo_time, rot, save=False):

    # clean-up: move back rot
    restore_list = (cleanup_axis.POS,)
    with cleanup(rot, restore_list=restore_list):
        # scan rot
        rot_pos_list=[0.0, 180.0]
        scan = pointscan(rot, rot_pos_list, expo_time, detector.image, title="align scan", save=save)
        
        return scan



def get_images(scan, detector):
    # read back the scan images aquired
    scan_images=[]
    lima_data = scan.get_data()[detector.image]
    npoints = lima_data.last_index
        
    if npoints < lima_data.buffer_max_number:
        for i in range(npoints):
            scan_images.append(lima_data.get_image(i))
        
        return scan_images
    
    else:
        ValueError("Cannot read images! Not enough images in the Lima image buffer") 
