# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import Timeout, sleep

from bliss.shell.cli.user_dialog import UserChoice, Container
from bliss.shell.cli.pt_widgets import BlissDialog

from bliss.config.settings import ParametersWardrobe
from bliss.common.logtools import log_info, log_debug

from tomo.TomoOptic import TomoOptic


class HasselbladOptic(TomoOptic):
    """
    Class to handle two lenses Hasselblad optics.
    The class has two parts: 
    The standart methods implemented for every optic and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:
    
    name : str
        The Bliss object name
    config : dictionary
        The Bliss configuration dictionary
    magnifications : list of float
        A list with the possible magnification lenses that can be used as top or bottom lenses
    magnification : float
        magnification value corresponding to current objective used
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective
    
    focus_motors : list of Bliss Axis objects
        The focus motors for the optics
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The range of the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
    
    **Parameters**:
    
    top_magnification : float
        The magnification of the top lens. To be configured in the setup.
    bottom_magnification : float
        The magnification of the bottom lens. To be configured in the setup.
        
    **Example yml file**::
    
        - name:  mrhasselblad
          plugin: bliss
          class: HasselbladOptic
          package: tomo.beamline.ID19
          
          magnifications: [24, 100, 210, 300]
          
          image_flipping_hor:  False
          image_flipping_vert: False
          
          rotc_motor:  $rotc1p29A
          focus_motor: $focus0p45A
          focus_type: "translation"     # translation or rotation
          focus_scan_range: 20
          focus_scan_steps: 20
          focus_lim_pos:  0.5
          focus_lim_neg: -0.5
"""

    def __init__(self, name, config):

        self.__name = name
        self.__config = config

        self.magnifications = config["magnifications"]

        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values

        param_name = self.__name + ":parameters"
        param_defaults = {}
        param_defaults["top_magnification"] = self.magnifications[0]
        param_defaults["bottom_magnification"] = self.magnifications[0]

        # Initialise the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)

        self.magnification = self.calculate_magnification()

    @property
    def description(self):
        """
        The name string the current optics 
        """
        name = (
            "Hasselblad_"
            + str(self.parameters.top_magnification)
            + "_"
            + str(self.parameters.bottom_magnification)
        )
        return name

    #
    # standart otics methods every otics has to implement
    #

    @property
    def type(self):
        """
        Returns the class name as the optics type
        """
        return self.__class__.__name__

    def calculate_magnification(self):
        """
        Returns magnification value according to top and bottom lenses used
        """
        return (
            1.0
            * self.parameters.top_magnification
            / self.parameters.bottom_magnification
        )

    @property
    def magnification(self):
        """
        Returns the magnification of the current objective used
        """
        return self.__magnification

    @magnification.setter
    def magnification(self, value):
        """
        Sets the magnification of the current objective used
        """
        self.__magnification = value

    #
    # Specific objective handling
    #

    def optic_setup(self):
        """
        Set-up the magnification for the two objectives mounted.
        They can be chosen from the list of possible magnifications.
        """

        value_list = []
        for i in self.magnifications:
            value_list.append((i, "X" + str(i)))

        # get the actual magnification values as default
        default1 = 0
        default2 = 0

        for i in range(0, len(value_list)):
            if self.parameters.top_magnification == value_list[i][0]:
                default1 = i
            if self.parameters.bottom_magnification == value_list[i][0]:
                default2 = i

        dlg1 = UserChoice(values=value_list, defval=default1)
        dlg2 = UserChoice(values=value_list, defval=default2)

        ct1 = Container([dlg1], title="Top Lens")
        ct2 = Container([dlg2], title="Bottom Lens")
        ret = BlissDialog([[ct1, ct2]], title="Hasselblad Setup").show()

        # returns False on cancel
        if ret != False:
            # magnification top lens
            self.parameters.top_magnification = float(ret[dlg1])
            # magnification bottom lens
            self.parameters.bottom_magnification = float(ret[dlg2])

            self.magnification = self.calculate_magnification()

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            top = self.parameters.top_magnification
            bottom = self.parameters.bottom_magnification
            magnification = self.magnification

            print(
                "Hasselblad Objective with X%s top lens and X%s bottom lens : magnification = X%s"
                % (str(top), str(bottom), str(magnification))
            )

        except ValueError as err:
            print("Optics indicates a problem:\n", err)
