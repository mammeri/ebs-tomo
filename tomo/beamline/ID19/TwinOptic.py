# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

# from gevent import Timeout, sleep


from bliss.shell.cli.user_dialog import UserChoice, Container
from bliss.shell.cli.pt_widgets import BlissDialog

from bliss.config.settings import ParametersWardrobe
from bliss.common.logtools import log_info, log_debug

from tomo.TomoOptic import TomoOptic


class TwinOptic(TomoOptic):
    """
    Class to handle double optics of type TwinMic.
    The class has three parts: 
    The standart methods implemented for every optic,
    methods to handle the two objectives and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:
    
    magnifications : list of float
        A list with the possible magnification lenses for this objective type
    magnification : float
        magnification value corresponding to current objective used    
    selection_motor : Bliss Axis object
        The motor used to switch the objectives.
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective
    
    focus_motor : Bliss Axis object
        The focus motor for the ojective
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
        
    **Example yml file**::
    
        - name:  twinmic
          plugin: bliss
          class: TwinOptic
          package: tomo.beamline.ID19
          
          magnifications: [2, 5]                         # Two objectives can be mounted and dynamically chosen.
                                                         # The magnifications given are used in the order [obj1, obj2]
                                                         # Possible lens mgnification values are [2, 5, 7.5, 10]
          selection_motor: $objsel
          image_flipping_hor:  True
          image_flipping_vert: False
          
          focus_motor: $hrfocus
          focus_type: "translation"     # translation or rotation
          focus_scan_steps: 20
          focus_lim_pos:  0.5
          focus_lim_neg: -0.5
    """

    def __init__(self, name, config):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        config : dictionary
            The Bliss configuration dictionary
        """

        self.__name = name
        self.__config = config

        self.magnifications = config["magnifications"]
        self.selection_motor = config["selection_motor"]

        param_name = self.__name + ":parameters"
        param_defaults = {}
        
        # Initialise the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)

        self.magnification = self.calculate_magnification()

    @property
    def description(self):
        """
        The name string the current optics 
        """
        name = "TwinMic_" + str(self.magnification)
        return name

    #
    # standart otics methods every otics has to implement
    #

    @property
    def type(self):
        """
        Returns the class name as the optics type
        """
        return self.__class__.__name__

    def calculate_magnification(self):
        """
        Returns magnification value according to current objective used
        """
        objective = self.objective
        return self.magnifications[objective - 1]

    @property
    def magnification(self):
        """
        Returns the magnification of the current objective used
        """
        return self.__magnification

    @magnification.setter
    def magnification(self, value):
        """
        Sets the magnification of the current objective used
        """
        self.__magnification = value

    #
    # Specific objective handling
    #

    def optic_setup(self):
        """
        Set-up the objective to be used by chosing the objective's magnification
        """
        value_list = []
        for i in self.magnifications:
            value_list.append((i, "X" + str(i)))

        # get the actual magnification value as default
        default1 = 0
        for i in range(0, len(value_list)):
            if self.magnification == value_list[i][0]:
                default1 = i

        dlg1 = UserChoice(values=value_list, defval=default1)
        ct1 = Container([dlg1], title="Objective")
        ret = BlissDialog([[ct1]], title="TwinMic Setup").show()

        # returns False on cancel
        if ret != False:
            # get the objective chosen
            sel_objective = self.magnifications.index(float(ret[dlg1])) + 1
            # move to objective
            if self.objective != sel_objective:
                self.objective = sel_objective

            self.magnification = self.calculate_magnification()

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            ojective = self.objective
            magnification = self.magnification
            print(
                "Objective %d selected with a magnification of X%s" % ojective,
                str(magnification),
            )

        except ValueError as err:
            print("Optics indicates a problem:\n", err)

    @property
    def objective(self):
        """
        Reads and sets the current objective (1 or 2)
        """
        return self._objective_state()

    @objective.setter
    def objective(self, value):
        """
        Moves to the objective 1 or 2
        """
        if value < 1 or value > 2:
            raise ValueError("Only the objectives 1 and 2 can be chosen!")

        if value == 1:
            self.selection_motor.hw_limit(-1)  # negative limit
        else:
            if value == 2:
                self.selection_motor.hw_limit(1)  # positive limit

    def _objective_state(self):
        """
        Evaluates which obective is currently used and returns its value (1or 2)
        """
        current_objective = None

        # Get current objective
        lim_high = self.selection_motor.state.LIMPOS
        lim_low = self.selection_motor.state.LIMNEG

        if lim_high is True and lim_low is False:
            current_objective = 2
        else:
            if lim_high is False and lim_low is True:
                current_objective = 1
            else:
                if lim_high is True and lim_low is True:
                    raise ValueError(
                        "No objective selected\nBoth limits are active at the same time, very strange!"
                    )
                else:
                    if lim_high is False and lim_low is False:
                        raise ValueError("No objective selected\nNo active limit!")

        return current_objective

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        # the focus scan range is dependent on the magnigication
        if self.magnification == 2:
            self.focus_scan_range = 0.2
        else:
            self.focus_scan_range = 0.025

        scan_params = super().focus_scan_parameters()
        return scan_params
