# -*- coding: utf-8 -*-
#
# This file is part of the bliss project
#
# Copyright (c) 2016 Beamline Control Unit, ESRF
# Distributed under the GNU LGPLv3. See LICENSE for more info.

from gevent import Timeout, sleep
import PyTango

from bliss.shell.cli.user_dialog import UserMsg, UserChoice, Container
from bliss.shell.cli.pt_widgets import BlissDialog

from bliss.common.logtools import log_info, log_debug

from tomo.TomoOptic import TomoOptic


class PeterOptic(TomoOptic):
    """
    Class to handle three objectives and four eyepieces of type OptiquePeter.
    The class has three parts: 
    The standart methods implemented for every optic,
    methods to handle the three objectives and four eypieces and
    methods to extract the used focus motor and the necessary focus scan parameters.

    **Attributes**:
    
    name : str
        The Bliss object name
    config : dictionary
        The Bliss configuration dictionary
    magnifications : list of float
        An ordered list with magnifications of the three objectives
    magnification : float
        magnification value corresponding to current objective and eyepiece used
    eyepiece_magnifications : list of float
        An ordered list of with the magnifications of the four eyepieces
    wago_device : string
        The name of the Wago server Tango device to use
    wago_proxy : Tango device proxy
        Proxy object used to communicate with wago device
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied verticalal image flipping by the objective
    
    rotc_motor : Bliss Axis object
        The camera rotation motor used for all three objectives
    focus_motor : Bliss Axis object
        The focus motor used for all three objectives
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_steps : int
        The number of scan steps for the focus scan. The scan range depends
        on the maginification used.
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
    
        
    **Example yml file**:
    
        - name:  hrpeter
          plugin: bliss
          class: PeterOptic
          package: tomo.beamline.ID19
          
          magnifications: [4, 10 , 20]                    # Three objectives can be mounted and dynamically chosen.
                                                          # The magnifications given are used in the order [obj1, obj2, obj3]
          
          eyepiece_magnifications: [2.0, 2.5, 3.3, 4.0]   # 4 different evepieces can be mounted. 
                                                          # which one is mounted is indicated by the Wago.
          wago_device: "id19/wcid19c/tg"
          image_flipping_hor:  False
          image_flipping_vert: False
          
          rotc_motor:  $hrrotc
          focus_motor: $focrev
          focus_type: "translation"     # translation or rotation
          focus_scan_range: 0.02
          focus_scan_steps: 20
          focus_lim_pos:  0.5
          focus_lim_neg: -0.5
"""

    def __init__(self, name, config):

        self.__name = name
        self.__config = config

        self.magnifications = config["magnifications"]
        self.eyepiece_magnifications = config["eyepiece_magnifications"]
        self.wago_device = config["wago_device"]

        self.current_objective = None
        self.current_eyepiece = None
        
        param_name = self.__name + ":parameters"
        param_defaults = {}

        # Connect to the wago device
        self.wago_proxy = PyTango.DeviceProxy(self.wago_device)

        # Initialise the TomoOptic class
        super().__init__(name, config, param_name, param_defaults)

        # self.magnification = 0.0

    @property
    def description(self):
        """
        The name string the current optics 
        """
        try:
            if self.current_eyepiece == None:
                name = (
                    "OptiquePeter_"
                    + str(self.magnifications[self.objective - 1])
                    + "_1"
                )
            else:
                name = (
                    "OptiquePeter_"
                    + str(self.magnifications[self.objective - 1])
                    + "_"
                    + str(self.eyepiece_magnifications[self.current_eyepiece])
                )
        except Exception as e:
            name = "OptiquePeter: " + str(e)

        return name

    #
    # standart otics methods every otics has to implement
    #

    @property
    def type(self):
        """
        Returns the class name as the optics type
        """
        return self.__class__.__name__

    # def calculate_magnification(self):
    # """
    # Returns magnification value according to current objective and eyepiece used
    # """
    # ret = self.objective
    # objective = self.current_objective
    # eyepiece  = self.current_eyepiece

    # if eyepiece == None:
    # mag = self.magnifications[objective]
    # else:
    # mag = self.magnifications[objective] * self.eyepiece_magnifications[eyepiece]

    # return mag

    @property
    def magnification(self):
        """
        Returns magnification value according to current objective and eyepiece used
        """
        ret = self.objective
        objective = self.current_objective
        eyepiece = self.current_eyepiece

        if eyepiece == None:
            mag = self.magnifications[objective]
        else:
            mag = (
                self.magnifications[objective] * self.eyepiece_magnifications[eyepiece]
            )

        return mag

    # @property
    # def magnification(self):
    # """
    # Returns the magnification of the current objective used
    # """
    # return self.__magnification

    # @magnification.setter
    # def magnification(self,value):
    # """
    # Sets the magnification of the current objective used
    # """
    # self.__magnification = value

    #
    # Specific objective handling
    #

    def optic_setup(self):
        """
        Set-up the magnification for the two objectives mounted.
        They can be chosen from the list of possible magnifications.
        """

        value_list = []
        for i in self.magnifications:
            value_list.append((i, "X" + str(i)))

        # get the actual magnification value as default
        default1 = 0
        for i in range(0, len(value_list)):
            if self.magnifications[self.current_objective] == value_list[i][0]:
                default1 = i

        dlg1 = UserChoice(values=value_list, defval=default1)

        if self.current_eyepiece == None:
            msg = "No eyepiece mounted"
        else:
            msg = f"Eyepiece with a magnification X{self.magnification}"
        dlg2 = UserMsg(label=msg)

        ct1 = Container([dlg1], title="Objective")
        ct2 = Container([dlg2], title="Eyepiece")
        ret = BlissDialog([[ct1], [ct2]], title="Peter Setup").show()

        # returns False on cancel
        if ret != False:
            # get the objective chosen
            sel_objective = self.magnifications.index(float(ret[dlg1])) + 1
            # move to objective
            if self.objective != sel_objective:
                self.objective = sel_objective

            # self.magnification = self.calculate_magnification()

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an objective cannot be determined, the reason gets printed.
        """
        try:
            magnification = self.magnification

            # test if eyepiece is connected
            if self.current_eyepiece == None:
                print(
                    "No eyepiece selected\nThe wago key opep does not indicate a selected eyepiece!\n"
                )
                print(
                    "Objective %d selected with NO eyepiece : magnification = X%s"
                    % ((self.current_objective + 1), str(magnification))
                )
            else:
                print(
                    "Objective %d selected with eyepiece %d : magnification = X%s"
                    % (
                        (self.current_objective + 1),
                        (self.current_eyepiece + 1),
                        str(magnification),
                    )
                )

        except ValueError as err:
            print("Optics indicates a problem:\n", err)

    @property
    def objective(self):
        """
        Reads and sets the current objective (1, 2 or 3)
        """
        return self._objective_state()

    @objective.setter
    def objective(self, value):
        """
        Moves to the objective 1, 2 or 3
        """
        if value < 1 or value > 3:
            raise ValueError("Only the objectives 1, 2 and 3 can be chosen!")

        ret = self.objective

        rotation = [0, 0]
        if (
            (value - 1) > self.current_objective
            and not (self.current_objective == 0 and (value - 1) == 2)
        ) or (self.current_objective == 2 and (value - 1) == 0):
            # rotate in the positive direction
            rotation[1] = 1
            # print ("positive")
        else:
            # rotate in the negative direction
            rotation[0] = 1
            # print ("negative")

        self.wago_proxy.write_attribute("oprot", rotation)
        sleep(0.1)

        rotation[0] = 0
        rotation[1] = 0

        self.wago_proxy.write_attribute("oprot", rotation)
        sleep(5.0)

    def _objective_state(self):
        """
        Evaluates which obective is currently used and returns its value (1, 2 or 3)
        """
        self.current_objective = None
        self.current_eyepiece = None

        # read objective and eyepiece arrays from wago
        attributes = self.wago_proxy.read_attributes(["opobj", "opep"])

        # The default value for objectives is 1. O indicates the objective is chosen.
        ojective_states = attributes[0].value

        found = False
        for i in range(0, len(ojective_states)):
            if ojective_states[i] == 0:
                self.current_objective = i
                found = True
                break
        if found == False:
            raise ValueError(
                "No objective selected\nThe wago key opobj does not indicate a selected opjective!"
            )

        # The default value for eypieces is 0. 1 indicates the eyepiece is chosen.
        eyepiece_states = attributes[1].value

        found = False
        for i in range(0, len(eyepiece_states)):
            if eyepiece_states[i] == 1:
                self.current_eyepiece = i
                found = True
                break
        if found == False:
            # no eyepiece found. Set to None!
            self.current_eyepiece = None
            # raise ValueError("No eyepiece selected\nThe wago key opep does not indicate a selected eyepiece!")

        return self.current_objective + 1

    #
    # Focus related methods
    #

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """

        # the focus scan range is dependent on the magnigication
        if self.objective == 1:
            self.focus_scan_range = 0.3
        else:
            self.focus_scan_range = 0.025

        scan_params = super().focus_scan_parameters()
        return scan_params
