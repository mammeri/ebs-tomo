import sys
import gevent
import numpy as np
import time
import click

from bliss.setup_globals import *
from bliss.common import scans
from bliss.common.scans import ascan, pointscan
from bliss.common.cleanup import error_cleanup, cleanup, capture_exceptions, axis as cleanup_axis
from bliss.scanning.scan_tools import peak, goto_peak
from bliss.controllers.lima.roi import Roi
from bliss.shell.standard import umv, mv, umvr, mvr, plotselect
from bliss.config.static import get_config

from nabu.preproc.alignment import CenterOfRotation
from nabu.preproc.alignment import DetectorTranslationAlongBeam
from nabu.preproc.alignment import CameraFocus
from nabu.preproc.alignment import CameraTilt

from bliss.common.plot import plot_image


class TomoAlign():
    
    def __init__(self, name, config):
        self.__name = name
        self.__config = config
        
        self.tomo        = config.get("tomo")
        self.paper       = config.get("paper")
        
        self.slits_vgap     = config.get("slits_vgap")
        self.slits_hgap     = config.get("slits_hgap")
        self.detector_vtilt = config.get("detector_vtilt")
        self.detector_htilt = config.get("detector_htilt")
        
        
    @property
    def name(self):
        """
        A unique name for the Bliss object
        """
        return self.__name

    @property
    def config(self):
        """
        The configuration for Tomo alignment
        """
        return self.__config
        
                 
    def __info__(self):
        info_str  = f"{self.name} info:\n"
        if self.tomo != None:
            info_str += f"  tomo            = {self.tomo.name} \n"
            info_str += f"  detector        = {self.tomo.tomo_ccd.detector.name}\n"
            info_str += f"  optic           = {self.tomo.optic.description}\n"
            info_str += f"  pixel size      = {self._pixel_size()} \n"
            info_str += f"  exposure time   = {self.tomo.parameters.exposure_time} \n\n"
            
            info_str += f"  focus motor     = {self.tomo.optic.focus_motor().name} \n"
            info_str += f"  rotc motor      = {self.tomo.optic.rotc_motor().name} \n"
            info_str += f"  lateral motor   = {self.tomo.reference.ref_motors[0].name} \n\n"
        else:
            info_str += f"  tomo            = No Tomo object configured!\n"
        if self.paper != None:
            info_str += f"  alignment paper = {self.paper.name} \n"
        else:
            info_str += f"  alignment paper = No alignment pager object configured!\n"
        if self.slits_vgap != None:
            info_str += f"  slits vgap      = {self.slits_vgap.name} \n"
        else:
            info_str += f"  slits vgap      = No slits vgap object configured!\n"
        if self.slits_hgap != None:
            info_str += f"  slits hgap      = {self.slits_hgap.name} \n"
        else:
            info_str += f"  slits hgap      = No slits hgap object configured!\n"
        if self.detector_vtilt != None:
            info_str += f"  detector vtilt  = {self.detector_vtilt.name} \n"
        else:
            info_str += f"  detector vtilt  = No detector vtilt object configured!\n"
        if self.detector_htilt != None:
            info_str += f"  detector htilt  = {self.detector_htilt.name} \n"
        else:
            info_str += f"  detector htilt  = No detector htilt object configured!\n"
        return info_str

    def _pixel_size(self):
        # pixel size returned by tomo is in um
        pixel_size = self.tomo.tomo_ccd.calculate_image_pixel_size(self.tomo.optic.magnification)
        
        # pixel size for Nabu caluculations must be in mm
        pixel_size = pixel_size / 1000.0
        print ("pixel_size in mm = %g" % pixel_size)

        return pixel_size
        
    def _get_images(self, scan, detector):
        # read back the scan images aquired
        scan_images=[]
        lima_data = scan.get_data()[detector.image]
        npoints = lima_data.last_index
            
        if npoints < lima_data.buffer_max_number:
            for i in range(npoints):
                scan_images.append(lima_data.get_image(i))
            
            return scan_images
        
        else:
            ValueError("Cannot read images! Not enough images in the Lima image buffer") 
    
    #
    # Detector focus alignment
    #
    
    def focus(self, save=False, plot=False):
        # prepare the focus scan parameters
        scan_pars = self.tomo.optic.focus_scan_parameters()
        
        scan_range = scan_pars["focus_scan_range"]
        scan_steps = scan_pars["focus_scan_steps"]
        focus_type = scan_pars["focus_type"]
        focus_motor = self.tomo.optic.focus_motor()
        
        if focus_type == "Rotation" and self.tomo.optic.magnification < 0.7:
            scan_range /= 2
            scan_steps *= 2
        
        start = focus_motor.position - scan_range
        stop  = focus_motor.position + scan_range
        
        #pixel size in mm
        pixel_size = self._pixel_size()
        
        # move in paper
        print ("Move in paper")
        self.paper.IN()
        
        # Do the focus scan
        focus_scan = self.focus_scan(focus_motor, start, stop, scan_steps, 
                                     self.tomo.parameters.exposure_time , 
                                     self.tomo.tomo_ccd.detector, save=save)
        
        if focus_scan != None:
            self.focus_calc(focus_scan, focus_motor, self.tomo.tomo_ccd.detector, pixel_size, plot)
        
        ct(self.tomo.parameters.exposure_time)
        print (f"Focus motor position = {focus_motor.position}")
        
        # Move out paper 
        if click.confirm("Move out paper?", default=True):
            self.paper.OUT()
        
    
    def focus_calc(self, focus_scan, focus_motor, detector, pixel_size, plot=False):
        if focus_scan != None:
            # read back the aquired images
            foc_images = self._get_images(focus_scan, detector)
            #convert to numpy array
            foc_array= np.array(foc_images)
            
            # read back the aquired positions
            foc_pos = focus_scan.get_data()[focus_motor.name]
            
            focus_calc = CameraFocus()
            # enable calculation results plotting
            if plot == True:
                focus_calc.verbose = True
            
            focus_pos, focus_ind, tilts_vh = focus_calc.find_scintillator_tilt(foc_array, foc_pos)
            #print (focus_pos, focus_ind, tilts_vh)
            
            tilts_corr_vh_deg = - np.rad2deg(np.arctan(tilts_vh / pixel_size))
            #tilts_corr_vh_rad = - tilts_vh / pixel_size
            
            print (f"\nBest focus position:         {focus_pos}")
            print (f"Tilts vert. and hor. in deg: {tilts_corr_vh_deg}\n")
    
            if click.confirm("Move to the best focus position?", default=True):
                # Always move to the best focus from the bottom
                mv(focus_motor, foc_pos[0])
                # move focus motor to maximum
                mv(focus_motor, focus_pos)
            
            # Kill the plot window when it was requested
            if plot == True:
                # workaround for nabu plotting problem
                plt.close('all')
                

    def focus_scan(self, focus_motor, start, stop, steps, expo_time, detector, save=False):
        # prepare roi counters for statistics
        focus_roi = Roi(0,0, detector.image.roi[2], detector.image.roi[3])
        
        det_rois = detector.roi_counters
        det_rois["focus_roi"] = focus_roi
        
        std = detector.roi_counters.get_single_roi_counters("focus_roi").std
        counters = (std, detector.image)
        
        # clean-up: move back focus motor, delete roi counters, move out paper
        restore_list = (cleanup_axis.POS,)
        with cleanup(*[focus_motor, ], restore_list=restore_list):
            with capture_exceptions(raise_index=0) as capture:
                with capture():
                    # execute focus scan
                    scan = ascan(focus_motor, start, stop, steps, expo_time, counters, title="focus scan", save=save)
                        
                    # get the position on the maximum of the standard deviation
                    pos_std_max = peak()
                    
                    # delete roi counters
                    del det_rois["focus_roi"]
                    
                    return scan
                
                # test if an error has occured
                if len(capture.failed) > 0:
                    # delete roi counters
                    del det_rois["focus_roi"]
                    
                    return None

            
    #
    # Lateral alignment and camera tilt
    # 
    
    def align(self, save=False, plot=False):
        # move rotation axis to 0
        umv (self.tomo.rotation_axis, 0)
        
        # take dark image
        dark_n = 1
        dark_scan = self.tomo.dark_scan(dark_n, self.tomo.parameters.exposure_time, save=save)
        # read back the aquired images
        dark_images = self._get_images(dark_scan, self.tomo.tomo_ccd.detector)
        dark_image  = dark_images[0]
        
        # update reference motor position to current values
        self.tomo.update_reference_positions()
        
        # take reference image
        ref_n = 1
        ref_scan = self.tomo.ref_scan(ref_n, self.tomo.parameters.exposure_time, save=save)
        # read back the aquired images
        ref_images = self._get_images(ref_scan, self.tomo.tomo_ccd.detector)
        ref_image  = ref_images[0]
        
        #pixel size in mm
        pixel_size = self._pixel_size()
        
        # do alignment scan
        al_scan = self.align_scan(self.tomo.rotation_axis,
                                  self.tomo.parameters.exposure_time, 
                                  self.tomo.tomo_ccd.detector, save=save)
        
        # read back the aquired images
        al_images = self._get_images(align_scan, detector)
        
        # works only with on motor today!
        lateral_motor = self.tomo.reference.ref_motors[0]
        rotc = self.tomo.optic.rotc_motor()
        
        self.align_calc(al_images, dark_image, ref_image, 
                        lateral_motor, rotc, 
                        self.tomo.tomo_ccd.detector, pixel_size,
                        self.tomo.parameters.source_sample_distance, 
                        self.tomo.parameters.sample_detector_distance,
                        plot)
        
        #update reference motor position to current values
        self.tomo.update_reference_positions()
        
        ct(self.tomo.parameters.exposure_time)
        print (f"Lateral alignment motor position = {lateral_motor.position}")
        print (f"Camera tilt position {rotc.name} = {rotc.position}")
    
    
    def align_calc(self, al_images, dark_image, ref_image,
                   lateral_motor, rotc_motor, 
                   detector, pixel_size,
                   source_sample_distance, 
                   sample_detector_distance, plot=False):
            
        # prepare the images
        radio0   = (al_images[0].astype(float) - dark_image.astype(float)) / (ref_image.astype(float)- dark_image.astype(float))
        radio180 = (al_images[1].astype(float) - dark_image.astype(float)) / (ref_image.astype(float)- dark_image.astype(float))
        
        # flip the radio180 for the calculation
        radio180_flip = np.fliplr(radio180.copy())
        
        #plot_image(radio0)
        #plot_image(radio180_flip)
        
        # calculate the lateral correction for the rotation axis 
        # and the camera tilt with line by line correlation
        tilt_calc = CameraTilt()
        # enable calculation results plotting
        if plot == True:
            tilt_calc.verbose=True
        
        pixel_cor, camera_tilt = tilt_calc.compute_angle(radio0, radio180_flip)
        print("CameraTilt: pixel_cor = %f" % pixel_cor)

        #distance from source to sample   = L1(mm)
        #distance from source to detector = L2(mm)
        #size of the image pixel          = s(mm/pixel)
        #dmot(mm) = L1/L2*s*pixel_cor
        cor_factor = source_sample_distance / \
                     (source_sample_distance + sample_detector_distance) * \
                      pixel_size
        pos_cor = cor_factor * pixel_cor
        
        print (f"\nLateral alignment position correction in pixel: {pixel_cor}")
        print (f"Lateral alignment position correction in mm:    {pos_cor}")
        print (f"Camera tilt in deg: {camera_tilt}\n")   
        
       
        if click.confirm("Apply the lateral position correction and the camera tilt?", default=True):
            # apply lateral correction
            umvr (lateral_motor, pos_cor)
            # apply tilt correction
            umvr (rotc_motor, camera_tilt)
            
        # Kill the plot window when it was requested
        if plot == True:
            # workaround for nabu plotting problem
            plt.close('all')
        
    
    def align_scan(self, rotation_motor, expo_time, detector, save=False):
        # clean-up: move back rot
        restore_list = (cleanup_axis.POS,)
        with cleanup(rotation_motor, restore_list=restore_list):
            # scan rot
            rot_pos_list=[0.0, 180.0]
            scan = pointscan(rotation_motor, rot_pos_list, expo_time, detector.image, title="align scan", save=save)
            
            return scan
         
                
    #
    # Camera stage tilt alignment
    # 
    
    def alignxc(self, start=100, stop=800, steps=10, save=False, plot=False):
        
        #pixel size in mm
        pixel_size = self._pixel_size()
        
        # slit gap to use
        if pixel_size < 0.003:      #3um
            slit_gap = 0.2
        else:
            slit_gap = 1.0
            
        alxc_scan = self.alignxc_scan(self.tomo.detector_axis, start, stop, steps, 
                                      self.tomo.parameters.exposure_time, 
                                      self.tomo.tomo_ccd.detector, 
                                      self.slits_vgap, self.slits_hgap, slit_gap, 
                                      save=save)
        # read back the aquired images
        alxc_images = get_images(alxc_scan, self.tomo.tomo_ccd.detector)
        #print(alxc_images)
        
        # read back the motor positions 
        alxc_pos = alxc_scan.get_data()[self.tomo.detector_axis.name]
        #print(al_pos)
        
        # take dark image
        dark_n = 1
        dark_scan = self.tomo.dark_scan(dark_n, self.tomo.parameters.exposure_time, save=save)
        # read back the aquired images
        dark_images = get_images(dark_scan, tomo.tomo_ccd.detector)
        dark_image  = dark_images[0]
        
        self.alignxc_calc(alxc_images, alxc_pos, dark_image, plot)
        
        

    def alignxc_calc(self, alxc_images, alxc_pos, dark_image, plot=False):
        
        # substract dark from images
        for i in al_images:
            i = i.astype(float) - dark_image.astype(float)
        #convert to numpy array
        al_array= np.array(al_images)
            
        # calculate XC tilt shifts
        # the scan positions and the pixel_size must have the same units!
        
        tr_calc = DetectorTranslationAlongBeam()
        # enable calculation results plotting
        if plot == True:
            tr_calc.verbose=True
        
        shifts_v, shifts_h = tr_calc.find_shift(al_array, al_pos)
        
        # pixel size should be im mm
        tilt_v_deg = - np.rad2deg(np.arctan(shifts_v * pixel_size / 1000.0))
        tilt_h_deg = - np.rad2deg(np.arctan(shifts_h * pixel_size / 1000.0))
        print (f"\nVertical   tilt in deg (thy): {tilt_v_deg}")
        print (f"Horizontal tilt in deg (thz): {tilt_h_deg}\n")
        
        # apply to the motors
        # needs a real object to configure motors!!!!!
    
        # Kill the plot window when it was requested
        if plot == True:
            if click.confirm("Close the plot window?", default=True):
                # workaround for nabu plotting problem
                plt.close('all')
    
    
    def alignxc_scan(self, xc, start, stop, steps, expo_time, detector, 
                     svg, shg, slit_gap, save=False):  
        
        # save current slit positions
        vg_pos = svg.position
        hg_pos = shg.position
        print (f"Current slit gap positions: vertical={vg_pos}, horizontal={hg_pos}")
        print (f"Current xc positions:       xc={xc.position}")
        
        # clean-up: move back xc motor, open_slits
        restore_list = (cleanup_axis.POS,)
        with error_cleanup(*[xc, svg, shg], restore_list=restore_list):
            # close the slits
            umv(svg, slit_gap, shg, slit_gap)
        
            # scan xc
            scan = ascan(xc, start, stop, steps, expo_time, detector.image, title="align xc scan", save=save)
        
            # open the slits
            umv(svg, vg_pos, shg, hg_pos)

            # return images
            return scan

