from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info,log_debug

class TomoSequencePreset:
    """
    Class for tomo sequence preset object.
    The class implements methods which allow to do some actions during tomo acquisition prepare, 
    before running tomo acquisition and at the end of tomo acquisition.
    
    ***Attributes***
    name : string
        class name
    config : dict
        contains tomo configuration
    """
    
    def __init__(self, name, config):
        
        # init logging
        self.log_name = name+'.sequence_preset'
        global_map.register(self, tag=self.log_name)
        log_info(self,"__init__() entering")
        
        self.name = name
        self.config = config
        
        log_info(self,"__init__() leaving")
        
    def prepare(self, tomo):
        """
        This methods is called during tomo acquisition prepare.
        """
        pass
        
    def start(self):
        """
        This methods is called before running tomo acquisition.
        """
        pass
        
    def stop(self):
        """
        This methods is called at the end of tomo acquisition.
        """
        pass
