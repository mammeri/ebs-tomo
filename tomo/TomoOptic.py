import sys
import gevent
import numpy as np
import time

import PyTango

import bliss
from bliss import global_map
from bliss.common import session
from bliss.common.logtools import log_info, log_debug
from bliss.shell.cli.user_dialog import UserChoice, Container, UserYesNo, UserFloatInput
from bliss.shell.cli.pt_widgets import display, BlissDialog

from tomo.TomoParameters import TomoParameters


class TomoOptic(TomoParameters):
    """
    Base class for all tomo optic objects.
    The class implements all the standard methods to be implemented for every optic.
    
    **Attributes**:
    scintillator : string
        Name of the scintillator associated with the optic
    image_flipping_hor : boolean
        Implied horizontal image flipping by the objective
    image_flipping_vert : boolean
        Implied vertical image flipping by the objective
    
    rotc_motor : Bliss Axis object
        The camera rotation motor for the ojective
    focus_motor : Bliss Axis object
        The focus motor for the ojective
    focus_type : string
        The motion type of the focus motor. Can be "translation" or "rotation".
    focus_scan_range : float
        The scan range for the focus scan
    focus_scan_steps : int
        The number of scan steps for the focus scan. 
    focus_lim_pos : float
        Positive soft limit for the focus motor
    focus_lim_neg : float
        Negative soft limit for the focus motor
    manual_pixel_size : boolean
        Flag used to specify that pixel size is set manually
    unbinned_pixel_size : float
        User pixel size value
    image_pixel_size : float
        Pixel size value calculated with detector field of view and optic magnification
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    """

    def __init__(self, name, config, param_name=None, param_defaults=None):
        # init logging
        self.log_name = name + ".optic"
        global_map.register(self, tag=self.log_name)
        log_info(self, "__init__() entering")

        self.__name = name
        self.__config = config
        
        param_defaults["manual_pixel_size"] = False
        param_defaults["unbinned_pixel_size"] = 0
        param_defaults["image_pixel_size"] = 0
        param_defaults["scintillator"] = 'None'

        # Initialise the TomoParameters class
        super().__init__(param_name, param_defaults)
        

        try:
            self.__scintillators = config["scintillator"]
        except:
            self.__scintillators = None

        try:
            self.__image_flipping_hor = config["image_flipping_hor"]
        except:
            self.__image_flipping_hor = False
        try:
            self.__image_flipping_vert = config["image_flipping_vert"]
        except:
            self.__image_flipping_vert = False

        try:
            self.__rotc_mot = config["rotc_motor"]
        except:
            self.__rotc_mot = None

        try:
            self.__focus_mot = config["focus_motor"]
        except:
            self.__focus_mot = None

        try:
            self.__focus_type = config["focus_type"]
        except:
            self.__focus_type = "unknown"
        try:
            self.__focus_scan_range = config["focus_scan_range"]
        except:
            self.__focus_scan_range = 0
        try:
            self.__focus_scan_steps = config["focus_scan_steps"]
        except:
            self.__focus_scan_steps = 0
        try:
            self.__focus_lim_pos = config["focus_lim_pos"]
        except:
            self.__focus_lim_pos = 0
        try:
            self.__focus_lim_neg = config["focus_lim_neg"]
        except:
            self.__focus_lim_neg = 0
        try:
            self.__scintillator = config["scintillator"]
        except:
            self.__scintillator = None
            
        self.__manual_pixel_size = self.parameters.manual_pixel_size
        self.__unbinned_pixel_size = self.parameters.unbinned_pixel_size
        self.__image_pixel_size = self.parameters.image_pixel_size

        log_info(self, "__init__() leaving")

    def manual_pixel_size_setup(self):
        """
        User dialog which allows to enter manually pixel size in micrometers
        """
        dlg1 = UserYesNo(label="Do you want to set pixel size manually?")
        ret = display(dlg1, title="Manual Pixel Size")

        if ret is True:            
            self.manual_pixel_size = True
            dlg1 = UserFloatInput(
                label="Unbinned pixel size in um", defval=self.unbinned_pixel_size
            )
            ct1 = Container([dlg1], title="Unbinned Pixel Size")
            ret = BlissDialog([[ct1]], title="Pixel Size Setup").show()

            if ret is not False:
                self.unbinned_pixel_size = ret[dlg1]
                return True
            else:
                self.manual_pixel_size = False
                return False
        else:
            self.manual_pixel_size = False
            return False

    def correct_pixel_size_setup(self, detector):
        """
        User dialog which allows to modify pixel size (entered manually by user or deduced from optic magnification)
        """
        self.calculate_image_pixel_size(detector)
        dlg1 = UserYesNo(
            label=f"Do you want to correct this value? ({self.image_pixel_size} um)"
        )
        ret = display(dlg1, title="Correct Pixel Size")

        if ret is True:
            dlg1 = UserFloatInput(
                label="Corrected unbinned pixel size in um",
                defval=self.unbinned_pixel_size,
            )
            ct1 = Container([dlg1], title="Corrected Pixel Size")
            ret = BlissDialog([[ct1]], title="Pixel Size Setup").show()

            if ret is not False:
                self.unbinned_pixel_size = ret[dlg1]
            else:
                self.unbinned_pixel_size = (
                    self.image_pixel_size / detector.proxy.image_bin[1]
                )

        self.image_pixel_size = self.unbinned_pixel_size * detector.proxy.image_bin[1]

    def calculate_image_pixel_size(self, detector):
        """
        Calculate the sample image pixel size.
        Needs detector parameters and the optics magnification
        """
        ccd_ps = detector.proxy.camera_pixelsize[0]

        if detector.camera_type.lower() == "frelon":
            # conversion in microns
            ccd_ps *= 1e6

        if self.manual_pixel_size:
            self.magnification = round(ccd_ps / self.unbinned_pixel_size, 3)
        else:
            self.unbinned_pixel_size = round(ccd_ps / self.magnification, 3)

        ccd_bin = detector.proxy.image_bin[1]
        self.image_pixel_size = self.unbinned_pixel_size * ccd_bin

    def select_scintillator_setup(self):
        """
        User dialog which allows to select a scintillator among list of scintillators defined in optic config file
        """
        if self.__scintillators is not None:
            value_list = []
            for i in self.__scintillators:
                value_list.append((i, str(i)))

            # get the actual scintillator name as default
            default1 = 0
            for i in range(0, len(value_list)):
                if (
                    self.scintillator is not None
                    and self.scintillator == value_list[i][0]
                ):
                    default1 = i

            dlg1 = UserChoice(values=value_list, defval=default1)
            ct1 = Container([dlg1], title="Scintillator")
            ret = BlissDialog([[ct1]], title="Scintillator Setup").show()

            if ret != False:
                # get the scintillator chosen
                self.scintillator = ret[dlg1]
        else:
            self.scintillator = "None"

    @property
    def name(self):
        """
        The name of the optic
        """
        return self.__name

    @property
    def config(self):
        """
        The configuration of the optic
        """
        return self.__config

    @property
    def description(self):
        """
        The name string of the current optic.
        Musst be implemented for every optic.
        """
        pass

    @property
    def type(self):
        """
        The class name of the optic.
        Musst be implemented for every optic.
        """
        pass

    @property
    def scintillator(self):
        """
        Returns the name of the scintillator associated with the optic
        """
        return self.__scintillator

    @scintillator.setter
    def scintillator(self, value):
        """
        Sets the name of the scintillator associated with the optic
        """
        self.__scintillator = value
        self.parameters.scintillator = value

    @property
    def manual_pixel_size(self):
        """
        Returns if pixel size is manually set or not
        """
        return self.__manual_pixel_size

    @manual_pixel_size.setter
    def manual_pixel_size(self, value):
        """
        Sets if pixel size is manually set or not
        """
        self.__manual_pixel_size = value
        self.parameters.manual_pixel_size = value

    @property
    def unbinned_pixel_size(self):
        """
        Returns pixel size value without binnning
        """
        return self.__unbinned_pixel_size

    @unbinned_pixel_size.setter
    def unbinned_pixel_size(self, value):
        """
        Sets pixel size without binnning to value
        """
        self.__unbinned_pixel_size = value
        self.parameters.unbinned_pixel_size = value

    @property
    def image_pixel_size(self):
        """
        Returns pixel size value without binnning
        """
        return self.__image_pixel_size

    @image_pixel_size.setter
    def image_pixel_size(self, value):
        """
        Sets pixel size without binnning to value
        """
        self.__image_pixel_size = value
        self.parameters.image_pixel_size = value

    @property
    def magnification(self):
        """
        Returns the magnification of the current optic used.
        Musst be implemented for every optic.
        """
        pass

    @magnification.setter
    def magnification(self, value):
        """
        Sets the magnification of the current optic used
        Musst be implemented for every optic.
        """
        pass

    @property
    def image_flipping(self):
        """
        Returns the implied horizontal and vertical image flipping as a list
        """
        return [self.__image_flipping_hor, self.__image_flipping_vert]

    def optic_setup(self):
        pass

    def setup(self, detector):
        """
        Set-up the optic
        Musst be implemented for every optic
        """

        self.optic_setup()
        self.manual_pixel_size_setup()

        self.select_scintillator_setup()
        self.correct_pixel_size_setup(detector)

    def status(self):
        """
        Prints the current ojective in use and its magnification.
        If an optics cannot be determined, the reason gets printed.
        Musst be implemented for every optic
        """
        pass

    @property
    def objective(self):
        """
        Reads and sets the current objective.
        Must be implemented for optics with more than one objective.
        """
        return 1

    @objective.setter
    def objective(self, value):
        """
        Moves to an objective
        Must be implemented for optics with more than one objective.
        """
        pass

    def rotc_motor(self):
        """
        Returns the Bliss Axis object of the rotation motor to be used for the current objective
        """

        return self.__rotc_mot

    def set_focus_motor(self, motor):
        """
        Sets the current focus motor for optics with several focus motors
        """
        self.__focus_mot = motor

    def focus_motor(self):
        """
        Returns the Bliss Axis object of the focus motor to be used for the current optic
        """
        return self.__focus_mot

    def focus_scan_parameters(self):
        """
        Returns a dictionary with the paramters for a focus scan
        """
        if self.__focus_mot == None:
            raise ValueError("No focus motor defined for the optic!")

        scan_params = {}
        scan_params["focus_type"] = self.__focus_type
        scan_params["focus_scan_range"] = self.__focus_scan_range
        scan_params["focus_scan_steps"] = self.__focus_scan_steps
        scan_params["focus_lim_pos"] = self.__focus_lim_pos
        scan_params["focus_lim_neg"] = self.__focus_lim_neg

        return scan_params

    def __info__(self):
        info_str = f"{self.name} optic info:\n"
        info_str += f"  description   = {self.description}\n"
        info_str += f"  objective     = {self.objective} \n"
        info_str += f"  magnification = {self.magnification} \n"
        if self.rotc_motor() == None:
            info_str += f"  rotc motor    = None \n"
        else:
            info_str += f"  rotc motor    = {self.rotc_motor().name} \n"
        if self.focus_motor() == None:
            info_str += f"  focus motor   = None \n"
        else:
            info_str += f"  focus motor   = {self.focus_motor().name} \n"
        info_str += f"  focus scan    = {self.focus_scan_parameters()} \n"

        return info_str
