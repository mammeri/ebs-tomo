import sys
import gevent
import numpy as np
import os
import glob

from bliss import global_map, setup_globals, current_session
from bliss.common.logtools import log_info,log_debug

from bliss.scanning.scan_saving import ScanSaving
from bliss.setup_globals import *

from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog

from tomo.TomoParameters import TomoParameters


class TomoSaving(TomoParameters):
    """
    Class for tomo saving object.
    The class implements methods to handle saving in tomo acquisition.
    
    ***Attributes***
    name : string
        class name
    tomo_name : string
        name of tomo object
    tomo_ccd : Tomo ccd object
        contains methods to handle detectors in tomo acquisition
    """
    def __init__(self, tomo_name, tomo_config, tomo_ccd, *args, **kwargs):
        # init logging
        self.name = tomo_name+".saving"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")
        
        self.tomo_name = tomo_name
        self.tomo_ccd  = tomo_ccd
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.tomo_name+':saving_parameters'
        saving_defaults = {}
        saving_defaults['lbs_active'] = False
        saving_defaults['comment']    = ''
        
        # Initialise the TomoParameters class
        super().__init__(param_name, saving_defaults)
  
        log_info(self,"__init__() leaving")
    
    
    def setup(self):
        """
        Set-up scan saving parameters.
        User can select storage space (visitor, inhouse, tmp) and storage system (nfs, lbs).
        Then user defines proposal name, collection name and dataset name.
        There is also the possiblity to add a comment that will be stored in tomo metadata.
        """
        scan_saving_config = current_session.scan_saving.scan_saving_config
        scan_saving        = current_session.scan_saving
        
        values = [("visitor","visitor"),("inhouse","inhouse"),("tmp","tmp")]
        proposal_type = scan_saving.proposal_type
        # get the current proposal type as default value
        index = 0
        for i in values:
            if proposal_type == i[0]:
                type_index = index
                break
            else:
                index = index + 1
        
        dlg_disk = UserChoice(values=values, defval=type_index)
        ct1 = Container( [dlg_disk], title="Storage Space")
        i = 0
        mp_values = []
        for mount_point in scan_saving.mount_points:
            if mount_point != '':
                mp_values.append((i,mount_point))
                i+=1
            
        if len(mp_values) > 1:
            defval = [index for index, value in mp_values if value == scan_saving.mount_point]
            dlg_mount_point = UserChoice(values=mp_values, defval=defval[0])
            ct2 = Container( [dlg_mount_point], title="Mount Points")
            ret = BlissDialog( [ [ct1], [ct2] ] , title='Saving Setup').show()
        else:
            ret = BlissDialog( [ [ct1] ] , title='Saving Setup').show()
        

        # returns False on cancel
        if ret != False:
            proposal_type = ret[dlg_disk]
            if len(mp_values) > 1:
                scan_saving.mount_point = mp_values[ret[dlg_mount_point]][1]
            
            if proposal_type == 'visitor':
                if len(mp_values) > 1:
                    index = [index for index, value in mp_values if value == scan_saving.mount_point]
                    base_path = scan_saving_config["visitor_data_root"][scan_saving.mount_point].replace("{beamline}", scan_saving.beamline)
                else:
                    base_path = scan_saving_config["visitor_data_root"].replace("{beamline}", scan_saving.beamline)
                proposal_tips = '(user experiment)'    
            elif proposal_type == 'inhouse':
                if len(mp_values) > 1:
                    index = [index for index, value in mp_values if value == scan_saving.mount_point]
                    base_path = scan_saving_config["inhouse_data_root"][scan_saving.mount_point].replace("{beamline}", scan_saving.beamline)
                else:    
                    base_path = scan_saving_config["inhouse_data_root"].replace("{beamline}", scan_saving.beamline)
                proposal_tips = '(Empty string for default proposal name or must start with blc or ih)'
            elif proposal_type == 'tmp':
                base_path = scan_saving_config["tmp_data_root"].replace("{beamline}", scan_saving.beamline)
                proposal_tips = '(Musst start with test, temp or tmp)'
            
            current_proposal = scan_saving.proposal
            current_collection   = scan_saving.collection.name
            current_dataset  = scan_saving.dataset
            current_comment  = self.parameters.comment
            
            # if the base path has changed, clean-up proposal, collection and dataset
            if scan_saving.base_path != base_path:
                current_proposal =''
                current_collection   =''
                current_dataset  =''
                current_comment  =''
            
            dlg_path      = UserMsg(label=f"Base Path {base_path}")
            v_proposal    = Validator(self.proposal_validator, proposal_type)
            dlg_proposal  = UserInput(label="Proposal Name ", defval=current_proposal, validator=v_proposal)
            dlg_tips      = UserMsg(label=proposal_tips)
            dlg_collection    = UserInput(label="Collection Name", defval=current_collection)
            dlg_dataset   = UserInput(label="Dataset Name", defval=current_dataset)
            dlg_comment   = UserInput(label="Comment", defval=current_comment)
            
            ret = BlissDialog( [ [dlg_path], [dlg_proposal], [dlg_tips], [dlg_collection], [dlg_dataset], [dlg_comment]] , title='Saving Setup').show()
            
            # returns False on cancel
            if ret != False:
                # get main parameters
                new_proposal = ret[dlg_proposal]
                new_collection = ret[dlg_collection]
                new_dataset = ret[dlg_dataset]
                new_comment = ret[dlg_comment]
        
                self.configure (proposal_type, new_proposal, new_collection, new_dataset, 
                                new_comment)
    
    
    def proposal_validator(self, str_input, proposal_type):
        """
        Verifies proposal name is consistent with proposal type.
        """
        str_input = str_input.lower()
        
        if proposal_type == 'tmp':
            if not str_input.startswith('tmp') and not str_input.startswith('temp') and not str_input.startswith('test'):
                raise ValueError("Proposal name does not start with test, temp or tmp")
                
        if proposal_type == 'inhouse':
            if not str_input=='' and not str_input.startswith('blc') and not str_input.startswith('ih') and not str_input.startswith(current_session.scan_saving.beamline):
                raise ValueError("Proposal name is not empty and does not start with blc or ih")
                
        if proposal_type == 'visitor':
            if str_input.startswith('tmp') or str_input.startswith('temp') or str_input.startswith('test') or \
               str_input.startswith('blc') or str_input.startswith('ih'):
                raise ValueError("Proposal name should neither start with \nblc or ih (inhouse) nor test, temp or tmp")
    
        return str_input
        
    
    def configure(self, proposal_type, proposal=None, collection=None, dataset=None, comment=None):
        """
        Configures proposal name, collection name, dataset name and the path extension when using a LBS.
        """
        scan_saving_config = current_session.scan_saving.scan_saving_config
        scan_saving        = current_session.scan_saving
        
        log_info(self,"configure() entering")
        
        if proposal_type == 'visitor':
            root_path = "visitor_data_root"
        elif proposal_type == 'inhouse':
            root_path = "inhouse_data_root"
        elif proposal_type == 'tmp':
            root_path = "tmp_data_root"
           
        if scan_saving.mount_point == 'lbs':
            self.check_lbs_free_space()

        # configure data policy
        if proposal != None:
            if scan_saving.proposal != proposal:
                self.proposal_validator (proposal, proposal_type)
                scan_saving.newproposal(proposal)
        
        if collection != None:
            if scan_saving.collection.name != collection:
                scan_saving.newcollection(collection)
                
        if dataset != None:           
            if scan_saving.dataset != dataset:
                scan_saving.newdataset(dataset)
                
        if comment != None:
            self.parameters.comment = comment
                    
        # Apply the correct directory mapping if specified for the detector
        if len(self.tomo_ccd.detector.directories_mapping) > 0:
            for proposal_type in self.tomo_ccd.detector.directories_mapping_names:
                if proposal_type in scan_saving.base_path:
                    self.tomo_ccd.detector.select_directories_mapping(proposal_type)    
        
        log_info(self,"configure() leaving")

    def check_lbs_free_space(self):
        """
        Verifies storage free space is sufficient on lbs pc.
        A threshold value is used for the check. 
        If memory used is > 80% and current saving directory has scans inside, they are transferred 
        towards nfs system ortherwise raises an exception. 
        """
        scan_saving = current_session.scan_saving
        current_dir = scan_saving.get_path()
        non_lbs_path = '/' + '/'.join(current_dir.split('/')[2:])
        
        if self.parameters.lbs_active:
            
            df = os.popen('df -h /lbsram').readlines()
            mem_use = [item[:-1] for item in df[1].split(' ') if '%' in item]
            mem_use = int(mem_use[0])

            if mem_use > 80:
                mem_available = 100 - mem_use
                msg = f"\n\033[1mIt remains only {mem_available}% of free space on lbs\033[0m\n"
                msg += "\033[1mPlease free up some space on lbs pc\033[0m\n" 
                raise Exception(msg)
        
        
