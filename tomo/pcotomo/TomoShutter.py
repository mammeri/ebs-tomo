import sys
import gevent
import numpy as np
import time

import PyTango

import bliss
from bliss import global_map
from bliss.common import session
from bliss.common.standard import *
from bliss.common.logtools import log_info,log_debug

from tomo.TomoParameters import TomoParameters

from enum import Enum
class ShutterType(Enum):
    CCD  = 1
    SOFT = 2
    NONE = 3

class TomoShutter(TomoParameters):
    def __init__(self, tomo_name, tomo_config, tomo_ccd, *args, **kwargs):
        # init logging
        self.name = tomo_name+".shutter"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")
        
        #print(tomo_config)
        config = tomo_config["shutter"]
        
        self.tomo_ccd  = tomo_ccd
        self.det_proxy = self.tomo_ccd.det_proxy
        self.tomo_name = tomo_name
        
        self.beam_shutter = config.get("beam_shutter",None)
        self.soft_shutter = config.get("soft_shutter",None)
        self.ccd_shutter  = config.get("ccd_shutter",False)
        self.ccd_shutter_time = config.get("ccd_shutter_time",0)
        self.shutter_used = ShutterType.CCD
         
        if self.ccd_shutter == True:
            if self.tomo_ccd.has_shutter() == False:
                self.shutter_used = ShutterType.SOFT
                
        if self.ccd_shutter == False and self.soft_shutter is not None:
            self.shutter_used = ShutterType.SOFT
        
        if self.soft_shutter == None:
            self.shutter_used = ShutterType.NONE
        
        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values
        
        param_name = self.tomo_name+':shutter_parameters'
        shutter_defaults = {}
        shutter_defaults['dark_images_with_beam_shutter']  = False
        shutter_defaults['No_ccd_shutter_in_FTM_mode']     = False
        
        # Initialise the TomoParameters class
        super().__init__(param_name, shutter_defaults)
        
        log_info(self,"__init__() leaving")
    
    
    
    def fast_shutter_open(self):
        """
        Opens fast shutter
        """
        log_info(self,"fast_shutter_open() entering")
        
        if self.shutter_used == ShutterType.CCD:
            self.tomo_ccd.autmatic_shutter_mode(True)
            self.tomo_ccd.set_shutter_time(self.parameters.ccd_shutter_time)
        else:
            if self.shutter_used == ShutterType.SOFT:
                self.soft_shutter.open()
        
        log_info(self,"fast_shutter_open() leaving")
        
    
    
    def fast_shutter_close(self):
        """
        Closes fast shutter
        """
        log_info(self,"fast_shutter_close() entering")
        
        if self.shutter_used == ShutterType.CCD:
            self.tomo_ccd.autmatic_shutter_mode(False)
            self.tomo_ccd.set_shutter_time(0.0)
        else:
            if self.shutter_used == ShutterType.SOFT:
                self.soft_shutter.close()
        
        log_info(self,"fast_shutter_close() leaving")
        
    
    
    def fast_shutter_closing_time(self):
        """
        Returns fast shutter closing time
        """
        log_info(self,"fast_shutter_closing_time() entering")
        
        if self.shutter_used == ShutterType.CCD:
            return self.parameters.ccd_shutter_time
        else:
            if self.shutter_used == ShutterType.SOFT:
                return self.soft_shutter.closing_time
            else: 
                return 0.0
        
        log_info(self,"fast_shutter_closing_time() leaving")
        
    
    
    def fast_shutter_used(self):
        """
        Returns fast shutter name
        """
        log_info(self,"fast_shutter_used() entering")
        
        return self.shutter_used.name
        
        log_info(self,"fast_shutter_used() leaving")
    
    
    
    def beam_shutter_open(self):
        """
        Opens beam shutter 
        """
        log_info(self,"beam_shutter_open() entering")
        
        self.beam_shutter.open()
        
        log_info(self,"beam_shutter_open() leaving")
        
        
        
    def beam_shutter_close(self):
        """
        Closes beam shutter
        """
        log_info(self,"beam_shutter_close() entering")
        
        self.beam_shutter.close()
        
        log_info(self,"beam_shutter_close() leaving")
      
      
        
    def beam_shutter_state(self):
        """
        Returns beam shutter state
        """
        log_info(self,"beam_shutter_state() entering")
        
        self.beam_shutter.state
        
        log_info(self,"beam_shutter_state() leaving")
        
        
        
    def dark_shutter_open(self):
        """
        Opens dark shutter 
        """
        
        log_info(self,"dark_shutter_open() entering")
        
        if self.parameters.dark_images_with_beam_shutter == True:
            self.beam_shutter_open()
        else:
            self.fast_shutter_open()
            
        log_info(self,"dark_shutter_open() leaving")
        
        
        
    def dark_shutter_close(self):
        """
        Closes beam shutter 
        """
        log_info(self,"dark_shutter_close() entering")
        
        if self.parameters.dark_images_with_beam_shutter == True:
            self.beam_shutter_close()
        else:
            self.fast_shutter_close()
            
        log_info(self,"dark_shutter_close() leaving")

