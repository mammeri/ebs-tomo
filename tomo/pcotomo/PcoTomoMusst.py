from bliss.setup_globals import *
from tomo.TomoMusst import TomoMusst,TriggerType
from tomo.pcotomo.PcoTomoMusstAcquisition import PcoTomoMusstAcquisitionSlave, PcoTomoMusstAcquisitionMaster
from tomo.pcotomo.PcoTomoTools import MusstWaitTurnCalc,MusstStartTimeCalc,MusstWaitTimeCalc
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave, MusstAcquisitionMaster
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave
import time

class PcoTomoMusst(TomoMusst):
    
    def __init__(self, tomo_name, card, motor, sync_shut_time):
        TomoMusst.__init__(self, tomo_name, card, motor)
        self.sync_shutter = False
        if sync_shut_time > 0:
            self.sync_shutter = True
        self.sync_shut_time = sync_shut_time
        
        self.tmrcfg = None

    def prepare(self,start_pos,trange,tomo_n,time,ntomo,nloop,nwait,sync_shut_angle,user_output,noread, scan_time, trigger_type=TriggerType.TIME):
        self.card.ABORT
        self.sync_motor()
        
        #if nwait >= 0:
            #self.tmrcfg = self.card.TMRCFG
            #self.set_timer_clock(scan_time)
        self.card.TMRCFG = '10MHZ'
        
        if nloop > 1:
            delta = (start_pos + trange)%360
            if delta > 0:
                delta = 360 - delta  
            if trigger_type == TriggerType.TIME:
                delta = trange + delta
            if nwait < 0:
                iotrig = 1
                if nwait < -1:
                    delta += abs(nwait) * 360 
            else:
                delta += nwait * 360
                iotrig = 0
        else:
            delta = 0
            iotrig = 0
                
        if trigger_type == TriggerType.POSITION:
            scan_step_size = trange / tomo_n
            step = int(scan_step_size*self.motor.steps_per_unit)
            mode = 0
        if trigger_type == TriggerType.TIME:
            scan_point_time = time / tomo_n
            step = int(scan_point_time*self.card.get_timer_factor())
            mode = 1
            
    
        delta = int(delta*self.motor.steps_per_unit)
        sturn = int(360*self.motor.steps_per_unit)
        nloop = int(nloop)
        nimg = int(tomo_n)
            
        if user_output > 0:
            if nloop > 1:
                out_on_loop = int(user_output)
                out_on_img = 0
            else:
                out_on_loop = 0 
                out_on_img = int(tomo_n/ntomo*user_output)
        else:
            out_on_loop = 0 
            out_on_img = 0 
        
        if sync_shut_angle > 0:
            sync_shut_angle = start_pos - sync_shut_angle 
            
        start = int(start_pos*self.motor.steps_per_unit*self.motor.sign)
        
        shut = int(sync_shut_angle*self.motor.steps_per_unit)

        musst_vars={'V_ZERO':0,
                    'V_SHUT':shut,
                    'V_TOMO':start,
                    'V_STEP':step,
                    'V_DELTA':delta,
                    'V_STURN':sturn,
                    'V_NIMG':nimg,
                    'V_MODE':mode,
                    'V_NLOOP':nloop,
                    'V_IOTRIG':iotrig,
                    'V_OUT_ON_LOOP':out_on_loop,
                    'V_OUT_ON_IMG':out_on_img
                    }
        
        program_template_replacement= {'$MOTOR_CHANNEL$':'CH%d' %self.mot_chan}
        if noread:
            # MUSST acquisition master
            musst_master = MusstAcquisitionMaster(self.card,
                                                  program='pcotomo.mprg',
                                                  program_start_name='PCOTOMO',
                                                  program_abort_name='PCOTOMO_CLEAN',
                                                  program_template_replacement=program_template_replacement,
                                                  vars=musst_vars)
        
            # MUSST acquisition slave                                  
            musst_slave = MusstAcquisitionSlave(self.card, store_list=self.storelist)    # store encoder and timer data
        
        else:
            musst_master = PcoTomoMusstAcquisitionMaster(self.card,
                                                         program='pcotomo.mprg',
                                                         program_start_name='PCOTOMO',
                                                         program_abort_name='PCOTOMO_CLEAN',
                                                         program_template_replacement=program_template_replacement,
                                                         vars=musst_vars)
        
            musst_slave = PcoTomoMusstAcquisitionSlave(self.card, store_list=self.storelist)
        
        
        return musst_master,musst_slave
        
    def prepare_calculation(self, musst_acq, musst_master, data_per_turn):
        
        calc_device = super().prepare_calculation(musst_acq)
        
        data_wturn = MusstWaitTurnCalc('calc_turn',f'{self.motor.name}_raw', data_per_turn, self.motor.steps_per_unit, 'waiting_turns')
        calc_wturns = CalcChannelAcquisitionSlave('calc_waiting_turns',(musst_acq,),data_wturn,data_wturn.acquisition_channels)

        data_time = MusstStartTimeCalc('pstn', 'timer_raw', musst_master, data_per_turn, self.card.get_timer_factor(), 'start_time')
        calc_stime = CalcChannelAcquisitionSlave('calc_start_time',(musst_acq,),data_time,data_time.acquisition_channels)
        
        data_wtime = MusstWaitTimeCalc('calc_time', 'timer_raw', data_per_turn, self.card.get_timer_factor(), 'waiting_time')
        calc_wtime = CalcChannelAcquisitionSlave('calc_waiting_time',(musst_acq,),data_wtime,data_wtime.acquisition_channels)

        calc_device.append(calc_wturns) 
        calc_device.append(calc_stime)
        calc_device.append(calc_wtime)
        
        return calc_device 
        
    def open_sync_shutter(self, sync_shut_time=None):
        if sync_shut_time is not None:
            self.sync_shut_time = sync_shut_time
        self.card.putget("BTRIG 1")
        time.sleep(self.sync_shut_time / 1000.0)
        
    def close_sync_shutter(self):
        self.card.putget("BTRIG 0")
