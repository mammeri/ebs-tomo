import numpy as np

from bliss import setup_globals, global_map
from bliss.common.logtools import log_info, log_debug
import tomo.pcotomo as pcotomo
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.acquisition.lima import (
    LimaAcquisitionMaster,
    RoiProfileAcquisitionSlave,
)
from bliss.scanning.scan import Scan


class PcoTomoScan:
    def __init__(self, tomo):
        """
        Parameters
        ----------
        name : str
            The Bliss object name
        tomo : PcoTomo object (ex: HrPcoTomo) 
            contains all info about pcotomo (hardware, parameters)
        """
        # init logging
        self.name = tomo.name + ".tomoscan"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")
        self.tomo = tomo
        self.in_pars = {}
        log_info(self, "__init__() leaving")

    def calculate_parameters(
        self, start_pos, end_pos, nb_points, exposure_time, latency_time=0, ntomo=1
    ):
        """
        Calculates scan parameters:
        - scan_step_size : angle in degrees between each image
        - scan_point_time : time in seconds between each image
         (exposure time + readout time + shutter time + latency time)
        - scan_speed : rotation speed in degrees per seconds
        - start_turn : number of turns needed by the rotation to reach the
         desired speed
        """

        log_info(self, "calculate_parameters() entering")

        self.in_pars["end_pos"] = end_pos * ntomo
        self.in_pars["trange"] = (end_pos - start_pos) * ntomo
        self.in_pars["tomo_n"] = nb_points * ntomo
        self.tomo.tomo_ccd.calculate_parameters(exposure_time)
        scan_point_time = exposure_time + self.tomo.tomo_ccd.parameters.readout_time + latency_time
        time = scan_point_time * nb_points 
        self.in_pars["time"] = time * ntomo
        self.in_pars["scan_step_size"] = (end_pos - start_pos) / nb_points
        self.in_pars["scan_speed"] = self.in_pars["scan_step_size"] / scan_point_time
        self.in_pars["scan_point_time"] = scan_point_time

        self.in_pars["sync_shut_time"] = self.tomo.shutter.sync_shutter_time / 1000.0
        self.in_pars["fast_shut_time"] = (
            self.tomo.shutter.fast_shutter_closing_time() / 1000.0
        )

        if self.in_pars["sync_shut_time"] > 0:
            self.in_pars["sync_shut_angle"] = (
                int(self.in_pars["sync_shut_time"] * self.in_pars["scan_speed"]) + 1
            )
        else:
            self.in_pars["sync_shut_angle"] = 0

        self.watchdog_trig_difference = 1000
        self.in_pars["start_turn"] = self.calculate_undershoot(
            self.tomo.rotation_axis, start_pos, time, self.tomo.pcotomo.parameters.start_turn
        )
        integ_ratio = self.tomo.parameters.exposure_time / scan_point_time
        self.in_pars['integration_ratio'] = integ_ratio

    def calculate_undershoot(self, motor, start_pos, time, start_turn):
        """
        Returns number of turns needed by rotation to reach scan speed
        """
        speed = motor.sign * (self.tomo.parameters.end_pos - start_pos) / time
        acctime = speed / motor.acceleration
        accdisp = speed * acctime / 2

        addturn = 0
        accpos = start_pos - accdisp
        if motor.position > accpos:
            if accpos < 0:
                accdisp -= start_pos
                addturn = int(accdisp / 360)
                if accdisp % 360 != 0:
                    addturn += 1
                undershoot = start_pos
            else:
                undershoot = accdisp
        else:
            undershoot = start_pos - motor.position

        if start_turn > addturn:
            addturn = start_turn

        undershoot_start_margin = addturn * 360

        return undershoot, undershoot_start_margin

    def continuous_scan(
        self,
        start_pos,
        end_pos,
        nb_trig,
        exposure_time,
        time,
        ntomo,
        nloop,
        nwait,
        noread,
        start_turn,
        shutter_angle,
        user_output,
        trigger_type,
        title,
        scan_info={},
        save=True,
        run=False,
    ):
        """
        PcoTomoMotorMaster moves rotation axis based on speed from start position at constant speed (related to scan point time) during the time necessary to acquire all scans

        MusstAcquisitionSlave runs 'pcotomo' program and receives encoder and timer data.
        Musst triggers are synchronized by rotation axis encoder.
        Triggers can be equally spaced in time or position.

        LimaAcquisitionMaster waits the external musst triggers and takes one image at each trigger.
        """
        chain = AcquisitionChain(parallel_prepare=True)

        def stop_motor(motor):
            return (
                motor_master.slaves[0].musst.STATE
                == motor_master.slaves[0].musst.RUN_STATE
            )

        undershoot, undershoot_start_margin = self.calculate_undershoot(
            self.tomo.rotation_axis, start_pos, time, start_turn
        )

        speed = (end_pos - start_pos) / time * self.tomo.rotation_axis.sign
        if noread:
            motor_master = pcotomo.PcoTomoMotorMaster(
                self.tomo.rotation_axis,
                start_pos,
                speed,
                end_func=stop_motor,
                undershoot=undershoot,
                start_margin=undershoot_start_margin,
            )
        else:
            motor_master = pcotomo.PcoTomoIterMotorMaster(
                self.tomo.rotation_axis,
                start_pos,
                speed,
                end_func=stop_motor,
                undershoot=undershoot,
                start_margin=undershoot_start_margin,
            )

        scan_time = time + ((end_pos - start_pos) % 360) / abs(speed) + nwait * time

        musst_master, musst_slave = self.tomo.pcotomo.tomo_musst.prepare(
            start_pos,
            end_pos - start_pos,
            nb_trig,
            time,
            ntomo,
            nloop,
            nwait,
            shutter_angle,
            user_output,
            noread,
            scan_time,
            trigger_type,
        )

        chain.add(musst_master, musst_slave)
        chain.add(motor_master, musst_master)

        # prepare Lima saving
        ctrl_params = {}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(nb_trig))

        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = nb_trig

        lima_master = LimaAcquisitionMaster

        if noread:
            acq_nb_frames = nb_trig * nloop
            prepare_once = True
            start_once = True
            wait_frame_id = [acq_nb_frames - 1]

        else:
            if nwait == -1:
                lima_master = pcotomo.PcoTomoLimaAcquisitionMaster
            acq_nb_frames = nb_trig
            prepare_once = False
            start_once = False
            wait_frame_id = [nb_trig - 1 for i in range(nloop)]

        acq_params = {
            "acq_nb_frames": acq_nb_frames,
            "acq_trigger_mode": "EXTERNAL_TRIGGER_MULTI",
            "acq_expo_time": exposure_time,
            "prepare_once": prepare_once,
            "start_once": start_once,
            "wait_frame_id": wait_frame_id,
        }

        lima_acq = lima_master(
            self.tomo.tomo_ccd.detector, ctrl_params=ctrl_params, **acq_params
        )

        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)

        if self.tomo.parameters.sinogram_active:

            for roi_profile in self.tomo.tomo_ccd.detector.roi_profiles.counters:
                # prepare roi spectrums acquisition
                spectrum_acq = RoiProfileAcquisitionSlave(
                    roi_profile, count_time=exposure_time, npoints=nb_trig
                )
                chain.add(lima_acq, spectrum_acq)

        chain.add(musst_master, lima_acq)

        # calculation devices are used to convert musst data (encoder + timer) in appropriate units (degree and seconds)
        calc_musst = self.tomo.pcotomo.tomo_musst.prepare_calculation(
            musst_slave, musst_master, nb_trig
        )
        calc_musst_timer = calc_musst[0]
        calc_musst_pos = calc_musst[1]
        for calc in calc_musst:
            chain.add(musst_master, calc)

        # Add data channel to motor master
        motor_master.add_external_channel(
            calc_musst_pos,
            calc_musst_pos.channels[0].short_name,
            rename=self.tomo.rotation_axis.name,
            dtype=np.float,
        )

        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(
                musst_master,
                chain,
                self.tomo.tomo_ccd.detector.name,
                exposure_time,
                nb_trig,
            )

        scan = Scan(
            chain,
            scan_info=scan_info,
            name=title,
            save=save,
            data_watch_callback=pcotomo.PcoTomoScanDisplay(
                trigger_name=calc_musst_timer.channels[0].name,
                motors=self.tomo.scan_motors,
                limas=(self.tomo.tomo_ccd.detector,),
            ),
            # watchdog_callback=pcotomo.PcoTomoScanWatchdog(trigger_name=musst_slave.channels[0].name,
            # limas= (self.tomo.tomo_ccd.detector,),
            # trig_difference= self.watchdog_trig_difference),
        )

        print(scan.acq_chain._tree)
        if run == True:
            scan.run()

        return scan
