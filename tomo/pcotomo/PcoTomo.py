import datetime
import time
import os
import glob
from bliss.scanning.toolbox import ChainBuilder
from bliss.controllers.lima.lima_base import Lima
from bliss import setup_globals, global_map, current_session
from bliss.setup_globals import *
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.common.logtools import log_info, log_debug, log_error
from bliss.common.session import get_current_session
from bliss.common.cleanup import cleanup, capture_exceptions, axis as cleanup_axis
from bliss.scanning.group import Sequence
from bliss.shell.standard import *
from bliss.config.static import get_config, ConfigList

import tomo
from tomo.Tomo import Tomo
from tomo.TomoParameters import TomoParameters
from tomo.TomoMusst import TriggerType
import tomo.pcotomo as pcotomo
from tomo.pcotomo.Presets import FastShutterPreset
from tomo.pcotomo.PcoTomoScanDisplay import PcoTomoScanDisplay


class PcoTomo(Tomo):
    def __init__(self, tomo_name, config):
        # init logging
        self.name = tomo_name
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")

        self.config = config
        self.tomo_name = tomo_name

        # print(config)

        # Initialise the hardware necessary for the tomo scans and access
        # the hardware onece to be sure that the initialization is done
        # and the equipment connected.

        # Init and test the rotation axis
        self.rotation_axis = config["rotation"]["axis"]

        print("Rotation axis used: %s" % self.rotation_axis.name)
        print("Rotation axis position: %f" % self.rotation_axis.position)

        # Init and test detector axis
        self.detector_axis = config["detector_x_axis"]
        print("Detector axis used: %s" % self.detector_axis.name)
        print("Detector axis position: %f" % self.detector_axis.position)

        # Init and test the detector
        self.tomo_ccd = tomo.TomoCcd(self.tomo_name, config)
        print("Detector used: %s" % self.tomo_ccd.get_detector_name())

        # Init and test the shutter
        config_shutter = config["shutter"]
        self.shutter = tomo.TomoShutter(self.tomo_name, config, self.tomo_ccd)

        print("Shutter used: %s" % self.shutter.shutter_used)
        self.shutter.fast_shutter_active = False
        if config_shutter["fast_shut_time"] > 0:
            self.shutter.fast_shutter_active = True
            if (
                self.shutter.fast_shutter.name == "exp_shutter"
                and self.shutter.fast_shutter.shutter == "SHTHIRD"
            ):
                self.shutter.fast_shutter.closing_times[
                    self.shutter.fast_shutter.shutter
                ] = config_shutter["fast_shut_time"]
            elif self.shutter.fast_shutter.name == "isg_shutter":
                self.shutter.fast_shutter.settings["opening_time"] = config_shutter[
                    "fast_shut_time"
                ]
            else:
                raise RuntimeError(
                    "Invalid fast shutter type! Should be beam shutter [bsh] or fast shutter [third]"
                )

        # Init and test the optic
        self.list_opt = config["optic"]

        param_name = tomo_name + f":optic_parameters"
        optic_defaults = {}
        optic_defaults["active_optic"] = None

        self._tomo_optic = tomo.TomoOptic(
            self.tomo_name, config, param_name, optic_defaults
        )

        active_optic = None
        if self._tomo_optic.parameters.active_optic is not None:
            active_optic = get_config().get(self._tomo_optic.parameters.active_optic)

        if active_optic is not None and active_optic in self.list_opt:
            self.optic = active_optic
        else:
            self.optic = self.list_opt[0]

        self._tomo_optic.parameters.active_optic = self.optic.name
        # apply image flipping specified for the optic used
        if self.tomo_ccd.detector is not None:
            self.tomo_ccd.set_image_flipping(self.optic.image_flipping)

        print("Optic used: %s" % self.optic.description)

        if self.tomo_ccd.detector is not None:
            self.optic.calculate_image_pixel_size(self.tomo_ccd.detector)

        # Init and test the musst
        self.musst_card = config["musst"]
        motor = self.rotation_axis
        self.tomo_musst = pcotomo.PcoTomoMusst(
            self.tomo_name, self.musst_card, motor, config_shutter["sync_shut_time"]
        )
        print("Musst channel used: %s" % self.tomo_musst.mot_chan)

        # Init the data saving
        self.saving = tomo.TomoSaving(self.tomo_name, config, self.tomo_ccd)
        self.saving.show_config()

        # Init the meta data object
        self.meta_data = pcotomo.PcoTomoMetaData(self.tomo_name, self)

        ## Init and test the additional counters
        try:
            self.counters = config["additional_counters"]
            # check the object type
            if not isinstance(self.counters, TomoCounters):
                self.counters = None
        except:
            self.counters = None

        if self.counters == None:
            print(f"No additional counters specified for {self.name}!")
        else:
            print(f"Additional counters from class {self.counters.__class__.__name__}")

        #
        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values

        param_name = self.tomo_name + ":scan_parameters"
        scan_defaults = {}
        scan_defaults["tomo_n"] = 1500
        scan_defaults["latency_time"] = 0.0002
        scan_defaults["exposure_time"] = 0.04
        scan_defaults["start_pos"] = 180
        scan_defaults["trange"] = 180
        scan_defaults["time"] = 750
        scan_defaults["ntomo"] = 1
        scan_defaults["nloop"] = 1
        scan_defaults["nwait"] = 0
        scan_defaults["noread"] = 1
        scan_defaults["user_output"] = 0
        scan_defaults["start_turn"] = 0
        scan_defaults["diff_check"] = 100
        scan_defaults["ref_n"] = 21
        scan_defaults["dark_n"] = 20
        scan_defaults["ref_images_at_start"] = False
        scan_defaults["ref_images_at_end"] = True
        scan_defaults["dark_images_at_start"] = True
        scan_defaults["dark_images_at_end"] = False
        scan_defaults["return_to_start_pos"] = True
        scan_defaults["energy"] = 0.0
        scan_defaults["source_sample_distance"] = 145000.0
        scan_defaults["sample_detector_distance"] = 0.0
        scan_defaults["trigger_type"] = TriggerType.TIME
        scan_defaults["sinogram_active"] = True

        # Initialise the PcoTomoParameters class
        self.parameters = TomoParameters(param_name, scan_defaults).parameters
        self.param_name = param_name

        # Init and test preset
        beamline = config["beamline"]
        sequence_preset = config["sequence_preset"]
        if type(sequence_preset) == ConfigList:
            self.sequence_preset = list()
            for preset in sequence_preset:
                self.sequence_preset.append(preset)
        else:
            self.sequence_preset = [sequence_preset]

        self.sequence_prepare = False
        self.reference = config["fasttomo"].reference
        self.pcotomo = pcotomo.PcoTomoSequence.PcoTomo(self)
        self.sequence = "pcotomo:basic"
        self.mode = ""
        self.scan_motors = [self.rotation_axis]
        self.in_pars = {}

        self.tomo_tools = pcotomo.PcoTomoTools(self)
        self.tomo_sino = tomo.TomoSinogram(self.tomo_name)

        log_info(self, "__init__() leaving")

    def __info__(self):
        info_str = f"{self.name} info:\n"
        info_str += f"Configuration\n"
        info_str += f"  rotation axis = {self.rotation_axis.name}\n"
        info_str += f"  detector axis = {self.detector_axis.name}\n"
        info_str += f"  detector      = {self.tomo_ccd.get_detector_name()}\n"
        info_str += f"  optic         = {self.optic.description}\n"
        info_str += f"  shutter       = {self.shutter.shutter_used}\n"

        info_str += f"Scan\n"
        info_str += f"  tomo_n        = {self.parameters.tomo_n}\n"
        info_str += f"  ntomo        = {self.parameters.ntomo}\n"
        info_str += f"  nloop        = {self.parameters.nloop}\n"
        info_str += f"  nwait        = {self.parameters.nwait}\n"
        info_str += f"  noread        = {self.parameters.noread}\n"
        info_str += f"  user_output        = {self.parameters.user_output}\n"
        info_str += f"  start_turn        = {self.parameters.start_turn}\n"
        info_str += f"  diff_check        = {self.parameters.diff_check}\n"
        info_str += f"  ref_n         = {self.parameters.ref_n}\n"
        info_str += f"  dark_n        = {self.parameters.dark_n}\n"
        info_str += f"  time = {self.parameters.time}\n"
        info_str += f"  latency_time  = {self.parameters.latency_time}\n"
        info_str += f"  start_pos     = {self.parameters.start_pos}\n"
        info_str += f"  trange       = {self.parameters.trange}\n"
        info_str += f"  trigger_type     = {self.parameters.trigger_type}\n"
        info_str += (
            f"  source_sample_distance   = {self.parameters.source_sample_distance}\n"
        )
        info_str += (
            f"  sample_detector_distance = {self.parameters.sample_detector_distance}\n"
        )
        info_str += f"  energy = {self.parameters.energy}\n"
        return info_str

    def simple_setup(self):

        # dlg_roix = UserIntInput(label="Image Roi Width?", defval=self.tomo_ccd.detector.image.width)
        # dlg_roiy = UserIntInput(label="Image Roi Height?", defval=self.tomo_ccd.detector.image.height)
        dlg_tomo_n = UserIntInput(
            label="Number of Projections?", defval=self.parameters.tomo_n
        )
        dlg_time = UserFloatInput(
            label="Acquisition Time of One Scan [s]?", defval=self.parameters.time
        )
        dlg_nwait = UserIntInput(
            label="Number of Waiting Turns between Tomo?", defval=self.parameters.nwait
        )
        dlg_startpos = UserIntInput(
            label="Tomo Start Position?", defval=self.parameters.start_pos
        )

        # ret = BlissDialog([[dlg_roix], [dlg_roiy], [dlg_tomo_n], [dlg_time], [dlg_nwait], [dlg_startpos]], title='Scan Setup').show()
        ret = BlissDialog(
            [[dlg_tomo_n], [dlg_time], [dlg_nwait], [dlg_startpos]], title="Scan Setup"
        ).show()

        # returns False on cancel
        if ret != False:

            self.parameters.tomo_n = ret[dlg_tomo_n]
            # self.tomo_ccd.detector.proxy.image_roi = [0,0,0,0]
            # roix = int((self.tomo_ccd.detector.proxy.image_roi[2] - ret[dlg_roix])/2)
            # roiy = int((self.tomo_ccd.detector.proxy.image_roi[3] - ret[dlg_roiy])/2)
            # self.tomo_ccd.detector.image.roi = [roix,roiy,ret[dlg_roix],ret[dlg_roiy]]
            self.parameters.time = ret[dlg_time]
            self.parameters.nwait = ret[dlg_nwait]
            self.parameters.start_pos = ret[dlg_startpos]

            self.setup_info()

    def setup_info(self, mode=None):

        depth = self.tomo_ccd.detector.image.depth
        width = self.tomo_ccd.detector.image.width
        height = self.tomo_ccd.detector.image.height
        sizeofoneimage = depth * width * height / 1024 / 1024
        sizeofonescan = sizeofoneimage * self.parameters.tomo_n / 1024
        self.tomo_ccd.estimate_transfer_rate(self.parameters.exposure_time)
        numberofscans = int(
            self.tomo_ccd.detector.camera.max_nb_images / self.parameters.tomo_n
        )
        totalcontinuoustime = numberofscans * self.parameters.time
        waitingtime = (
            (360 / self.parameters.trange) * self.parameters.nwait
            + int(180 / self.parameters.trange)
        ) * self.parameters.time
        frequency = waitingtime + self.parameters.time
        totaltime = frequency * numberofscans
        transferrate = round(
            self.tomo_ccd.detector.proxy.saving_statistics[-1] / 1024 / 1024, 2
        )
        unlimitwaitingturns = (
            int(
                ((sizeofonescan / (transferrate / 1024)) / self.parameters.time)
                / (360 / self.parameters.trange)
            )
            + 2
        )
        unlimitwaitingtime = (
            (360 / self.parameters.trange) * unlimitwaitingturns
            + int(180 / self.parameters.trange)
        ) * self.parameters.time
        unlimitfrequency = unlimitwaitingtime + self.parameters.time

        if mode is None:
            dlg_trans_rate = UserMsg(label=f"Transfer Rate: {transferrate} MB/s")
            dlg_roi = UserMsg(label=f"ROI: {width} x {height}\n")

            dlg_continuous_mode = UserCheckBox(
                label="Consecutive Scans Without Waiting Turns"
            )
            dlg_max_nb_scans = UserMsg(
                label=f"You can collect a maximum of {numberofscans} scans in the memory"
            )
            dlg_one_scan_size = UserMsg(
                label=f"One scan is {round(sizeofonescan*1024*1024*1024,2)} B or {round(sizeofonescan,2)} GB"
            )
            dlg_acq_time = UserMsg(
                label=f"It means a film of {totalcontinuoustime} s ({round(totalcontinuoustime/60,2)} min)\n"
            )
            dlg_saving_time = UserMsg(
                label=f"Time to download film: {round((sizeofonescan*numberofscans)/(transferrate/1024),2)} s ({round((sizeofonescan*numberofscans)/(transferrate/1024)/60,2)} min)\n"
            )

            ct1 = Container(
                [
                    dlg_continuous_mode,
                    dlg_max_nb_scans,
                    dlg_one_scan_size,
                    dlg_acq_time,
                    dlg_saving_time,
                ],
                title="Continuous Mode",
            )

            dlg_multiple_mode = UserCheckBox(label="Waiting Turns Between Each Scan")
            dlg_nb_scans = UserMsg(
                label=f"You will collect a total of {numberofscans} scans\n"
            )
            dlg_waiting_turns = UserMsg(
                label=f"You will wait {self.parameters.nwait} turns between each scan"
            )
            dlg_acq_time = UserMsg(
                label=f"One scan of {self.parameters.time} s every {frequency} s during {totaltime} s ({round(totaltime/60,2)} min) \n"
            )
            dlg_saving_time = UserMsg(
                label=f"Time to download film: {round((sizeofonescan*numberofscans)/(transferrate/1024),2)} s ({round((sizeofonescan*numberofscans)/(transferrate/1024)/60,2)} min)\n"
            )

            ct2 = Container(
                [
                    dlg_multiple_mode,
                    dlg_nb_scans,
                    dlg_waiting_turns,
                    dlg_acq_time,
                    dlg_saving_time,
                ],
                title="Multiple Mode",
            )

            dlg_endless_mode = UserCheckBox(label="Downloading Between Each Scan")
            dlg_waiting_turns = UserMsg(
                label=f"You will wait around {unlimitwaitingturns} turns between each scan, during this time, scan is downloaded"
            )
            dlg_acq_time = UserMsg(
                label=f"One scan of {self.parameters.time} s every {unlimitfrequency} s FOREVER \n"
            )
            dlg_saving_time = UserMsg(
                label=f"Time to download one scan: {round(sizeofonescan/(transferrate/1024),2)} s ({round(sizeofonescan/(transferrate/1024)/60,2)} min) \n"
            )
            dlg_nb_scans = UserIntInput(label="Number of scans?", defval=numberofscans)

            ct3 = Container(
                [
                    dlg_endless_mode,
                    dlg_waiting_turns,
                    dlg_acq_time,
                    dlg_saving_time,
                    dlg_nb_scans,
                ],
                title="Endless Mode",
            )

            ret = BlissDialog(
                [[dlg_trans_rate, dlg_roi], [ct1], [ct2], [ct3]], title="Mode Setup"
            ).show()

            # returns False on cancel
            if ret != False:

                if ret[dlg_continuous_mode]:
                    self.mode = "consecutive"
                    self.parameters.nloop = 1
                    self.parameters.ntomo = numberofscans
                    self.parameters.nwait = 0
                    self.parameters.noread = 1
                elif ret[dlg_multiple_mode]:
                    self.mode = "multiple"
                    self.parameters.ntomo = 1
                    self.parameters.nloop = numberofscans
                    self.parameters.noread = 1
                elif ret[dlg_endless_mode]:
                    self.mode = "endless"
                    self.parameters.ntomo = 1
                    self.parameters.noread = 0
                    self.parameters.nloop = ret[dlg_nb_scans]
                    self.parameters.nwait = -1
        else:
            self.mode = mode
            if mode == "consecutive":
                print("\n\nConsecutive Scans Without Waiting Turns")
                print(
                    f"You can collect a maximum of {numberofscans} scans in the memory"
                )
                print(
                    f"One scan is {round(sizeofonescan*1024*1024*1024,2)} B or {round(sizeofonescan,2)} GB"
                )
                print(
                    f"It means a film of {totalcontinuoustime} s ({round(totalcontinuoustime/60,2)} min)\n"
                )
                print(
                    f"Time to download film: {round((sizeofonescan*numberofscans)/(transferrate/1024),2)} s ({round((sizeofonescan*numberofscans)/(transferrate/1024)/60,2)} min)\n"
                )
            elif mode == "multiple":
                print("\n\nWaiting Turns Between Each Scan")
                print(f"You will collect a total of {numberofscans} scans\n")
                print(f"You will wait {self.parameters.nwait} turns between each scan")
                print(
                    f"One scan of {self.parameters.time} s every {frequency} s during {totaltime} s ({round(totaltime/60,2)} min) \n"
                )
                print(
                    f"Time to download film: {round((sizeofonescan*numberofscans)/(transferrate/1024),2)} s ({round((sizeofonescan*numberofscans)/(transferrate/1024)/60,2)} min)\n"
                )
            elif mode == "endless":
                print(
                    f"\n\nYou will wait {unlimitwaitingturns} turns between each scan, during this time, scan is downloaded"
                )
                print(
                    f"One scan of {self.parameters.time} s every {unlimitfrequency} s FOREVER \n"
                )
                print(
                    f"Time to download one scan: {round(sizeofonescan/(transferrate/1024),2)} s ({round(sizeofonescan/(transferrate/1024)/60,2)} min) \n"
                )
                self.parameters.nwait = unlimitwaitingturns

    def setup(self):
        """
        Set-up the tomo scan and all its sub objects like detector, 
        optic, shutter ,saving, etc.
        """
        value_list = [
            ("scan", "Scan_Parameter Setup"),
            ("ccd", "Detector Setup"),
            ("optic", "Optic Setup"),
            ("reference", "Reference Setup"),
            ("saving", "Saving Setup"),
            ("flags", "Scan Option Setup"),
            ("exit", "Exit"),
        ]

        ret = True
        choice = "scan"
        default = 0

        while ret != False and choice != "exit":
            dlg1 = UserChoice(values=value_list, defval=default)

            ret = BlissDialog([[dlg1]], title="Tomo Setup").show()

            # returns False on cancel
            if ret != False:
                choice = ret[dlg1]

                if choice == "scan":
                    self.scan_setup()
                if choice == "flags":
                    self.flag_setup()
                if choice == "ccd":
                    self.tomo_ccd.setup()
                if choice == "optic":
                    cancel = self.select_optic()
                    if not cancel:
                        self.optic.setup(self.tomo_ccd)
                if choice == "reference":
                    self.reference.setup()
                if choice == "saving":
                    self.saving.setup()

                for i in range(0, len(value_list)):
                    if choice == value_list[i][0]:
                        default = i
                        break

    def scan_setup(self):
        """
        Set-up the main tomo scan parameters
        """

        dlg_tomo_n = UserIntInput(
            label="Number of Projections?", defval=self.parameters.tomo_n
        )
        dlg_latency = UserFloatInput(
            label="Latency Time [s]?", defval=self.parameters.latency_time
        )
        dlg_time = UserFloatInput(
            label="Acquisition Time of a Single Tomo [s]?", defval=self.parameters.time
        )
        values = [(0, "Position"), (1, "Time")]
        dlg_trigger_type = UserChoice(
            label="Musst Triggered In?",
            values=values,
            defval=self.parameters.trigger_type.value,
        )

        dlg_ntomo = UserIntInput(
            label="Number of Consecutive Tomo?", defval=self.parameters.ntomo
        )
        dlg_nloop = UserIntInput(
            label="Number of Tomo Loops?", defval=self.parameters.nloop
        )
        dlg_nwait = UserIntInput(
            label="Number of Waiting Turns between Tomo?", defval=self.parameters.nwait
        )
        dlg_readout = UserCheckBox(
            label="Image Transfer between Tomo", defval=self.parameters.noread != 1
        )
        dlg_iowait = UserCheckBox(
            label="Use External IO (Opiom) for Waiting Turns",
            defval=self.parameters.nwait == -1,
        )
        dlg_user_output = UserIntInput(
            label="User Output after Tomo Number?", defval=self.parameters.user_output
        )
        dlg_check_diff = UserIntInput(
            label="Maximum Frame/Trigger difference allowed?",
            defval=self.parameters.diff_check,
        )
        dlg_start_turn = UserIntInput(
            label="Number of turns before starting?", defval=self.parameters.start_turn
        )

        dlg_dark_n = UserIntInput(
            label="Number of Dark Images?", defval=self.parameters.dark_n
        )
        dlg_ref_n = UserIntInput(
            label="Number of Reference Images?", defval=self.parameters.ref_n
        )

        dlg_sdd = UserFloatInput(
            label="Sample to Detector Distance [mm]?",
            defval=self.parameters.sample_detector_distance,
        )
        dlg_ssd = UserFloatInput(
            label="Source to Sample Distance [mm]?",
            defval=self.parameters.source_sample_distance,
        )

        ct1 = Container(
            [dlg_tomo_n, dlg_latency, dlg_time, dlg_trigger_type], title="Single Tomo"
        )
        ct2 = Container(
            [
                dlg_ntomo,
                dlg_nloop,
                dlg_nwait,
                dlg_readout,
                dlg_iowait,
                dlg_user_output,
                dlg_check_diff,
                dlg_start_turn,
            ],
            title="Options",
        )
        ct3 = Container([dlg_dark_n, dlg_ref_n], title="Dark and Reference Images")
        ct4 = Container([dlg_sdd, dlg_ssd], title="Distances")

        ret = BlissDialog([[ct1], [ct2], [ct3], [ct4]], title="Tomo Scan Setup").show()

        # returns False on cancel
        if ret != False:
            self.parameters.tomo_n = int(ret[dlg_tomo_n])
            self.parameters.latency_time = float(ret[dlg_latency])
            self.parameters.time = float(ret[dlg_time])
            self.parameters.trigger_type = TriggerType(ret[dlg_trigger_type])
            self.parameters.ntomo = int(ret[dlg_ntomo])
            self.parameters.nloop = int(ret[dlg_nloop])
            self.parameters.nwait = int(ret[dlg_nwait])
            if ret[dlg_readout] == True:
                self.parameters.noread = 0
            else:
                self.parameters.noread = 1
                if self.parameters.nwait < 0:
                    self.parameters.nwait = 0
            if ret[dlg_iowait] == True:
                self.parameters.nwait = -1
            self.parameters.user_output = int(ret[dlg_user_output])
            self.parameters.diff_check = ret[dlg_check_diff]
            self.parameters.start_turn = ret[dlg_start_turn]
            self.parameters.dark_n = int(ret[dlg_dark_n])
            self.parameters.ref_n = int(ret[dlg_ref_n])
            self.parameters.sample_detector_distance = float(ret[dlg_sdd])
            self.parameters.source_sample_distance = float(ret[dlg_ssd])

    def flag_setup(self):
        """
        Set-up the tomo scan options for dark images, reference images, return images and others. 
        All options can be True or False
        """
        dlg1 = UserCheckBox(
            label="Dark Images at Start", defval=self.parameters.dark_images_at_start
        )
        dlg2 = UserCheckBox(
            label="Dark Images at End", defval=self.parameters.dark_images_at_end
        )
        dlg3 = UserCheckBox(
            label="Dark Images with Safety Shutter",
            defval=self.shutter.parameters.dark_images_with_beam_shutter,
        )

        dlg4 = UserCheckBox(
            label="Reference Images at Start",
            defval=self.parameters.ref_images_at_start,
        )
        dlg5 = UserCheckBox(
            label="Reference Images at End", defval=self.parameters.ref_images_at_end
        )

        dlg6 = UserCheckBox(
            label="Return to Start Position", defval=self.parameters.return_to_start_pos
        )

        ct1 = Container([dlg1, dlg2, dlg3], title="Dark Images")
        ct2 = Container([dlg4, dlg5], title="Reference Images")
        ct3 = Container([dlg6], title="Others")

        ret = BlissDialog([[ct1], [ct2], [ct3]], title="Tomo Option Setup").show()

        # returns False on cancel
        if ret != False:
            self.parameters.dark_images_at_start = ret[dlg1]
            self.parameters.dark_images_at_end = ret[dlg2]
            self.shutter.parameters.dark_images_with_beam_shutter = ret[dlg3]
            self.parameters.ref_images_at_start = ret[dlg4]
            self.parameters.ref_images_at_end = ret[dlg5]
            self.parameters.return_to_start_pos = ret[dlg6]

    def calibrate_detector(self):

        frate = self.parameters.tomo_n / self.parameters.time

        self.tomo_ccd.calibrate_ccd_readout_time(0.000001)

        if frate > self.tomo_ccd.detector.camera.frame_rate:
            raise Exception(f"Error, frame rate too high {frate}")

        fexpo = 1 / self.tomo_ccd.detector.camera.frame_rate
        self.tomo_ccd.calibrate_ccd_readout_time(fexpo)
        deadtime = self.tomo_ccd.detector.camera.coc_run_time - fexpo

        scan_point_time = self.parameters.time / self.parameters.tomo_n
        expo_time = scan_point_time - deadtime - self.parameters.latency_time

        return expo_time

    def ref_scan(
        self,
        ref_n=None,
        expo_time=None,
        projection=1,
        turn=0,
        scan_sequence=None,
        header={},
        save=True,
        run=True,
    ):
        shutter_preset = FastShutterPreset(self.shutter, self.tomo_musst)
        return super().ref_scan(
            ref_n=ref_n,
            expo_time=expo_time,
            projection=projection,
            turn=turn,
            shutter_preset=shutter_preset,
            scan_sequence=scan_sequence,
            header=header,
            save=save,
            run=run,
        )

    def def_ref_disp(self):
        """
        Calculates from the camera field-of-view, the default lateral motor displacement needed
        to move sample out of the beam.
        For half acquisition, it determines the lateral motor displacement needed 
        to see half of the sample in the camera field-of-view.
        """
        ref_disp = self.tomo_ccd.field_of_view(self.optic.magnification)
        # add 10% and round to nearest 0.1 mm
        return int(11 * ref_disp + 0.5) / 10

    def prepare_camera(self):

        if not os.path.exists(current_session.scan_saving.get_path()):
            os.makedirs(current_session.scan_saving.get_path())
        self.tomo_ccd.detector.proxy.saving_mode = "AUTO_FRAME"
        self.tomo_ccd.detector.proxy.saving_directory = (
            current_session.scan_saving.get_path()
        )
        self.tomo_ccd.detector.proxy.saving_frame_per_file = (
            self.parameters.tomo_n * self.parameters.ntomo
        )
        self.tomo_ccd.detector.proxy.saving_format = (
            self.tomo_ccd.detector.saving.file_format
        )
        self.tomo_ccd.detector.proxy.acq_mode = "SINGLE"
        self.tomo_ccd.detector.proxy.acq_trigger_mode = "EXTERNAL_TRIGGER_MULTI"
        self.tomo_ccd.detector.proxy.acq_expo_time = self.calibrate_detector()
        if self.parameters.noread:
            self.tomo_ccd.detector.proxy.acq_nb_frames = (
                self.parameters.tomo_n * self.parameters.ntomo * self.parameters.nloop
            )
        else:
            self.tomo_ccd.detector.proxy.acq_nb_frames = (
                self.parameters.tomo_n * self.parameters.ntomo
            )
        self.tomo_ccd.detector.proxy.video_source = "LAST_IMAGE"
        self.tomo_ccd.detector.proxy.video_active = True
        self.tomo_ccd.detector.proxy.saving_prefix = self.tomo_ccd.detector.name + "_"
        current_saving_path = current_session.scan_saving.get_path()
        data_saving_path = current_saving_path + "/camera_prepare_time.txt"
        data_file = open(data_saving_path, "a")
        start = time.time()
        ### fix for buffer memory increasing
        self.tomo_ccd.detector.proxy.set_timeout_millis(30000)
        self.tomo_ccd.set_acq_mode("ACCUMULATION")
        self.tomo_ccd.detector.proxy.prepareAcq()
        self.tomo_ccd.set_acq_mode("SINGLE")
        self.tomo_ccd.detector.proxy.prepareAcq()
        ###
        self.tomo_ccd.detector.proxy.set_timeout_millis(3000)
        prepare_time = time.time() - start
        self.tomo_ccd.detector.stopAcq()
        print(f"prepare camera took: {prepare_time}")
        unit = "seconds"
        if prepare_time < 1:
            prepare_time *= 1000
            unit = "milliseconds"
        data_file.write(
            f"\nFor roi {self.tomo_ccd.detector.proxy.image_roi[2]}x{self.tomo_ccd.detector.proxy.image_roi[3]}, prepare camera took {prepare_time:.2f} {unit}"
        )

        data_file.close()

    def prepare(self):

        print("Prepare tomo sequence")
        self.sequence_prepare = False

        # Verify the active detecor is still available
        self.tomo_ccd.verify_active_detector()

        # set pixel rate to maximum
        if self.tomo_ccd.detector.camera_type.lower() == "pco":
            self.tomo_ccd.set_max_pixel_rate()

        self.saving.configure(setup_globals.SCAN_SAVING.proposal_type)

        self.pcotomo.prepare()

        if self.parameters.ref_images_at_start or self.parameters.ref_images_at_end:
            self.reference.update_in_beam_position()

        if self.rotation_axis.position != self.parameters.start_pos:
            print(f"{self.rotation_axis.name} moving to start position")
            umv(self.rotation_axis, self.parameters.start_pos)

        # sequence preset prepare, needs to execute after the tomo prepare!
        for preset in self.sequence_preset:
            preset.prepare(self)

        self.tomo_musst.card.ABORT
        self.prepare_camera()
        if self.parameters.noread:
            self.tomo_ccd.detector.saving.mode = (
                self.tomo_ccd.detector.saving.SavingMode.ONE_FILE_PER_SCAN
            )
        else:
            self.tomo_ccd.detector.saving.mode = (
                self.tomo_ccd.detector.saving.SavingMode.ONE_FILE_PER_N_FRAMES
            )
        self.tomo_ccd.detector.saving.frames_per_file = (
            self.parameters.ntomo * self.parameters.tomo_n
        )

        self.sequence_prepare = True
        print("Sequence preparation ended well!\n")

    def run(self):
        PcoTomoScanDisplay.START_TIME = time.time()
        self.prepare()
        self.show_scan_info()
        if self.sequence_prepare is False:
            return
        self.pcotomo.run()

    def run_sequence(self, scan_sequence):

        print("Run pcotomo sequence")
        scan_seq = scan_sequence

        scan_display = setup_globals.SCAN_DISPLAY.auto
        setup_globals.SCAN_DISPLAY.auto = True

        self.list_dark_scans = list()
        self.list_ref_scans = list()
        self.list_proj_scans = list()

        scan_t0 = datetime.datetime.fromtimestamp(time.time())

        with capture_exceptions(raise_index=0) as capture:
            with capture():

                log_info(self, "start scan sequence")

                # sequence preset start
                for preset in self.sequence_preset:
                    preset.start()

                # take dark images
                if self.parameters.dark_images_at_start:
                    self.dark_scan(
                        expo_time=self.tomo_scan.in_pars["exposure_time"],
                        scan_sequence=scan_seq,
                    )

                if self.parameters.ref_images_at_start:
                    self.ref_scan(
                        expo_time=self.tomo_scan.in_pars["exposure_time"],
                        projection=1,
                        scan_sequence=scan_seq,
                    )

                # projection scan title
                title = (
                    "projections "
                    + str(1)
                    + " - "
                    + str(
                        self.parameters.tomo_n
                        * self.parameters.ntomo
                        * self.parameters.nloop
                    )
                )
                self.pcotomo.projection_scan(
                    self.parameters.start_pos,
                    self.parameters.start_pos + self.tomo_scan.in_pars["trange"],
                    self.tomo_scan.in_pars["tomo_n"],
                    title,
                    scan_sequence=scan_seq,
                )

                if self.parameters.dark_images_at_end:
                    self.dark_scan(
                        expo_time=self.tomo_scan.in_pars["exposure_time"],
                        scan_sequence=scan_seq,
                    )

                if self.parameters.ref_images_at_end:
                    self.ref_scan(
                        expo_time=self.tomo_scan.in_pars["exposure_time"],
                        projection=self.parameters.tomo_n
                        * self.parameters.ntomo
                        * self.parameters.nloop,
                        scan_sequence=scan_seq,
                    )

                setup_globals.SCAN_DISPLAY.auto = scan_display

            if len(capture.failed) > 0:
                print("\n")
                log_error(
                    self, "A problem occured during pcotomo sequence, sequence aborted"
                )
                log_error(self, capture.exception_infos)
                log_error(self, "\n")

                # sequence preset stop
                for preset in self.sequence_preset:
                    preset.stop()

                setup_globals.SCAN_DISPLAY.auto = scan_display
            else:
                print("\nPcoTomo sequence ended well!\n")

        scan_tend = datetime.datetime.fromtimestamp(time.time())
        print("Total scan sequence took {0}".format(scan_tend - scan_t0))

        log_info(self, "sequence ended")

    def single_tomo(self, start, trange, nimg, time):

        self.parameters.start_pos = start
        self.parameters.trange = trange
        self.parameters.tomo_n = nimg
        self.parameters.time = time
        self.parameters.ntomo = 1
        self.parameters.nloop = 1

        self.run()

    def consecutive_tomo(self, start, nimg, time, ntomo, trange=180):

        self.mode = "consecutive"
        self.parameters.start_pos = start
        self.parameters.trange = trange
        self.parameters.tomo_n = nimg
        self.parameters.time = time
        self.parameters.ntomo = ntomo
        self.parameters.nloop = 1
        self.parameters.noread = 1
        self.parameters.nwait = 0

        self.run()

    def multiple_tomo(self, start, nimg, time, nloop, nwait, trange=180):

        self.mode = "multiple"
        self.parameters.start_pos = start
        self.parameters.trange = trange
        self.parameters.tomo_n = nimg
        self.parameters.time = time
        self.parameters.ntomo = 1
        self.parameters.nloop = nloop
        self.parameters.nwait = nwait
        self.parameters.noread = 1

        self.run()

    def endless_tomo(self, start, nimg, time, ntomo, nloop, nwait, trange=180):

        self.mode = "endless"
        self.parameters.start_pos = start
        self.parameters.trange = trange
        self.parameters.tomo_n = nimg
        self.parameters.time = time
        self.parameters.ntomo = ntomo
        self.parameters.nloop = nloop
        self.parameters.nwait = nwait
        self.parameters.noread = 0

        self.run()

    def show_scan_info(self):
        print("\n=================================================================")
        print(f"Acquisition mode: {self.mode}")
        print(f"Total number of scans: {self.parameters.ntomo*self.parameters.nloop}")
        print(
            f"Number of images per scan: {self.parameters.tomo_n*self.parameters.ntomo}"
        )
        print(f"Scan start position: {self.parameters.start_pos}")
        print(
            f"Scan end position: {self.parameters.start_pos + self.tomo_scan.in_pars['trange']}"
        )
        print(f"Start turn: {abs(self.tomo_scan.in_pars['start_turn'][1]/360)}")
        print(f"Scan step size: {self.tomo_scan.in_pars['scan_step_size']:.3f} degrees")
        print(
            f"Scan point time: {self.tomo_scan.in_pars['scan_point_time']} sec (Exposure time: {round(self.tomo_scan.in_pars['exposure_time'],5)} sec)"
        )
        print(f"Scan speed: {self.tomo_scan.in_pars['speed']:.2f} degrees / sec")
        print(f"Saving path: {current_session.scan_saving.get_path()}")
        print(f"Size of one image = {self.tomo_ccd.in_pars['image_mbsize']:.2f} MB")
        print(f"Image file format: {self.tomo_ccd.detector.saving.file_format}")
        print(
            f"Size of one file: {self.tomo_ccd.in_pars['image_mbsize']*self.parameters.tomo_n*self.parameters.ntomo/1024:.2f} GB"
        )
        print(
            f"Number of images in one file: {self.tomo_ccd.detector.saving.frames_per_file}"
        )
        if (
            self.parameters.noread
            or not self.parameters.noread
            and self.parameters.nwait != -1
        ):
            print(
                f"Approximative Estimated Scan Time: {self.in_pars['estimated_time_scan']}"
            )
        print("=================================================================")

    def get_projection_scan(self):
        """
        Returns a list of existing projection scans in tomo sequence.
        Returns an error message if no sequence has been launched. 
        """
        try:
            return self.list_proj_scans[0]
        except AttributeError:
            raise Exception("Error no tomo sequence has been launched")

    def get_reference_scan(self):
        """
        Returns a list of existing reference scans in tomo sequence.
        Returns an error message if no sequence has been launched or if no reference images has been acquired.
        """
        if (
            self.parameters.ref_images_at_start
            or self.parameters.ref_images_at_end
            or self.parameters.reference_groups
        ):
            try:
                return self.list_ref_scans[0]
            except AttributeError:
                raise Exception("Error no tomo sequence has been launched")
        else:
            raise Exception(
                "No reference images has been requested, please change scan option in setup"
            )

    def get_dark_scan(self):
        """
        Returns a list of existing dark scans in tomo sequence.
        Returns an error message if no sequence has been launched or if no dark images has been acquired.
        """
        if self.parameters.dark_images_at_start or self.parameters.dark_images_at_end:
            try:
                return self.list_dark_scans[0]
            except AttributeError:
                raise Exception("Error no tomo sequence has been launched")
        else:
            raise Exception(
                "No dark images has been requested, please change scan option in setup"
            )

    def show_config(self, all=False):
        """
        Allows to see parameters of all objects related to tomo
        """
        log_info(self, "show_config() entering")

        super().show_config()

        # if all is requested, show all other parameter sets
        if all == True:
            print("\nUsage: %s.reference.parameters\n" % self.tomo_name)
            self.fasttomo.reference.show_config()
            print("\nUsage: %s.optic.parameters\n" % self.tomo_name)
            self.optic.show_config()
            print("Usage: %s.shutter.parameters\n" % self.tomo_name)
            self.shutter.show_config()
            print("Usage: %s.saving.parameters\n" % self.tomo_name)
            self.saving.show_config()

        log_info(self, "show_config() leaving")

    def save_scan_config(self, directory, all=False):
        """
        Allows to save in a specified directory:
            - scan parameters 
            - reference, optic, shutter and saving parameters if all is set to True
        """
        # Save the scan parameters
        super().save_scan_config(directory)

        # if all is requested, save all other parameter sets
        if all == True:
            self.tomo_ccd.save_scan_config(directory)
            self.fasttomo.reference.save_scan_config(directory)
            self.optic.save_scan_config(directory)
            self.shutter.save_scan_config(directory)
            self.saving.save_scan_config(directory)

    def load_scan_config(self, directory, all=False):
        """
        Allows to load from a specified directory:
            - scan parameters 
            - reference, optic, shutter and saving parameters if all is set to True
        """
        # Load the scan paramters
        super().load_scan_config(directory)

        # if all is requested, load all other parameter sets
        if all == True:
            self.tomo_ccd.save_scan_config(directory)
            self.fasttomo.reference.load_scan_config(directory)
            self.optic.load_scan_config(directory)
            self.shutter.load_scan_config(directory)
            self.saving.load_scan_config(directory)

    def reset(self, all=False):
        """
        Allows to remove from redis and set to default values:
            - scan parameters
            - reference, optic, shutter and saving parameters if all is set to True
        """
        log_info(self, "reset() entering")

        # Remove the scan paramters
        super().reset()

        # if all is requested, remove all other parameter sets
        if all == True:
            self.tomo_ccd.reset()
            self.fasttomo.reference.reset()
            self.optic.reset()
            self.shutter.reset()
            self.saving.reset()

        log_info(self, "reset() leaving")
