import datetime
import tomo
import numpy
from tomo.TomoTools import TomoTools
from bliss.controllers.motor import CalcController
from tomo.pcotomo.PcoTomoMusst import TriggerType
from bliss.scanning.chain import AcquisitionChannel
import os

class MusstWaitTurnCalc(object):
    """ 
    """
    def __init__(self, name, source_name, data_per_turn, factor, dest_name):
        """
        """
        self.name = name
        self._source_name = source_name
        self._data_per_turn = data_per_turn
        self._factor = factor
        self._dest_name = dest_name
        self._data = numpy.array([],dtype=numpy.int32)
        self._turn = 1
    
    def __call__(self, sender, data_dict):
        """
        Function called at each new data coming from source channel
        """
        data = data_dict.get(self._source_name, None)
        
        if data is None:
            return {}

        self._data = numpy.append(self._data,data)
        
        if len(self._data) > self._turn*self._data_per_turn:
            wturn = int((self._data[self._turn*self._data_per_turn] - self._data[self._turn*self._data_per_turn-1])/self._factor/360)
            self._turn += 1
        else:
            return {}
        
        return {f'{self._dest_name}' : wturn}

    @property
    def acquisition_channels(self):
        """
        Returns AcquisitionChannel associated to calculation device
        """
        return [AcquisitionChannel(f'{self._dest_name}',numpy.int,())]     

class MusstStartTimeCalc(object):
    """ 
    """
    def __init__(self, name, source_name, musst_master, data_per_turn, factor, dest_name):
        """
        """
        self.name = name
        self._source_name = source_name
        self._data_per_turn = data_per_turn
        self._factor = factor
        self._dest_name = dest_name
        self._musst_master = musst_master
        self._data = numpy.array([],dtype=numpy.int32)
        self._turn = 0
        self._overflow = 0
        self._last_data = None
    
    def __call__(self, sender, data_dict):
        """
        Function called at each new data coming from source channel
        """
        data = data_dict.get(self._source_name, None)
        
        if data is None:
            return {}
        
        self._data = numpy.append(self._data,data)
        
        if self._last_data is None:
           self._last_data = self._data[0] 
           
        raw_data = numpy.append(self._last_data,data)
        abs_data = raw_data.astype(numpy.float)
        
        if len(self._data) >= 2:
            over_idxs = numpy.where(numpy.diff(abs_data) != numpy.diff(raw_data))[0]
            abs_data += self._overflow * 2**32
            for idx in over_idxs:
                ovr_sign = raw_data[idx] > 0 and 1. or -1.
                abs_data[idx+1:] += ovr_sign * 2**32
                self._overflow += ovr_sign
        
        self._last_data = raw_data[-1]
        
        if len(self._data) > self._turn*self._data_per_turn:
            start_scan_idx = len(self._data) - self._turn*self._data_per_turn - 1
            stime = self._musst_master._start_epoch + (abs_data[start_scan_idx+1])/self._factor
            self._turn += 1
        else:
            return {}
        
        return {f'{self._dest_name}' : datetime.datetime.fromtimestamp(stime).isoformat(' ')}

    @property
    def acquisition_channels(self):
        """
        Returns AcquisitionChannel associated to calculation device
        """
        return [AcquisitionChannel(f'{self._dest_name}',str,())]      
        
class MusstWaitTimeCalc(object):
    """ 
    """
    def __init__(self, name, source_name, data_per_turn, factor, dest_name):
        """
        """
        self.name = name
        self._source_name = source_name
        self._data_per_turn = data_per_turn
        self._factor = factor
        self._dest_name = dest_name
        self._data = numpy.array([],dtype=numpy.int32)
        self._turn = 1
    
    def __call__(self, sender, data_dict):
        """
        Function called at each new data coming from source channel
        """
        data = data_dict.get(self._source_name, None)
        
        if data is None:
            return {}

        self._data = numpy.append(self._data,data)
        
        if len(self._data) > self._turn*self._data_per_turn:
            wtime = float(self._data[self._turn*self._data_per_turn] - self._data[self._turn*self._data_per_turn-1])/self._factor
            self._turn += 1
        else:
            return {}
        
        return {f'{self._dest_name}' : wtime}

    @property
    def acquisition_channels(self):
        """
        Returns AcquisitionChannel associated to calculation device
        """
        return [AcquisitionChannel(f'{self._dest_name}',numpy.float,())]      

class PcoTomoTools(TomoTools):

    def __init__(self, tomo):
        """
        Parameters
        ----------
        tomo_ccd : object
            tomo detector object
        parameters : ParametersWardrobe 
            tomo parameters
        reference : object
            tomo reference object
        tomo : Tomo object (ex: HrPcoTomo) 
            contains all info about tomo (hardware, parameters)
        """
        
        self.tomo = tomo
        self.tomo_ccd = tomo.tomo_ccd 
        self.reference = tomo.reference
        self.parameters = tomo.parameters
        
    def check_params(self):
        """
        Checks if scan parameters are consistant
        """
        
        if self.tomo.rotation_axis.name == 'hrsrot' and self.tomo.tomo_scan.in_pars['scan_speed'] >= 1810:
            raise Exception(f"Error, scan speed is too high for this motor (should be <= 1200 °/s)")
        # must be checked, new controller for rotation on mr    
        #elif self.tomo.rotation_axis.name == 'mrsrot' and self.tomo.tomo_scan.in_pars['speed'] > 600:
        #    raise Exception(f"Error, scan speed is too high for this motor (should be <= 600 °/s)")
                
        if self.tomo.pcotomo.parameters.noread:
            totimg = self.parameters.tomo_n * self.tomo.pcotomo.parameters.nloop
        else:
            totimg = self.parameters.tomo_n
        
        if totimg > self.tomo_ccd.detector.camera.max_nb_images:
            raise Exception(f"Error, Could not acquire {totimg} frames in pco memory\n"
                            +f"Current max number of frames is {self.tomo_ccd.detector.camera.max_nb_images} \n")     
        
        if self.parameters.exposure_time <= 0:
            raise ValueError("Error, dead time + latency time > expo time ==> Change parameters") 
            
        print("<<<Parameters are consistant !>>>")
 
    def continuous_scan_time(self, motor, start_pos):
        """
        Estimates continuous motor scan time 
        """
        speed = self.tomo.tomo_scan.in_pars['trange'] / self.tomo.tomo_scan.in_pars['time']
        acctime = speed / motor.acceleration
        accdisp =  speed * acctime / 2
        
        undershoot,undershoot_start_margin = self.tomo.tomo_scan.calculate_undershoot(motor,start_pos,self.tomo.tomo_scan.in_pars['time'],
                                                                                 self.tomo.pcotomo.parameters.start_turn)
        disp = undershoot + undershoot_start_margin
        
        scan_time = 0

        disp += self.tomo.pcotomo.parameters.nloop*self.tomo.tomo_scan.in_pars['trange']  
            
        if self.tomo.pcotomo.parameters.nloop > 1:
            delta = (start_pos + self.tomo.tomo_scan.in_pars['trange'])%360
            if delta > 0:
                delta = 360 - delta  
            if self.tomo.parameters.trigger_type == TriggerType.TIME:
                delta = self.tomo.tomo_scan.in_pars['trange'] + delta
            if self.tomo.pcotomo.parameters.nwait < 0:
                if self.tomo.pcotomo.parameters.nwait < -1:
                    delta += abs(self.tomo.pcotomo.parameters.nwait) * 360 
            else:
                delta += self.tomo.pcotomo.parameters.nwait * 360
        else:
            delta = 0
            
        disp += delta*self.tomo.pcotomo.parameters.nloop
        
        scan_time += self.mot_disp_time(motor,disp,speed) 
        
        return scan_time  

    def estimate_ref_scan_duration(self):
        # ref motor
        ref_mot = self.reference.ref_motors[0]
        if isinstance(ref_mot.controller, CalcController):
            unit_time = 0.0
        else:
            init_pos = self.reference.parameters.in_beam_position[0]
            target_pos = init_pos + self.reference.parameters.out_of_beam_displacement[0]
            # back and forth
            unit_time = 2 * self.mot_disp_time(ref_mot,target_pos-init_pos,ref_mot.velocity)
            unit_time += 2 * self.reference.settle_time

        motor_time = 0
        ref_img = 0
            
        if self.parameters.ref_images_at_start:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
            
        if self.parameters.ref_images_at_end:
            ref_img += self.parameters.ref_n
            motor_time += unit_time
                
        image_time = ref_img * self.tomo.tomo_scan.in_pars['scan_point_time']
            
        return motor_time + image_time
