from bliss.scanning.scan import ScanPreset
from bliss.common import session
import time


class PcoTomoShutterPreset(ScanPreset):
    
    def __init__(self, tomo_shutter, tomo_musst, mux):
        self.shutter = tomo_shutter
        self.musst = tomo_musst
        self.mux = mux
        
    def prepare(self,scan):
        if self.shutter.fast_shutter_active:
            print(f"Closing the fast shutter")
            self.shutter.fast_shutter_close() 
        
    def start(self, scan):
        if self.shutter.fast_shutter_active:
            print(f"Opening the fast shutter")
            self.shutter.fast_shutter_open()
                
        if self.musst.sync_shutter_active:
            self.mux.switch("SHMODE","MUSST")
    
    def stop(self, scan):  
        if self.musst.sync_shutter_active:
            self.mux.switch("SHMODE","SOFT")
        
        if self.shutter.fast_shutter_active:
            print(f"Closing the fast shutter")
            self.shutter.fast_shutter_close() 

class FastShutterPreset(ScanPreset):
    def __init__(self, tomo_shutter, tomo_musst):
        self.shutter = tomo_shutter
        self.musst = tomo_musst
        
    def prepare(self,scan):
        print(f"Preparing shutter for {scan.name}\n")
        
    def start(self,scan):
        print(f"Opening the fast shutter")
        if self.shutter.fast_shutter_active:
            self.shutter.fast_shutter_open()
        if self.musst.sync_shutter_active:
            self.musst.open_sync_shutter()
        
    def stop(self,scan):
        print(f"Closing the fast shutter")
        if self.shutter.fast_shutter_active:
            self.shutter.fast_shutter_close()
        if self.musst.sync_shutter_active:
            self.musst.close_sync_shutter()
