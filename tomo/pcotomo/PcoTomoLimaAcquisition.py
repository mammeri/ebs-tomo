from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.chain import ChainNode
from bliss import setup_globals
import time

class PcoTomoLimaAcquisitionMaster(LimaAcquisitionMaster):
    
    def __init__(self, device, ctrl_params=None, **acq_params):
        LimaAcquisitionMaster.__init__(self, device, ctrl_params, **acq_params)
    
    def start(self):
        LimaAcquisitionMaster.start(self)
        if self._LimaAcquisitionMaster__sequence_index > 0:
            setup_globals.config.get('multiplexer_tomo').switch('IOTRIG','ON')
    def wait_ready(self):
        if self._LimaAcquisitionMaster__sequence_index > 0:
            setup_globals.config.get('multiplexer_tomo').switch('IOTRIG','OFF')
        LimaAcquisitionMaster.wait_ready(self) 
        
