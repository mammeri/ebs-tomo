from bliss.scanning.chain import AcquisitionChain, AcquisitionChannel, AcquisitionMaster
from bliss.scanning.scan import Scan,ScanState, DataWatchCallback, WatchdogCallback
from bliss import setup_globals
from bliss.scanning.scan import is_zerod
from bliss.common import session, scans
import sys
import gevent
import numpy as np
import bliss
import time
import datetime
import shutil
from bliss.common import axis
from bliss.common import event
from bliss.config.static import get_config


def BOLD(msg):
    return "\033[1m{0}\033[0m".format(msg)

class PcoTomoScanDisplay(DataWatchCallback):
    """
    Class to display data during a tomo scan
    """

    HEADER = (
        "Scan {scan_nb} {start_time_str} {filename} "
        + "{session_name} user = {user_name}\n"
        + "{title}"
    )

    STATE_MSG = {
        ScanState.PREPARING: "Preparing",
        ScanState.STARTING: "Running",
        ScanState.STOPPING: "Stopping",
    }

    def __init__(self, trigger_name=None, motors=list(), limas=list()):
        """
        Initialisation of all scan members
        """
        self.__motors = motors
        self.__motor_names = [motor.name for motor in motors]
        self.__limas = limas
        self.__trig_name = trigger_name

    def on_state(self, state):
        """
        If True "on_scan_data" will be called at 
        each scan state: PREPARING, STARTING and STOPPING
        """
        return True

    def on_scan_new(self, scan, info):
        """
        Displays scan info: scan number, data saving path, user name, ...
        Called when the scan is about to start
        
        scan -- is the scan object
        info -- is the dict of information about this scan
        """

        print(self.HEADER.format(**info))
        self.__state = None
        self.__infos = dict()
        for name in self.__motor_names:
            self.__infos[name] = "----.---"
        if self.__trig_name is not None:
            self.__infos["trig"] = 0
        for lima in self.__limas:
            self.__infos[lima.name] = 0
        self.__start_acq_time = 0
        self.__acq_time = 0
        self.__start_saving_time = 0
        self.__saving_time = 0
        self.__prepare_time = dict()
        self.__scan = scan
        for lima in self.__limas:
            self.__prepare_time[lima.name] = 0
        self.__musst_acq_dev = scan.acq_chain.nodes_list[1]
        self.__end = False
        
    def on_scan_end(self, info):
        """
        Displays scan execution time.
        Called at the end of the scan.
        """
        start = datetime.datetime.fromtimestamp(info["start_timestamp"])
        end = datetime.datetime.fromtimestamp(time.time())
        msg = "\nFinished (took {0})\n".format(end - start)
        print(msg)

    def on_scan_data(self, data_events, data_nodes, info):
        """
        This callback is called when new data is emitted
        Displays:
            - motor position
            - trigger number
            - last image recorded
            - last image saved
            - difference between trigger number and last image recorded
            - data saving rate in MB/s
        Checks if difference between trigger number and last image 
        recorded is lower than limit value otherwise stops the scan
        """
        # look for scan state
        state = info.get("state", None)
        if state != self.__state:
            if state == ScanState.STARTING:
                for lima in self.__limas:
                    self.__prepare_time[lima.name] =  self.__scan.statistics.elapsed_time[lima.name+'.prepare']
                    msg = f"\n{lima.name} prepare took {round(self.__prepare_time[lima.name][0]*10**3,2)} ms"
                    print(msg)
            self.__state = state
            state_msg = self.STATE_MSG.get(state, None)
            if state_msg is not None:
                print("\n{0} ...".format(state_msg))
            
        if self.__state == ScanState.PREPARING:
            # print current motor positions
            msg = ""
            for motor in self.__motors:
                name = motor.name
                value = "{0:8.3f}".format(motor.position)
                msg += "{0} {1}  ".format(BOLD(name), value)
                self.__infos[name] = value
            if len(msg):
                print(msg + "\r", end="")
            return

        if self.__state == ScanState.STARTING:
            # print last motor pos recorded
            for acqdev, signals in data_events.items():
                for signal in signals:
                    # print("... {0}".format(signal))
                    data_node = data_nodes.get(acqdev)
                    if is_zerod(data_node):
                        channel_name = data_node.name
                        # print("    data_name {0}".format(channel_name))
                        prefix,_,name = channel_name.rpartition(":")
                        if name in self.__motor_names:
                            last_pos = data_node.get(-1)
                            self.__infos[name] = "{0:8.3f}".format(last_pos)
                        #if "start_time" in channel_name:
                            #self.__scan_start_time = (datetime.datetime.fromisoformat(data_node.get(-1)) - datetime.datetime.fromtimestamp(PcoTomoScanDisplay.START_TIME)).total_seconds()
                            #msg = f"Time before starting acquiring projections: {round(self.__scan_start_time,2)} s"
                            #print(msg)
                        if channel_name == self.__trig_name:
                            self.__infos["trig"] = len(data_node)
                            scan = int((self.__infos["trig"])/(info['technique']['scan']['tomo_n']*info['technique']['scan']['nb_tomo']))
                            scan_start = scan * info['technique']['scan']['tomo_n'] * info['technique']['scan']['nb_tomo']
                            if self.__infos["trig"] > scan_start:
                                self.__end = False
                                self.__acq_time = 0
                                self.__start_acq_time = self.__musst_acq_dev._start_epoch + data_node.get(scan_start) 
                                   
                                
            # print last images acquired and saved
            for cam in self.__limas:
                if info['technique']['scan']['no_saving_between_tomo']:
                    last_rec = cam.camera.last_img_recorded
                    last_saved = cam._proxy.last_image_saved + 1
                else: 
                    if cam.camera.last_img_recorded in [0,cam._proxy.acq_nb_frames]:
                        last_rec = self.__infos["trig"]
                    else:
                        last_rec = int((self.__infos["trig"]-1) / cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam.camera.last_img_recorded
                    if cam._proxy.last_image_saved + 1 not in [0,cam._proxy.acq_nb_frames]:
                        last_saved = int((self.__infos["trig"]-1)/cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam._proxy.last_image_saved + 1
                    else:
                        last_saved = cam._proxy.saving_next_number * cam._proxy.saving_frame_per_file
                diff_trigger = self.__infos["trig"] - last_rec
                if diff_trigger < 0:
                    diff_trigger = 0
                scan = int(self.__infos["trig"] / info['technique']['scan']['tomo_n']) + 1
                if self.__infos["trig"] % info['technique']['scan']['tomo_n'] == 0:
                    scan -= 1
                self.__infos[cam.name] = "{0} (saved {1} diff {2} scan {3})".format(last_rec, last_saved, diff_trigger, scan)

            total_scan = info['technique']['scan']['nb_tomo']*info['technique']['scan']['nb_loop']
            if self.__acq_time != 0 and self.__saving_time != 0 and last_saved % info['technique']['scan']['tomo_n'] == 0 and not self.__end:
                #print('\033[2K\033[1G', end="")
                msg = f"Acquisition of scan {scan} / {total_scan} took {round(self.__acq_time,2)} s or {round(self.__acq_time/60,2)} min"
                print("\n\n" + msg, end="")
                msg = f"\nReadout/Saving of scan {scan} / {total_scan} took {round(self.__saving_time,2)} s or {round(self.__saving_time/60,2)} min\n"
                print(msg + "\r", end="")
                #put cursor back to previous lines allowing to overwriting each printed line
                if scan != total_scan:
                    print("\033[F"*5)
                self.__end = True
            
            msg = ""
            for (name, value) in self.__infos.items():
                msg += "{0} {1}  ".format(BOLD(name), value)
                
            if len(msg):
                # erase line and go to beginning of line
                terminal_width = shutil.get_terminal_size()[0]
                if len(msg) >= terminal_width:
                    lstr = []
                    #divide the entire string into multiples strings each one fitting terminal width
                    for part in range(int(len(msg)/terminal_width)):
                        lstr.append(msg[terminal_width*part:terminal_width*(part+1)])
                    lstr.append(msg[terminal_width*(part+1):])
                    for msg in lstr:
                        print(msg)
                    #put cursor back to previous lines allowing to overwriting each printed line
                    print("\033[F"*(len(lstr)+1))
                else:
                    print(msg + "\r", end="")
            
            for cam in self.__limas:
                if info['technique']['scan']['no_saving_between_tomo']:
                    if cam._proxy.last_image_saved + 1 == 0:
                        self.__start_saving_time = time.time()
                    last_saved = cam._proxy.last_image_saved + 1
                else:
                    if cam._proxy.last_image_saved + 1 not in [0,cam._proxy.acq_nb_frames]:
                        last_saved = int((self.__infos["trig"]-1)/cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam._proxy.last_image_saved + 1
                    else:
                        last_saved = cam._proxy.saving_next_number * cam._proxy.saving_frame_per_file
                        self.__start_saving_time = time.time()
                if cam._proxy.saving_mode == 'AUTO_FRAME':
                    if last_rec == self.__infos["trig"] and cam.camera.last_img_recorded == cam._proxy.acq_nb_frames:
                        acq_time = (datetime.datetime.fromtimestamp(time.time()) - datetime.datetime.fromtimestamp(self.__start_acq_time)).total_seconds() 
                        if self.__acq_time == 0:
                            self.__acq_time = acq_time
                        while last_saved != self.__infos["trig"] and cam.acquisition.status == 'Running':
                            if info['technique']['scan']['no_saving_between_tomo']:
                                last_saved = cam._proxy.last_image_saved + 1
                            else: 
                                if cam._proxy.last_image_saved + 1 not in [0,cam._proxy.acq_nb_frames]:
                                    last_saved = int((self.__infos["trig"]-1)/cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam._proxy.last_image_saved + 1
                                else:
                                    last_saved = cam._proxy.saving_next_number * cam._proxy.saving_frame_per_file
                            msg = "Saving of scan {0} / {1} in progress... {2} saved {3} data readout rate {4} MB/s file saving rate {5} MB/s)".format(scan, total_scan, BOLD(cam.name), last_saved, round(cam._proxy.saving_statistics[3]/1024/1024,2), round(cam._proxy.saving_statistics[0]/1024/1024,2))
                            terminal_width = shutil.get_terminal_size()[0]
                            if len(msg) >= terminal_width:
                                lstr = []
                                #divide the entire string into multiples strings each one fitting terminal width
                                for part in range(int(len(msg)/terminal_width)):
                                    lstr.append(msg[terminal_width*part:terminal_width*(part+1)])
                                lstr.append(msg[terminal_width*(part+1):])
                                for msg in lstr:
                                    print(msg)
                                #put cursor back to previous lines allowing to overwriting each printed line
                                print("\033[F"*(len(lstr)+1))
                            else:
                                print(msg + "\r", end="")
                        saving_time = (datetime.datetime.fromtimestamp(time.time()) - datetime.datetime.fromtimestamp(self.__start_saving_time)).total_seconds() 
                        if round(saving_time) != 0:
                            self.__saving_time = saving_time
                        
            return


        if self.__state == ScanState.STOPPING:
            # print last motor position
            msg = ""
            for motor in self.__motors:
                msg += "{0} {1:8.3f}  ".format(BOLD(motor.name), motor.position)
            if len(msg):
                print(msg + "\r", end="")
            return
                
class PcoTomoScanWatchdog(WatchdogCallback):
    
    def __init__(self, trigger_name=None, limas=list(), trig_difference=100):
        """
        watchdog_timeout -- is the maximum calling frequency of **on_timeout**
        method.
        """
        self.__trig_difference = trig_difference
        self.__watchdog_timeout = 900.0
        self.__limas = limas
        self.__trig_name = trigger_name
        self.__stop_reason = ''

    @property
    def timeout(self):
        return self.__watchdog_timeout

    def on_timeout(self):
        """
        This method is called when **watchdog_timeout** elapsed it means
        that no data event is received for the time specified by
        **watchdog_timeout**
        """
        for cam in self.__limas:
            #check if lima is saving    
            if cam.camera.last_img_recorded != cam._proxy.acq_nb_frames:
                self.__stop_reason = f'No data received since {self.__watchdog_timeout} seconds'
                raise Exception   

    def on_scan_new(self, scan, scan_info):
        """
        Called when scan is starting
        """
        self.__state = None
        self.__trig = 0
    
    def on_scan_data(self, data_events, nodes, scan_info):
        """
        Called when new data are emitted by the scan.  This method should
        raise en exception to stop the scan.  All exception will
        bubble-up exception the **StopIteration**.  This one will just
        stop the scan.
        """
        # look for scan state
        state = scan_info.get("state", None)
        if state != self.__state:
            if state == ScanState.STARTING:
                self.__watchdog_timeout = 20.0
            self.__state = state
        
        if self.__state == ScanState.STARTING:
            # print last motor pos recorded
            for acqdev, signals in data_events.items():
                for signal in signals:
                    # print("... {0}".format(signal))
                    node = nodes.get(acqdev)
                    if is_zerod(node):
                        channel_name = node.name
                        # print("    data_name {0}".format(channel_name))
                        prefix,_,name = channel_name.rpartition(":")
                        if channel_name == self.__trig_name:
                            self.__trig = len(node)
                            
        
            for cam in self.__limas:
                loop = int((self.__trig-1)/cam._proxy.acq_nb_frames) + 1
                if scan_info['technique']['scan']['no_saving_between_tomo']:
                    last_rec = cam.camera.last_img_recorded
                else:
                    if cam.camera.last_img_recorded in [0,cam._proxy.acq_nb_frames]:
                        last_rec = self.__trig
                    else:
                        last_rec = int(self.__trig / cam._proxy.acq_nb_frames)*cam._proxy.acq_nb_frames + cam.camera.last_img_recorded
                diff_trigger = abs(self.__trig - last_rec)
                if self.__trig_difference != -1 and diff_trigger >= self.__trig_difference:
                    self.__stop_reason = f"\nSYNCHRO LOST: loop {loop}\n \
                    Frame/Trigger difference exceed {self.__trig_difference} ({diff_trigger})\n"
                    raise Exception   
        return

    def on_scan_end(self,scan_info):
        if len(self.__stop_reason):
            print(BOLD(self.__stop_reason) + "\n")

              
