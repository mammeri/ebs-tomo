from bliss import setup_globals, global_map
from bliss.scanning.acquisition.musst import *
from bliss.scanning.acquisition.calc import CalcChannelAcquisitionSlave
from bliss.common import session
from bliss.common.logtools import log_info

import tomo

from enum import Enum


class TriggerType(Enum):
    POSITION = 0
    TIME = 1


class TomoMusst:
    """
    Class for tomo musst object.
    The class implements methods to handle musst in tomo acquisition.
    
    ***Attributes***
    name : string
        class name
    tomo_name : string
        name of tomo object
    card : Bliss musst object
        musst controller
    motor : Bliss axis object
        axis used for musst triggers generation
    enc_channel : Bliss musst channel object
        musst channel corresponding to motor encoder
    motchan : Bliss musst channel id object
        musst channel id corresponding to motor encoder
    storelist : list
        name of variables/data recorded by musst device during tomo acquisition
    tmrcfg : Bliss musst timer object
        musst clock value before tomo acquisition
    """

    def __init__(self, tomo_name, card, motor, sync_shut_time=None, *args, **kwargs):

        self.name = tomo_name + ".musst"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")

        self.tomo_name = tomo_name

        self.card = card

        self.motor = motor
        # get musst channel associated to motor
        try:
            motor_name = getattr(self.motor, 'original_name', self.motor.name)
            self.enc_channel = self.card.get_channel_by_name(motor_name)
            self.mot_chan = self.enc_channel.channel_id
        except RuntimeError:
            self.enc_channel = None
            self.mot_chan = None
            print(
                "No musst channel associated to this motor! \
            \nOnly step scans or continuous scans with soft synchronization are possible!"
            )

        self.storelist = ["timer_raw", f"{self.motor.name}_raw"]
        self.tmrcfg = None
        self.sync_shutter_active = False
        if sync_shut_time is not None and sync_shut_time > 0:
            self.sync_shutter_active = True
        self.sync_shut_time = sync_shut_time

        log_info(self, "__init__() leaving")

    def sync_motor(self):
        """
        Sets musst channel value with motor encoder value. 
        """
        self.motor.wait_move()
        self.enc_channel.value = int(
            round(
                (
                    self.motor.position * self.motor.steps_per_unit * self.motor.sign
                    + 0.5
                )
                // 1
            )
        )

    def set_timer_clock(self, scan_time):
        """
        Sets musst clock with the most adapted value (highest precision with 
        long enough duration) for tomo acquisition.
        """
        clock_list = [1e3, 10e3, 100e3, 1e6, 10e6, 50e6]
        clock_cmds = ["1KHZ", "10KHZ", "100KHZ", "1MHZ", "10MHZ", "50MHZ"]
        clock_index = clock_list.index(50e6)

        while clock_index >= 0 and scan_time > (pow(2, 32) / clock_list[clock_index]):
            clock_index -= 1

        self.card.TMRCFG = clock_cmds[clock_index]

    def set_timer_back(self):
        """
        Sets musst clock to previous value (before tomo acquisition).
        """
        if self.tmrcfg is not None:
            self.card.TMRCFG = self.tmrcfg[0]

    def prepare_continuous(
        self,
        start_pos,
        exposure_time,
        latency_time,
        nb_points,
        scan_step_size,
        scan_point_time,
        scan_time,
        accumulation=1,
        trigger_type=TriggerType.TIME,
    ):
        """
        Configures musst for hard continuous scan.
        Musst generates pulses in position or in time and gates in time to control eventually a shutter.
        Musst program handles accumulation mode.
        """

        log_info(self, "prepare_continuous() entering")
        self.card.CLEAR
        self.sync_motor()

        # mot_zero is a list of encoder values corresponding to
        # start position tomo parameter
        mot_start = int(
            round((start_pos * self.motor.steps_per_unit * self.motor.sign + 0.5) // 1)
        )

        self.tmrcfg = self.card.TMRCFG
        self.set_timer_clock(scan_time)

        trig_delta = 0
        if accumulation > 1:
            # convert exposure time to timer ticks
            trig_delta = (scan_point_time - latency_time) / accumulation

        gatewidth = int(
            round(
                (
                    (
                        ((accumulation - 1) * trig_delta + exposure_time / accumulation)
                        * self.card.get_timer_factor()
                    )
                    + 0.5
                )
                // 1
            )
        )

        trig_delta = int(round(trig_delta * self.card.get_timer_factor() + 0.5) // 1)

        # --- position mode
        if trigger_type == TriggerType.POSITION:
            pos_delta = scan_step_size * self.motor.steps_per_unit * self.motor.sign

            # musst will correct step size at each image
            enc_delta = int(pos_delta)
            mot_error = abs(int((enc_delta - pos_delta) * 2 ** 31))

            # configure musst variables used by 'ftomo_acc' program
            # corresponding to a reference group
            musst_vars = {
                "MOTSTART": int(mot_start),
                "NPTS": int(nb_points),
                "PTMODE": 1,
                "PTDELTA": int(pos_delta),
                "GATEWIDTH": gatewidth,
                "MOTERR": mot_error,
                "NTRIGS": int(accumulation),
                "TRIGDELTA": trig_delta,
            }

        # --- time mode
        if trigger_type == TriggerType.TIME:
            time_delta = (
                scan_point_time * self.card.get_timer_factor() * self.motor.sign
            )

            musst_vars = {
                "MOTSTART": mot_start,
                "NPTS": int(nb_points),
                "PTMODE": 0,
                "PTDELTA": int(time_delta),
                "GATEWIDTH": gatewidth,
                "NTRIGS": int(accumulation),
                "TRIGDELTA": trig_delta,
            }

        # allows to replace alias 'MOTOR_CHANNEL' in 'ftomo_acc' program with channel associated to motor 'CH#'
        template_replacement = {"$MOTOR_CHANNEL$": "CH%d" % self.mot_chan}

        # MUSST acquisition master
        musst_master = MusstAcquisitionMaster(
            self.card,
            program="ftomo_acc.mprg",
            program_start_name="TOMO",
            program_abort_name="TOMO_CLEAN",
            program_template_replacement=template_replacement,
            vars=musst_vars,
        )

        # MUSST acquisition slave
        musst_slave = MusstAcquisitionSlave(
            self.card, store_list=self.storelist
        )  # store encoder and timer data

        log_info(self, "prepare_continuous() leaving")

        return musst_master, musst_slave

    def prepare_sweep(
        self, start_pos, nb_points, scan_step_size, undershoot, scan_time
    ):
        """
        Configures musst for sweep scan.
        Musst generates gates in position. 
        """

        log_info(self, "prepare_sweep() entering")

        self.sync_motor()

        # mot_zero is a list of encoder values corresponding to
        # start position tomo parameter
        mot_zero = int(
            round((start_pos * self.motor.steps_per_unit * self.motor.sign + 0.5) // 1)
        )

        self.tmrcfg = self.card.TMRCFG
        self.set_timer_clock(scan_time)

        pos_delta = scan_step_size * self.motor.steps_per_unit * self.motor.sign
        # musst will correct step size at each image
        enc_delta = int(pos_delta)
        mot_error = abs(int((enc_delta - pos_delta) * 2 ** 31))

        # configure musst variables used by 'ftomo_wo_gap' program
        # corresponding to a reference group
        first_iter_vars = {
            "POSSTART": mot_zero,
            "POSDELTA": enc_delta,
            "NPULSES": nb_points,
            "MOTERR": mot_error,
            "UNDERSHOOT": int(
                undershoot * self.motor.steps_per_unit * 0.9
            ),  # target value used for motor back movement to update target register
        }
        musst_vars = first_iter_vars

        # allows to replace alias 'MOTOR_CHANNEL' in 'ftomo_wo_gap' program with channel associated to motor 'CH#'
        template_replacement = {"$MOTOR_CHANNEL$": "CH%d" % self.mot_chan}

        # MUSST acquisition master
        musst_master = MusstAcquisitionMaster(
            self.card,
            program="ftomo_wo_gap.mprg",
            program_start_name="SWEEP",
            program_abort_name="SWEEPCLEAN",
            program_template_replacement=template_replacement,
            vars=musst_vars,
        )

        # MUSST acquisition slave
        musst_slave = MusstAcquisitionSlave(
            self.card, store_list=self.storelist
        )  # store encoder and timer data

        log_info(self, "prepare_sweep() leaving")
        return musst_master, musst_slave

    def prepare_soft(self, exposure_time, nb_points, scan_time=None):
        """
        Configures musst for soft continuous and step scan.
        Uses RUNCT musst command. 
        """

        log_info(self, "prepare_soft() entering")

        self.tmrcfg = self.card.TMRCFG

        if scan_time is not None:
            self.set_timer_clock(scan_time)

        # will run RUNCT command
        musst_acq = tomo.SoftTriggerMusstAcquisitionMaster(
            self.card, exposure_time, nb_points
        )

        log_info(self, "prepare_soft() leaving")

        return musst_acq

    def prepare_calculation(self, musst_acq, data_per_point=1, data_per_line=None):
        """
        Configures calculation devices.
        Used to convert encoder and timer data from the musst in degrees and seconds.  
        """

        log_info(self, "prepare_calculation() entering")

        # use musst timer factor to convert timer musst data into seconds
        name = "elapsed_time"
        data_time = tomo.MusstConvDataCalc(
            name,
            self.card,
            "timer_raw",
            data_per_point,
            data_per_line,
            self.card.get_timer_factor(),
            name,
        )
        calc_time_device = CalcChannelAcquisitionSlave(
            "calc_time_device", (musst_acq,), data_time, data_time.acquisition_channels
        )

        # use motor steps per unit to convert encoder musst data into degrees
        name = f"calc_{self.motor.name}"
        data_pos = tomo.MusstConvDataCalc(
            name,
            self.card,
            f"{self.motor.name}_raw",
            data_per_point,
            data_per_line,
            self.motor.steps_per_unit * self.motor.sign,
            name,
        )
        calc_pos_device = CalcChannelAcquisitionSlave(
            "calc_pos_device", (musst_acq,), data_pos, data_pos.acquisition_channels
        )

        calc_device = [calc_time_device, calc_pos_device]

        log_info(self, "prepare_calculation() leaving")

        return calc_device
