import sys
import gevent
import numpy as np
import time
import os
import glob

import PyTango
import bliss

from bliss import global_map, setup_globals, current_session
from bliss.config.static import get_config
from bliss.common.logtools import log_info, log_debug, log_warning
from bliss.common import session
from bliss.shell.cli.user_dialog import *
from bliss.shell.cli.pt_widgets import BlissDialog
from bliss.shell.dialog.controller.lima_dialogs import *
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan, ScanState, DataWatchCallback
from bliss.scanning.toolbox import ChainBuilder

from bliss.controllers.lima.lima_base import Lima
from bliss.controllers.lima.roi import RoiProfileController

from tomo.ScanDisplay import LimaTakeDisplay

from tomo.TomoParameters import TomoParameters


class TomoCcd(TomoParameters):
    """
    Class for tomo detector object.
    The class implements methods to handle detectors in tomo acquisition.
    
    ***Attributes***
    name : string
        class name
    tomo_name : string
        name of tomo object
    detector : Bliss detector object
        detector controller
    det_proxy : tango device proxy
        detector proxy
    has_shutter : boolean
        flag to store the shutter capability of the detector
    is_frelon : boolean
        flag to store if detector type is frelon or not
    list_det : list of bliss detector objects
        list of detectors availables in the measurement group
    saved_configurations : dict
        allows to save current detector configuration
    in_pars : dict    
        detector calculated parameters
    def_chain_single_mode : Bliss default chain object
        default chain configuration for single acquisition mode
    def_chain_acc_mode : Bliss default chain object
        default chain configuration for accumulation acquisition mode    
        
    Parameters
    ----------
    active_detector : Bliss detector object
        currently selected detector controller
    readout_time : float
        detector readout time per frame (or sub-frame in accumulation) in seconds
    extra_time : float
        additional time in seconds to be added to detector exposure time + readout time 
    exposure_time : float
        detector exposure time per frame in seconds
    acc_max_expo_time : float
        detector maximum exposure time per sub-frame in seconds
    acq_mode : string
        detector acquisition mode (SINGLE or ACCUMULATION)
    image_roi : list
        list containing image roi (x starting point, y starting point, width, height) values
    image_binning : list
        list containing image binning (factor in width, factor in height) values
    """

    def __init__(self, tomo_name, tomo_config, *args, **kwargs):
        # init logging
        self.name = tomo_name + ".ccd"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")

        self.tomo_name = tomo_name
        self.saved_configurations = {}
        self.in_pars = {}

        param_name = self.tomo_name + f":ccd_parameters"
        ccd_defaults = {}
        ccd_defaults["active_detector"] = None
        ccd_defaults["readout_time"] = 0.0
        ccd_defaults["extra_time"] = 50e-6
        ccd_defaults["exposure_time"] = None
        ccd_defaults["acc_max_expo_time"] = None
        ccd_defaults["acq_mode"] = "SINGLE"
        ccd_defaults["image_roi"] = list()
        ccd_defaults["image_binning"] = list()

        self.detector = None
        self.det_proxy = None
        self.__has_shutter = False
        self.__is_frelon = False
        self.def_chain_single_mode = tomo_config["def_chain_single_mode"]
        self.def_chain_acc_mode = tomo_config["def_chain_acc_mode"]

        # Initialise the TomoParameters class
        super().__init__(param_name, ccd_defaults)

        # read available detectors from the active measurement group
        self.list_det = self.get_detectors_from_measurement_group()

        # Check last active detector or take the first configured
        self.parameters.switch("default")

        active_detector = None
        if self.parameters.active_detector is not None:
            active_detector = get_config().get(self.parameters.active_detector)

        if active_detector is not None and active_detector in self.list_det:
            self.init_detector(active_detector)
        elif len(self.list_det):
            self.init_detector(self.list_det[0])

        log_info(self, "__init__() leaving")

    def get_detector_name(self):
        """
        Returns tomo detector name
        """
        if self.detector is not None:
            return self.detector.name
        else:
            return "UNDEFINED !!"

    def get_detectors_from_measurement_group(self):
        """
        Returns list of detectors present in active measurement group
        """
        list_det = []

        # analyse the active measurement group (ACTIVE_MG)
        builder = ChainBuilder([])

        for node in builder.get_nodes_by_controller_type(Lima):
            list_det.append(node.controller)

        return list_det

    def verify_active_detector(self):
        """
        Checks if tomo detector selected is available in active measurement group
        """
        # update the list of available detectors
        self.list_det = self.get_detectors_from_measurement_group()

        # Detector is still avalable in the measurement group
        for det in self.list_det:
            if det.name == self.parameters.active_detector:
                if self.detector is None:
                    self.init_detector(det)
                return
        # Detector is no longer in the measurement group
        else:
            # if only one detector in measurement group, try to configure it
            det_n = len(self.list_det)

            if det_n == 1:
                print(
                    f"\nSwitching detector from {self.parameters.active_detector} to {self.list_det[0].name}\n"
                )
                self.init_detector(self.list_det[0])
                return

            # if none or several detectors found, raise an exception
            if det_n < 1:
                raise RuntimeError(
                    "DETECTOR ERROR : No detector defined in the active measurement group!\n"
                )

            if det_n > 1:
                raise RuntimeError(
                    "DETECTOR ERROR : More than one detector defined in the active measurement group!\n"
                    "Please select the detector to be used in the setup dialog"
                )

    def init_detector(self, detector):
        """
        Initialization of tomo detector.
        Checks communication with detector.
        Retrieves parameters related to detector.
        Resets readout_time.
        Checks shutter capability.
        Tests if camera type is frelon or not.
        """
        # try to reach detector server
        try:
            detector.camera
        except:
            # Communication error with tango device
            log_warning(self, detector.__info__())
            return

        self.detector = detector
        self.det_proxy = detector.proxy
        self.parameters.switch("default")
        self.parameters.active_detector = detector.name
        self.parameters.switch(detector.name)
        # when changing detector, reset readout_time to force calibration
        self.parameters.readout_time = 0

        # check if ccd has shutter capability
        sh_modes = self.det_proxy.getAttrStringValueList("shutter_mode")
        self.__has_shutter = "AUTO_FRAME" in sh_modes
        self.__is_frelon = detector.camera_type == "Frelon"

    def __info__(self):
        if self.detector is not None:
            info_str = f"{self.detector.name} info:\n"
            info_str += f"  detector_type     = {self.detector.camera_type}\n"
        else:
            info_str = "NO active detector !!\n"
        info_str += f"  acq_mode          = {self.parameters.acq_mode}\n"
        info_str += f"  exposure_time     = {self.parameters.exposure_time}\n"
        info_str += f"  acc_max_expo_time = {self.parameters.acc_max_expo_time}\n"
        info_str += f"  readout_time      = {self.parameters.readout_time}\n"
        info_str += f"  image_roi         = {self.parameters.image_roi}\n"
        info_str += f"  image_binning     = {self.parameters.image_binning}\n"
        return info_str

    def select_detector(self):
        """
        User dialog to select and initialize one detector among list of detectors present 
        in active measurement group.
        """
        # update the list of available detectors
        self.list_det = self.get_detectors_from_measurement_group()

        defval = 0
        values = list()
        for i in range(len(self.list_det)):
            if self.list_det[i] == self.detector:
                defval = i
            values.append((i, self.list_det[i].name))

        dlg = UserChoice(values=values, defval=defval)
        ret = BlissDialog([[dlg]], title="Ccd Select").show()

        if ret != False:
            self.init_detector(self.list_det[ret[dlg]])

    def calculate_parameters(self, exposure_time):
        """
        Calibrates detector to deduce readout time, number of subframes 
        and exposure time per subframe if accumulation.
        Calculates image size in megabytes.
        Saves results in detector parameters and in_pars attribute.
        """
        image_change = False

        self.detector.acquisition.expo_time = exposure_time

        # check modified parameters
        if exposure_time != self.parameters.exposure_time:
            self.parameters.exposure_time = exposure_time
            image_change = True

        if self.detector.acquisition.mode != self.parameters.acq_mode:
            self.parameters.acq_mode = self.detector.acquisition.mode
            image_change = True

        if self.parameters.acq_mode == "ACCUMULATION":
            if (
                self.detector.accumulation.max_expo_time
                != self.parameters.acc_max_expo_time
            ):
                self.parameters.acc_max_expo_time = (
                    self.detector.accumulation.max_expo_time
                )
                image_change = True

        roi_list = self.detector.image.roi
        if roi_list != list(self.parameters.image_roi):
            self.parameters.image_roi = roi_list
            image_change = True

        if self.detector.image.binning != self.parameters.image_binning:
            self.parameters.image_binning = self.detector.image.binning
            image_change = True

        if self.parameters.readout_time == 0:
            self.parameters.readout_time = self.calibrate_ccd_readout_time(
                exposure_time
            )
        else:
            self.parameters.readout_time = self.calibrate_ccd_readout_time(
                exposure_time, image_change
            )

        if self.parameters.acq_mode == "ACCUMULATION":
            self.in_pars["acc_nb_frames"] = self.detector.accumulation.nb_frames
            self.in_pars["acc_expo_time"] = self.detector.accumulation.expo_time
        else:
            self.in_pars["acc_nb_frames"] = 1
            self.in_pars["acc_expo_time"] = self.detector.acquisition.expo_time

        depth = self.detector.image.depth
        width = self.detector.image.width
        height = self.detector.image.height
        mbsize = depth * width * height / 1024 / 1024

        self.in_pars["image_mbsize"] = mbsize

    def setup(self):
        """
        User dialog allowing to:
        - select one detector among list of detectors availables in active measurement group
        - select acquisition mode (SINGLE or ACCUMULATION)
        - configure image saving (image format, number of frames per file)
        - configure image flipping, binning and rois
        """

        value_list = [
            ("select", "Detector selection"),
            ("mode", "Acquisition mode"),
            ("saving", "Image Saving"),
            ("config", "Image configuration"),
            ("exit", "Exit"),
        ]

        ret = True
        choice = "config"
        default = 3

        while ret != False and choice != "exit":
            self.verify_active_detector()
            dlg1 = UserMsg(label=f"Active detector: {self.parameters.active_detector}")
            dlg2 = UserChoice(values=value_list, defval=default)
            ret = BlissDialog([[dlg1], [dlg2]], title="Detector Setup").show()

            # returns False on cancel
            if ret != False:
                choice = ret[dlg2]

                if choice == "select":
                    self.select_detector()
                if choice == "mode":
                    self.mode_setup()
                if choice == "saving":
                    lima_saving_parameters_dialog(self.detector)
                if choice == "config":
                    lima_image_dialog(self.detector)

                for i in range(0, len(value_list)):
                    if choice == value_list[i][0]:
                        default = i
                        break

    def mode_setup(self):
        """
        User dialog allowing to select acquisition mode (SINGLE or ACCUMULATION) 
        and set maximum exposure time per subframe in accumulation.
        """
        acc_mode = self.get_acq_mode() == "ACCUMULATION"
        dlg_acc_mode = UserChoice(
            values=[("SINGLE", "SINGLE"), ("ACCUMULATION", "ACCUMULATION")],
            defval=acc_mode,
        )

        v = Validator(self.valid_max_acc_time)
        dlg_acc_max_expo_time = UserInput(
            label="Maximum time per frame [s]?",
            defval=self.detector.accumulation.max_expo_time,
            validator=v,
        )

        ret = BlissDialog(
            [[dlg_acc_mode], [dlg_acc_max_expo_time]],
            title=f"{self.parameters.active_detector}: Acquisition Mode",
        ).show()
        if ret != False:
            acc_mode = ret[dlg_acc_mode]
            if acc_mode == "ACCUMULATION":
                self.set_accumulation()
                self.detector.acquisition.mode = "ACCUMULATION"
                self.detector.accumulation.max_expo_time = float(
                    ret[dlg_acc_max_expo_time]
                )
            else:
                self.unset_accumulation()
                self.detector.acquisition.mode = "SINGLE"
                self.detector.accumulation.max_expo_time = (
                    self.detector.acquisition.expo_time
                )

    def valid_max_acc_time(self, str_input):
        """
        Checks maximum exposure time per subframe in accumulation for dimax is < 0.04.
        Displays an error otherwise.
        """
        if (
            "pco" in self.detector.camera_type.lower()
            and "dimax" in self.detector.camera.cam_name.lower()
        ):
            if float(str_input) > 0.04:
                raise ValueError("Maximum time per frame must be < 0.04")

    def set_accumulation(self):
        """
        Activates default chain for accumulation mode
        """
        setup_globals.DEFAULT_CHAIN.set_settings(
            self.def_chain_acc_mode["chain_config"]
        )

        det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(self.detector)
        if det_def_chain is None:
            setup_globals.DEFAULT_CHAIN.set_settings(
                self.def_chain_single_mode["chain_config"]
            )
            raise Exception(
                f"{self.detector.name} is not configured in default chain for accumulation"
            )

    def unset_accumulation(self):
        """
        Deactivates default chain for accumulation mode
        """
        setup_globals.DEFAULT_CHAIN.set_settings(
            self.def_chain_single_mode["chain_config"]
        )

        det_def_chain = setup_globals.DEFAULT_CHAIN._settings.get(self.detector)
        if det_def_chain is None:
            raise Exception(f"{self.detector.name} is not configured in default chain")

    def save_ccd_config(self, config_name, parameters=None):
        """
        Saves a set of detector parameters  
        """

        default_parameters = [
            "saving_format",
            "saving_overwrite_policy",
            "saving_frame_per_file",
            "saving_index_format",
            "saving_directory",
            "saving_prefix",
            "saving_next_number",
            "saving_suffix",
            "saving_mode",
            "acq_trigger_mode",
            "acq_expo_time",
            "acq_nb_frames",
            "latency_time",
            "acc_max_expo_time",
        ]

        # Clean-up formerly saved parameters
        if config_name in self.saved_configurations:
            del self.saved_configurations[config_name]

        # Apply the default parameters if no list is given
        if parameters == None:
            parameters = default_parameters

        # Read all parameters from the Lima server
        attributes = self.det_proxy.read_attributes(parameters)

        # store all parameters in a dictionary
        saved_state = {}
        param_len = len(parameters)
        for i in range(0, param_len):
            saved_state[parameters[i]] = attributes[i].value

        # store in the configurations dictionary
        self.saved_configurations[config_name] = saved_state

    def restore_ccd_config(self, config_name):
        """
        Restores the saved detector parameters 
        """

        saved_state = self.saved_configurations[config_name]
        # saving directory must be a valid path
        # is empty when lima server is restarted
        # does not exist anymore if directory has been deleted
        if (
            saved_state.get("saving_directory") == ""
            or saved_state.get("saving_directory") is not None
            and not os.path.exists(saved_state.get("saving_directory"))
        ):
            del saved_state["saving_directory"]
        # Write all saved parameters to the Lima server
        self.det_proxy.write_attributes(list(saved_state.items()))

    def has_shutter(self):
        """
        Returns if detector has shutter capability or not
        """
        return self.__has_shutter

    def use_shutter(self):
        """
        Returns if detector controls shutter or not.
        No shutter control in case of frelon camera type with frame transfer mode.
        Otherwise shutter control if detector has shutter capability.
        """
        if self.__is_frelon:
            image_mode = self.detector.camera.image_mode
            if image_mode == "FRAME TRANSFER":
                return False
        return self.__has_shutter

    def automatic_shutter_mode(self, automatic):
        """
        Sets the camera shutter to manual or automatic operation
        """
        if self.has_shutter():
            if automatic == True:
                mode = "AUTO_FRAME"
            else:
                mode = "MANUAL"
            self.det_proxy.shutter_mode = mode
            log_debug(self, f"set shutter_mode to {mode}")
        else:
            raise ValueError(
                "No automatic shutter mode available for the detector %s"
                % self.detector.name
            )

    def set_shutter_time(self, shuttime):
        """
        Sends the shutter time to Lima
        """
        if self.has_shutter():
            if not self.__is_frelon:
                self.det_proxy.shutter_open_time = shuttime

            self.det_proxy.shutter_close_time = shuttime
            log_debug(self, f"set shutter_open/close_time to {shuttime} sec")

    def get_shutter_time(self):
        """
        Reads the current shutter time from Lima
        """
        if self.has_shutter():
            return det_proxy.shutter_close_time
        else:
            return 0.0

    def calibrate_ccd_readout_time(self, expo_time, image_change=True):
        """
        Calculates the readout time of a camera
        """
        print("Calibrate readout time for %s" % self.detector.name)

        config_name = "calibration"
        readout_time = self.parameters.readout_time

        self.set_acq_mode(self.detector.acquisition.mode)
        self.det_proxy.acc_max_expo_time = self.detector.accumulation.max_expo_time
        self.det_proxy.image_bin = np.array(self.detector.image.binning)
        self.det_proxy.image_flip = np.array(self.detector.image.flip)
        self.det_proxy.image_rotation = (
            str(self.detector.image.rotation)
            if self.detector.image.rotation != 0
            else "none"
        )
        self.det_proxy.image_roi = np.array(self.detector.image.roi)

        # save the detector state
        self.save_ccd_config(config_name)

        # disable shutter and saving
        self.det_proxy.saving_mode = "MANUAL"

        ccd_type = self.det_proxy.camera_type
        # PCO detectors
        if ccd_type == "Pco":

            if image_change:
                nimg = 2
                self.take_image(expo_time, nimg)

                runtime = self.detector.camera.coc_run_time

                if self.det_proxy.acq_mode == "ACCUMULATION":
                    readout_time = runtime - self.detector.accumulation.expo_time
                    self.detector.acquisition.expo_time = expo_time
                else:
                    readout_time = runtime - expo_time

                if (
                    self.detector.accumulation.nb_frames > 1
                    and self.detector.accumulation.expo_time < 0.012
                ):
                    self.parameters.extra_time = 200e-6
                else:
                    self.parameters.extra_time = 100e-6

                print(f"Pco readout time = {readout_time} sec")
        # Basler cameras
        elif ccd_type == "Basler":

            if image_change:
                nimg = 10

                runtime = self.take_image(expo_time, nimg)
                if self.det_proxy.acq_mode == "ACCUMULATION":
                    readout_time = (
                        runtime / nimg
                    ) - self.detector.accumulation.expo_time
                else:
                    readout_time = (runtime / nimg) - expo_time

                print(f"Balser run time = {runtime} sec")
                print(f"Balser readout time = {readout_time} sec")

        # Frelon detectors
        elif ccd_type == "Frelon":
            if image_change:
                self.det_proxy.prepareAcq()
            if self.detector.camera.image_mode == "FRAME TRANSFER":
                readout_time = self.detector.camera.transfer_time
                min_expo_time = self.detector.camera.readout_time
                if expo_time < min_expo_time:
                    raise RuntimeError(
                        "CALIBRATION ERROR : in Frelon FTM mode, we must have exposure time > readout time\n"
                        f"NOW : exposure time [{expo_time:.3f} sec] < readout time [{min_expo_time:.3f} sec]\n"
                    )
            else:
                readout_time = self.detector.camera.readout_time
            print(f"Frelon readout time = {readout_time} sec")
        else:
            raise ValueError(
                "No readout time calibration method defined for the detector %s"
                % self.detector.name
            )

        # restore the detector state
        self.restore_ccd_config(config_name)

        # enable shutter back

        return readout_time

    def image_scan(
        self,
        expotime,
        nbframes,
        title,
        lima_acq_params={},
        lima_ctrl_params={},
        scan_info={},
        sinogram=False,
        save=True,
        run=False,
    ):
        """
        Creates and runs (if run attribute is True) acquisition of nbframes 
        images with an exposure time of expotime seconds
        """
        lima_acq = {
            "acq_nb_frames": nbframes,
            "acq_expo_time": expotime,
            "acq_mode": "SINGLE",
            "acq_trigger_mode": "INTERNAL_TRIGGER",
            "prepare_once": True,
            "start_once": False,
        }
        lima_ctrl = {}

        # merge all acquisition and controller related parameters
        lima_acq.update(lima_acq_params)
        lima_ctrl.update(lima_ctrl_params)

        # build the acquisition chain
        chain = AcquisitionChain()

        counters = [self.detector.image]

        if sinogram:
            for roi_name in self.detector.roi_profiles.keys():
                roi = self.detector.roi_profiles.get(roi_name)
                if (
                    roi.x + roi.width > self.detector.image.width
                    or roi.y + roi.height > self.detector.image.height
                ):
                    log_warning(
                        self,
                        f"roi profile {roi_name} is going to be removed because it does not fit into image roi",
                    )
                    self.detector.roi_profiles.remove(roi_name)
            if len(self.detector.roi_profiles.get_rois()) == 0:
                self.detector.roi_profiles["sino"] = [
                    0,
                    self.detector.image.height / 2,
                    self.detector.image.width,
                    1,
                ]

            counters = [self.detector]

        builder = ChainBuilder(counters)

        limadevs = list()
        for node in builder.get_nodes_by_controller_type(Lima):
            limadevs.append(node.controller)

            node.set_parameters(acq_params=lima_acq, ctrl_params=lima_ctrl)

            if sinogram:
                lima_children_params = {"count_time": expotime, "npoints": nbframes}

                for child_node in node.children:
                    child_node.set_parameters(acq_params=lima_children_params)
                    chain.add(node, child_node)
            else:
                chain.add(node)

        scan_info.update({"type": "image_scan"})

        scan = Scan(
            chain,
            scan_info=scan_info,
            name=title,
            save=save,
            data_watch_callback=LimaTakeDisplay(*limadevs),
        )

        print(chain._tree)
        if run == True:
            scan.run()

        return scan

    def image_header(self, header):
        """
        Sets saving common header of detector proxy with header attribute (dictionary)
        """
        header_list = list()
        for key, value in header.items():
            temp = key + "=" + value
            header_list.append(temp)

        self.det_proxy.saving_common_header = header_list

    def reset_image_header(self):
        """
        Resets saving common header of detector proxy 
        """
        self.det_proxy.resetCommonHeader()

    def take_image(self, expo_time, nimg=1):
        """
        Takes one or several images
        """

        # check the ACCUMULATION mode
        if self.det_proxy.acq_mode == "ACCUMULATION":
            if self.det_proxy.acc_nb_frames > 1:
                expo_time = self.det_proxy.acc_expo_time

        # prepare
        self.det_proxy.acq_trigger_mode = "INTERNAL_TRIGGER"
        self.det_proxy.acq_expo_time = expo_time
        self.det_proxy.acq_nb_frames = nimg
        self.det_proxy.prepareAcq()

        # start image acquisition
        runtime = time.time()
        self.det_proxy.startAcq()

        while self.det_proxy.acq_status == "Running":
            time.sleep(0.02)

        runtime = time.time() - runtime

        return runtime

    def set_acq_mode(self, mode):
        """
        Switches the camera acquisition mode
        """
        config_name = "image"
        image_parameters = ["image_bin", "image_flip", "image_roi"]

        # save the detector image state
        self.save_ccd_config(config_name, image_parameters)

        self.det_proxy.acq_mode = mode

        # restore the detector image state
        self.restore_ccd_config(config_name)

    def get_acq_mode(self):
        """
        Gets the camera acquisition mode
        """
        return self.det_proxy.acq_mode

    def get_image_parameters(self):
        """
        Return a dictonary with the current image parameters
        """
        ccd_attributes = ["user_detector_name", "camera_pixelsize", "image_sizes"]

        read_res = self.det_proxy.read_attributes(ccd_attributes)

        image_par = dict()
        image_par["name"] = read_res[0].value
        # Not found in Bliss parameters
        image_par["pixel_size"] = read_res[1].value.tolist()
        image_par["size"] = [self.detector.image.width, self.detector.image.height]
        # Need to read from Lima, because accumulation mode is applied with proxy!
        image_par["depth"] = read_res[2].value[1].tolist()
        # Binning not yet added to parameters in Bliss
        image_par["binning"] = self.detector.image.binning
        image_par["flipping"] = self.detector.image.flip
        image_par["roi"] = self.detector.image.roi

        return image_par

    def field_of_view(self, optic):
        """
        Calculate the detector field of view in mm
        """
        pixel_size = optic.image_pixel_size
        print(f"Current pixel size: {pixel_size:.4f} um")

        ccd_xsize = self.det_proxy.image_sizes[2]
        return pixel_size / 1000 * ccd_xsize

    def set_image_flipping(self, flipping):
        """
        Set the horizontal and vertical image flipping
        """
        self.detector.image.flip = flipping

    def get_image_flipping(self):
        """
        Get the horizontal and vertical image flipping
        """
        return self.detector.image.flip

    def set_max_pixel_rate(self):
        """
        Set pixel rate to maximum
        """
        self.detector.camera.pixel_rate = self.detector.camera.pixel_rate_valid_values[
            -1
        ]

    def lima_saving_parameters(self, npoints):
        """
        Returns a dictionary containing number of frames per file if saving mode is one file per scan
        """
        SavingMode = self.detector.saving.SavingMode

        lima_params = {}
        if self.detector.saving.mode == SavingMode.ONE_FILE_PER_SCAN:
            lima_params["saving_frame_per_file"] = npoints

        return lima_params

    def estimate_transfer_rate(self, expo_time):

        if not os.path.exists(current_session.scan_saving.get_path()):
            os.makedirs(current_session.scan_saving.get_path())
        depth = self.detector.image.depth
        roix = self.detector.image.roi[0]
        roiy = self.detector.image.roi[1]
        width = self.detector.image.width
        height = self.detector.image.height
        self.detector.proxy.image_roi = [roix, roiy, width, height]
        self.detector.proxy.saving_prefix = "transfer"
        self.detector.proxy.acq_nb_frames = 50
        self.detector.proxy.acq_expo_time = expo_time
        self.detector.proxy.acq_trigger_mode = "INTERNAL_TRIGGER"
        self.detector.proxy.saving_mode = "AUTO_FRAME"
        self.detector.proxy.saving_frame_per_file = self.detector.saving.frames_per_file
        self.detector.proxy.saving_directory = setup_globals.SCAN_SAVING.get_path()
        self.detector.prepareAcq()
        self.detector.startAcq()
        while self.detector.proxy.acq_status == "Running":
            print("lima acquisition is running")
            time.sleep(0.5)
        self.detector.proxy.abortAcq()
        files = glob.glob(
            self.detector.proxy.saving_directory
            + "/"
            + self.detector.proxy.saving_prefix
            + "*"
            + self.detector.proxy.saving_suffix
        )
        for f in files:
            os.unlink(f)
