import sys
import gevent
import numpy as np
import time

import PyTango
import bliss

from bliss import setup_globals,global_map
from bliss.common.logtools import log_info,log_debug
from bliss.common import session

from bliss.scanning.acquisition.motor import LinearStepTriggerMaster, MotorMaster, SoftwarePositionTriggerMaster, SweepMotorMaster, MeshStepTriggerMaster
from bliss.scanning.acquisition.lima import LimaAcquisitionMaster
from bliss.scanning.acquisition.musst import MusstAcquisitionSlave
from bliss.scanning.chain import AcquisitionChain
from bliss.scanning.scan import Scan

import tomo
from tomo.TomoMusst import TriggerType, TomoMusst
from bliss.scanning.acquisition.timer import SoftwareTimerMaster



class TopoScan:
    """
    Class for topotomo scan object.
    The class implements methods to handle scans in topotomo acquisition.
    Acquisition chain is constructed with a first motor (top master) 
    that triggers a second motor (nested motor).
    The top master is a step-by-step motor that will stops at each 
    projection to trigger and wait the end of nested scan.
    Nested scan type is the same as fasttomo: step scan, continuous scan 
    synchronized by musst/soft, continuous scan without gap.
    Contrary to fasttomo, multiple reference groups are not possible.
    Reference images can only be taken at each projection angle before 
    each scan.
    
    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    nested_axis : Bliss axis object
        motor used for scanning
    start_pos : float
        nested motor start position
    end_pos : float
        nested motor end position
    nb_points : int
        number of images acquired during scanning
    topo_musst : Tomo musst object
        contains methods to handle musst in topotomo acquisition
    watchdog_trig_difference : int 
        difference tolerated between triggers sent and images acquired
    """
    def __init__(self, tomo, nested_axis, start_pos, end_pos, nb_points, topo_musst, *args, **kwargs):
        # init logging
        self.name = tomo.name+".toposcan"
        global_map.register(self, tag=self.name)
        log_info(self,"__init__() entering")
        
        self.tomo = tomo
        
        # scanning axis for topo
        self.nested_axis = nested_axis
        self.start_pos = start_pos
        self.end_pos = end_pos
        self.nb_points = nb_points
        
        # scanning musst configuration for topo
        self.topo_musst = topo_musst
        
        self.watchdog_trig_difference = 20
        
        log_info(self,"__init__() leaving")
    
    
    def step_scan(self, start_pos, end_pos, nb_points, exposure_time, 
                      title, scan_info={}, save=True, run=False):
        """
        Configures acquisition chain for nested step scan.
        MeshStepTriggerMaster moves nested axis and rotation axis step by step.
        At each rotation axis step, nested scan is triggered.
        One nested scan corresponds to a tomo step scan.
        """
        # add one more trigger to have the same positioning as for the continuous scans,
        # but one image more!
        self.nb_points = self.nb_points + 1
        
        chain = AcquisitionChain()
        
        motor_masters = MeshStepTriggerMaster(self.nested_axis,self.start_pos,self.end_pos,self.nb_points,
                                              self.tomo.rotation_axis,start_pos,end_pos,nb_points)
                                              
        timer_master = SoftwareTimerMaster(count_time=exposure_time, npoints=self.nb_points, sleep_time=0)
        chain.add(motor_masters, timer_master)
                                              
        # musst acquisition device behave like musst.ct(exposure_time) 
        musst_acq = self.topo_musst.prepare_soft(exposure_time, self.nb_points)
        chain.add(motor_masters,musst_acq)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(self.nb_points * nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = self.nb_points
        
        # prepare Lima
        acq_params = {'acq_nb_frames'       :   self.nb_points * nb_points,       # number of images wanted per topo scan
                      'acq_trigger_mode'    :   'EXTERNAL_TRIGGER_MULTI',
                      'acq_expo_time'       :   exposure_time,
                      'prepare_once'        :   True,
                      'start_once'          :   True,
                      'wait_frame_id'       :   range(self.nb_points * nb_points)
        }
        
        if self.tomo.tomo_ccd.detector.acquisition.mode == 'ACCUMULATION':
            acq_params.update({'acq_mode'           :   'ACCUMULATION'})
        else:
            if self.tomo.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo.tomo_ccd.detector.camera.cam_name.lower():
                acq_params.update({'acq_mode'           :   'ACCUMULATION',
                                   'acc_max_expo_time'  :   0.04})
        
        # prepare LIMA
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        chain.add(musst_acq,lima_acq)
        
        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_acq, chain, self.tomo.tomo_ccd.detector.name, exposure_time, 
                                            self.nb_points*nb_points, timer=timer_master)
        
        # create the scan object
        step_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=f'axis:{self.nested_axis.name}',
                                                              motors= self.tomo.scan_motors,
                                                              limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.tomo.tomo_musst.card,
                                                             trigger_name=f'axis:{self.tomo.rotation_axis.name}',
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference),     
        )
                                                                
        print (step_scan.acq_chain._tree)
        if run == True:
            step_scan.run()
            
        return step_scan
    
    
    def continuous_scan(self, start_pos, end_pos, nb_points, exposure_time, latency_time, scan_step_size, scan_point_time, 
                        trigger_type, title, scan_info={}, save=True, run=False):
        """
        Configures acquisition chain for nested continuous scan synchronized by musst.
        LinearStepTriggerMaster moves rotation axis step by step.
        At each rotation axis step, nested scan is triggered.
        One nested scan corresponds to a tomo continuous scan synchronized by musst.
        """
        
        chain = AcquisitionChain()
        
        # Step scan master
        step_motor_master = LinearStepTriggerMaster(nb_points, self.tomo.rotation_axis, start_pos, end_pos)
        
        acc_margin = self.acc_margin
        
        if self.use_step_size:
            # undershoot start margin is  added to be sure motor will reach scan speed   
            acc_margin = abs(scan_step_size)

        # nested motor goes from start position to end position in one move and emits soft triggers regularly in time
        scan_motor_master = MotorMaster(self.nested_axis, self.start_pos, self.end_pos, time=scan_point_time*self.nb_points, 
                                        undershoot_start_margin = acc_margin, undershoot_end_margin = acc_margin)
        chain.add(step_motor_master, scan_motor_master)
        
        scan_time = self.tomo.active_tomo.topo_tools.continuous_scan_time(self.tomo.rotation_axis, start_pos, end_pos)
        
        # prepare MUSST
        accumulation = self.tomo.tomo_ccd.detector.accumulation.nb_frames
        musst_master, musst_slave = self.topo_musst.prepare_continuous(self.start_pos, exposure_time, latency_time, self.nb_points, 
                                                                            scan_step_size, scan_point_time, scan_time, accumulation, trigger_type)
                                                                       
        chain.add(scan_motor_master, musst_master)
        chain.add(musst_master, musst_slave)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(self.nb_points * nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = self.nb_points
        
        acq_params = {'acq_nb_frames'       :   self.nb_points * nb_points,
                      'acq_trigger_mode'    :   'EXTERNAL_TRIGGER_MULTI',
                      'acq_expo_time'       :   exposure_time,
                      'prepare_once'        :   True,
                      'start_once'          :   True,
                      'wait_frame_id'       :   [(i*self.nb_points -1) for i in range(1,nb_points+1)]
        }

        if self.tomo.tomo_ccd.detector.acquisition.mode == 'ACCUMULATION':
            acq_params.update({'acq_mode'           :   'ACCUMULATION'})
        else:
            if self.tomo.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo.tomo_ccd.detector.camera.cam_name.lower():
                acq_params.update({'acq_mode'           :   'ACCUMULATION',
                                   'acc_max_expo_time'  :   0.04})
  
        # prepare LIMA
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        
        
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        chain.add(musst_master,lima_acq)
        
        # calculation devices are used to convert musst data (encoder + timer) in appropriate units (degree and seconds)
        calc_musst = self.topo_musst.prepare_calculation(musst_slave,data_per_line=int(self.nb_points))
        calc_musst_timer = calc_musst[0]
        calc_musst_pos = calc_musst[1]
        for calc in calc_musst:
            chain.add(musst_master,calc)

        scan_motor_master.add_external_channel(calc_musst_pos, calc_musst_pos.channels[0].short_name,
                                          rename=self.nested_axis.name,
                                          dtype=np.float)

        
        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_master, chain, self.tomo.tomo_ccd.detector.name, exposure_time, 
                                            (self.nb_points * nb_points))
        
        # create the scan object
        cont_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=calc_musst_timer.channels[0].name,
                                                              motors= self.tomo.scan_motors,
                                                              limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.topo_musst.card,
                                                             trigger_name=calc_musst_timer.channels[0].name,
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference)
        )
                                                                
        print (cont_scan.acq_chain._tree)
        if run == True:
            cont_scan.run()
            
        return cont_scan
        
    
    def soft_scan(self, start_pos, end_pos, nb_points, exposure_time, scan_step_size, scan_point_time, 
                      title, scan_info={}, save=True, run=False):
        """
        Configures acquisition chain for nested continuous scan synchronized by soft.
        LinearStepTriggerMaster moves rotation axis step by step.
        At each rotation axis step, nested scan is triggered.
        One nested scan corresponds to a tomo continuous scan synchronized by soft.
        """
        
        chain = AcquisitionChain()

        # Step scan master
        step_motor_master = LinearStepTriggerMaster(nb_points, self.tomo.rotation_axis, start_pos, end_pos)
        
        acc_margin = self.acc_margin
        
        if self.use_step_size:
            # undershoot start margin is  added to be sure motor will reach scan speed   
            acc_margin = abs(scan_step_size)
        
        # nested motor goes from start position to end position in one move and emits soft triggers regularly in time
        scan_motor_master = SoftwarePositionTriggerMaster(self.nested_axis, self.start_pos, self.end_pos, self.nb_points, 
                                                          time=scan_point_time*self.nb_points,
                                                          undershoot_start_margin = acc_margin, undershoot_end_margin = acc_margin)
        
        chain.add(step_motor_master, scan_motor_master)
        
        scan_timer_master = SoftwareTimerMaster(count_time=0, npoints=self.nb_points, sleep_time=0,
                                                name="fast_timer")
        chain.add(scan_motor_master, scan_timer_master)
        
        scan_time = self.tomo.active_tomo.topo_tools.continuous_scan_time(self.tomo.rotation_axis, start_pos, end_pos)
        
        # prepare MUSST
        # musst acquisition device behave like musst.ct(exposure_time) 
        musst_acq = self.topo_musst.prepare_soft(exposure_time,self.nb_points, scan_time)
        chain.add(scan_motor_master, musst_acq)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(self.nb_points * nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = self.nb_points
        
        acq_params = {'acq_nb_frames'       :   self.nb_points * nb_points,
                      'acq_trigger_mode'    :   'EXTERNAL_TRIGGER_MULTI',
                      'acq_expo_time'       :   exposure_time,
                      'prepare_once'        :   True,
                      'start_once'          :   True,
                      # list of frame IDs for the last frame of every continuous scan (MotorMaster)
                      # last frame ID for every line!
                      'wait_frame_id'       :   [(i*self.nb_points -1) for i in range(1,nb_points+1)]
        }

        if self.tomo.tomo_ccd.detector.acquisition.mode == 'ACCUMULATION':
            acq_params.update({'acq_mode'           :   'ACCUMULATION'})
        else:
            if self.tomo.tomo_ccd.detector.camera_type.lower() == 'pco' and 'dimax' in self.tomo.tomo_ccd.detector.camera.cam_name.lower():
                acq_params.update({'acq_mode'           :   'ACCUMULATION',
                                   'acc_max_expo_time'  :   0.04})
  
        # prepare LIMA
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        chain.add(musst_acq,lima_acq)
    
        
        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_acq, chain, self.tomo.tomo_ccd.detector.name, exposure_time, 
                                            (self.nb_points *nb_points))
        
        # create the scan object
        soft_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=f'axis:{self.nested_axis.name}',
                                                                motors= self.tomo.scan_motors,
                                                                limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.tomo.tomo_musst.card,
                                                             trigger_name=f'axis:{self.tomo.rotation_axis.name}',
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference),
        )
                                                                
        print (soft_scan.acq_chain._tree)
        if run == True:
            soft_scan.run()
            
        return soft_scan
        

    def sweep_scan(self, start_pos, end_pos, nb_points, exposure_time, scan_step_size, scan_point_time, 
                   title, scan_info={}, save=True, run=False):
        """
        Configures acquisition chain for nested continuous scan without gap.
        LinearStepTriggerMaster moves rotation axis step by step.
        At each rotation axis step, nested scan is triggered.
        One nested scan corresponds to a tomo continuous scan without gap (sweep scan).
        """
        
        chain = AcquisitionChain()
        
        # Step scan master
        step_motor_master = LinearStepTriggerMaster(nb_points, self.tomo.rotation_axis, start_pos, end_pos)
        
        acc_margin = self.acc_margin
        
        if self.use_step_size:
            # undershoot start margin is  added to be sure motor will reach scan speed   
            acc_margin = abs(scan_step_size)
        
        npoints = self.nb_points
        time = exposure_time
        data_per_point = 2
        
        # npoints corresponds to number of sweep movements required for each reference group
        # time regulates motor speed, corresponds to time needed by the motor to do one sweep move 
        scan_motor_master = SweepMotorMaster(self.nested_axis, self.start_pos, self.end_pos, npoints = npoints, 
                                             time = time,
                                             undershoot_start_margin = acc_margin, undershoot_end_margin = acc_margin)
        chain.add(step_motor_master, scan_motor_master)
        
        # get full calculate undershoot
        undershoot = scan_motor_master._undershoot
        
        scan_time = self.tomo.active_tomo.topo_tools.sweep_scan_time(self.tomo.rotation_axis, start_pos, end_pos, npoints)
        
        # prepare MUSST
        # acquisition device equivalent to musst.ct(exposure_time)
        musst_master, musst_slave = self.topo_musst.prepare_sweep(self.start_pos, self.nb_points, 
                                                                  scan_step_size, undershoot,
                                                                  scan_time)
        chain.add(scan_motor_master, musst_master)
        chain.add(musst_master, musst_slave)
        
        # prepare Lima saving
        ctrl_params={}
        ctrl_params.update(self.tomo.tomo_ccd.lima_saving_parameters(self.nb_points * nb_points))
        
        self.tomo.tomo_ccd.detector.proxy.saving_statistics_history = self.nb_points
        
        acq_params = {'acq_nb_frames'       :   self.nb_points*nb_points,
                      'acq_trigger_mode'    :   'EXTERNAL_GATE',
                      'acq_expo_time'       :   exposure_time,
                      'prepare_once'        :   True,
                      'start_once'          :   True,
                      # list of frame IDs for the last frame of every continuous scan (MotorMaster)
                      # last frame ID for every line!
                      'wait_frame_id'       :   [(i*self.nb_points -1) for i in range(1,nb_points+1)]
        }

        if self.tomo.tomo_ccd.detector.acquisition.mode == 'ACCUMULATION':
            acq_params.update({'acq_mode'           :   'ACCUMULATION'})
        
        # prepare Lima
        lima_acq = LimaAcquisitionMaster(self.tomo.tomo_ccd.detector, 
                                         ctrl_params = ctrl_params,
                                         **acq_params
        )
        lima_acq.add_counter(self.tomo.tomo_ccd.detector.image)
        chain.add(musst_master,lima_acq)
        
        # calculation devices are used to convert musst data (encoder + timer) in appropriate units (degree and seconds)
        calc_musst = self.topo_musst.prepare_calculation(musst_slave,data_per_point=data_per_point)
        calc_musst_timer = calc_musst[0]
        calc_musst_pos = calc_musst[1]
        for calc in calc_musst:
            chain.add(musst_master,calc)

        scan_motor_master.add_external_channel(calc_musst_pos, calc_musst_pos.channels[0].short_name,
                                          rename=self.nested_axis.name,
                                          dtype=np.float)
        
        # add slow counters from the active measurement group
        if self.tomo.counters is not None:
            self.tomo.counters.add_counters(musst_master, chain, self.tomo.tomo_ccd.detector.name, exposure_time, 
                                            (self.nb_points*nb_points))
        
        # create the scan object
        sweep_scan = Scan(chain,
                         scan_info = scan_info,
                         name      = title,
                         save      = save,
                         data_watch_callback=tomo.ScanDisplay(trigger_name=calc_musst_timer.channels[0].name,
                                                              motors= self.tomo.scan_motors,
                                                              limas= (self.tomo.tomo_ccd.detector,)),
                         watchdog_callback=tomo.ScanWatchdog(musst_card= self.topo_musst.card,
                                                             trigger_name=calc_musst_timer.channels[0].name,
                                                             limas= (self.tomo.tomo_ccd.detector,),
                                                             trig_difference= self.watchdog_trig_difference),
        )
                                                                
        print (sweep_scan.acq_chain._tree)
        if run == True:
            sweep_scan.run()
            
        return sweep_scan
        
        
