import sys
import gevent
import numpy as np
import time
import ast
import datetime
import h5py

import PyTango
import bliss

from bliss import setup_globals, global_map
from bliss.common import session
from bliss.common.logtools import log_info, log_debug, log_warning
from bliss.common.standard import *
from bliss.common.cleanup import cleanup, axis as cleanup_axis
from bliss.shell.standard import umv, umvr
from bliss.scanning.group import Sequence
from bliss.controllers.machinfo import WaitForRefillPreset

from tomo.TomoScan import TomoScan
from tomo.TopoScan import TopoScan
from tomo.Tomo import Tomo, ScanType
from tomo.TomoParameters import TomoParameters
from tomo.Presets import (
    DarkShutterPreset,
    FastShutterPreset,
    CommonHeaderPreset,
    DefaultChainFastShutterPreset,
)
from tomo.TomoMusst import TomoMusst
from tomo.ScanDisplay import ScanDisplay
from tomo.TomoTools import TopoTools

from tomo.pcotomo.PcoTomoScan import PcoTomoScan
from tomo.pcotomo.Presets import PcoTomoShutterPreset
from tomo.pcotomo.PcoTomoTools import PcoTomoTools
from tomo.pcotomo.PcoTomoMusst import PcoTomoMusst


"""
    Module to handle fasttomo acquisition and variants
    
    There is one class per sequence
    
    Sequences implemented: 
            - fasttomo          (class FastTomo)
            - halftomo          (class HalfTomo)
            - z_series          (class ZSeries)
            - topotomo          (class TopoTomo)
            - mtomo             (class Mtomo)
            - progressivetomo   (class Progressive)
    
    FastTomo object is in charge of scan preparation: calculates parameters and constructs acquisition chain
    Except topotomo, variants derive from fasttomo, they are considered as an additional layer and then reuse Fasttomo functionalities
    
"""


class FastTomo:
    """
    Class to handle fasttomo acquisition
    
    Use TomoScan object to create all existing types of scan: step, continuous synchronized by musst/soft, sweep
    
    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    """

    def __init__(self, tomo, *args, **kwargs):
        # init logging
        self.name = tomo.tomo_name + ".fasttomo"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")
        self.tomo = tomo
        tomo.tomo_scan = TomoScan(tomo)
        log_info(self, "__init__() leaving")

    def scan_info(self):
        pass

    def calculate_parameters(self):
        """
        Calculates fasttomo sequence parameters:
        - nb_groups : Number of reference groups in the acquisition
        - start_group_pos : List of start positions of each reference group
        - nb_groups_points : Number of projections inside each reference group
        - sample_pixel_size : Pixel size of the sample according to the optic in micrometers
        """

        log_info(self, "calculate_parameters() entering")

        if (
            not self.tomo.parameters.reference_groups
            or self.tomo.parameters.ref_on == 0
        ):
            nb_groups = 1
            nb_points = [self.tomo.parameters.tomo_n]
        else:
            if self.tomo.parameters.tomo_n < self.tomo.parameters.ref_on:
                self.tomo.parameters.ref_on = self.tomo.parameters.tomo_n
            nb_groups = int(self.tomo.parameters.tomo_n / self.tomo.parameters.ref_on)
            # list of number of triggers needed for each reference group
            nb_points = [self.tomo.parameters.ref_on] * nb_groups

        # look if number of images per reference group stays unchanged
        if self.tomo.parameters.reference_groups and self.tomo.parameters.ref_on != 0:
            if self.tomo.parameters.tomo_n % self.tomo.parameters.ref_on != 0:
                print(
                    "{} total images with references every {} projections"
                    " will create a group with less projections!".format(
                        self.tomo.parameters.tomo_n, self.tomo.parameters.ref_on
                    )
                )
                nb_groups = nb_groups + 1

        self.tomo.in_pars["nb_groups"] = nb_groups

        # list of motor start position for each reference group
        self.tomo.in_pars["start_group_pos"] = list(
            np.linspace(
                self.tomo.parameters.start_pos,
                self.tomo.parameters.end_pos,
                nb_groups + 1,
            )[:-1]
        )

        if self.tomo.parameters.reference_groups and self.tomo.parameters.ref_on != 0:
            if self.tomo.parameters.tomo_n % self.tomo.parameters.ref_on != 0:
                last_group_proj = (
                    self.tomo.parameters.tomo_n % self.tomo.parameters.ref_on
                )
                print("last group number of projections: %.3f" % last_group_proj)
                nb_points.append(last_group_proj)
                step_size = (
                    self.tomo.parameters.end_pos - self.tomo.parameters.start_pos
                ) / self.tomo.parameters.tomo_n
                self.tomo.in_pars["start_group_pos"] = list(
                    np.linspace(
                        self.tomo.parameters.start_pos,
                        self.tomo.parameters.end_pos - last_group_proj * step_size,
                        nb_groups,
                    )
                )
        
        self.tomo.in_pars["nb_group_points"] = nb_points

        self.tomo.in_pars["end_pos"] = self.tomo.parameters.end_pos

        self.tomo.in_pars["sample_pixel_size"] = self.tomo.optic.image_pixel_size

        log_info(self, "calculate_parameters() leaving")

    def estimate_scan_time(self, motor, start_pos, end_pos, nb_points):
        """
        Estimates scan duration according to tomo scan type.
        Total scan duration is composed of:
        - reference scans duration
        - dark scans duration
        - projection scans duration
        - return scan duration
        """
        tomo_tools = self.tomo.tomo_tools

        ref_scan_time = tomo_tools.estimate_ref_scan_duration()

        dark_scan_time = tomo_tools.estimate_dark_scan_duration()

        if self.tomo.parameters.scan_type == ScanType.STEP:
            proj_scan_time = tomo_tools.step_scan_time(motor, start_pos, nb_points)
        if self.tomo.parameters.scan_type in [
            ScanType.CONTINUOUS_HARD,
            ScanType.CONTINUOUS_SOFT,
        ]:
            proj_scan_time = tomo_tools.continuous_scan_time(motor, start_pos, end_pos)
        if self.tomo.parameters.scan_type == ScanType.SWEEP:
            proj_scan_time = tomo_tools.sweep_scan_time(
                motor, start_pos, end_pos, nb_points
            )

        return_scan_time = tomo_tools.estimate_return_scan_duration(motor)

        return ref_scan_time + dark_scan_time + proj_scan_time + return_scan_time

    def prepare(self):
        """
        Calculates parameters and contructs acquisition chain corresponding to scan type.
        """
        self.tomo.tomo_tools.check_params(self.tomo.tomo_musst)

        self.calculate_parameters()

        self.tomo.tomo_scan.calculate_parameters(
            self.tomo.parameters.start_pos,
            self.tomo.parameters.end_pos,
            self.tomo.parameters.tomo_n,
            self.tomo.parameters.exposure_time,
            self.tomo.parameters.latency_time,
        )

        est_time_scan = self.estimate_scan_time(
            self.tomo.tomo_musst.motor,
            self.tomo.parameters.start_pos,
            self.tomo.parameters.end_pos,
            self.tomo.parameters.tomo_n,
        )

        self.tomo.in_pars["estimated_time_scan"] = str(
            datetime.timedelta(seconds=est_time_scan)
        )

    def projection_scan(
        self,
        start_pos,
        end_pos,
        nb_points,
        title,
        scan_sequence=None,
        header={},
        run=True,
    ):
        """
        Fasttomo scan for one reference group.
        """
        log_info(self, "projection_scan() entering")

        # add image identification to the common image header
        header["image_key"] = "0"  # projection NXtomo definition

        # add meta data
        meta_data = self.tomo.meta_data.tomo_scan_info()
        scan_info = {}
        scan_info["scan"] = meta_data["technique"]["scan"]
        scan_info["detector"] = meta_data["technique"]["detector"]
        scan_info = {"type": "tomo", "technique": scan_info}

        restore_list = (cleanup_axis.POS, cleanup_axis.VEL)

        # do not return to initial position when no error is detected
        if self.tomo.parameters.return_to_start_pos == False:
            restore_list = list()

        with error_cleanup(self.tomo.rotation_axis, restore_list=restore_list):

            if self.tomo.parameters.return_images or (
                self.tomo.parameters.reference_groups
                and end_pos != self.tomo.parameters.end_pos
            ):
                restore_list = (cleanup_axis.VEL,)

            with cleanup(
                self.tomo.rotation_axis,
                self.tomo.tomo_ccd.detector,
                restore_list=restore_list,
            ):

                proj_scan = None
                if self.tomo.parameters.scan_type == ScanType.STEP:
                    proj_scan = self.tomo.tomo_scan.step_scan(
                        start_pos,
                        end_pos,
                        nb_points,
                        self.tomo.parameters.exposure_time,
                        title,
                        scan_info=scan_info,
                        save=True,
                        run=False,
                    )

                if self.tomo.parameters.scan_type == ScanType.CONTINUOUS_HARD:
                    proj_scan = self.tomo.tomo_scan.continuous_scan(
                        start_pos,
                        end_pos,
                        nb_points,
                        self.tomo.parameters.exposure_time,
                        self.tomo.parameters.latency_time,
                        self.tomo.tomo_scan.in_pars["scan_step_size"],
                        self.tomo.tomo_scan.in_pars["scan_point_time"],
                        self.tomo.parameters.trigger_type,
                        title,
                        scan_info=scan_info,
                        save=True,
                        run=False,
                    )

                if self.tomo.parameters.scan_type == ScanType.CONTINUOUS_SOFT:
                    proj_scan = self.tomo.tomo_scan.soft_scan(
                        start_pos,
                        end_pos,
                        nb_points,
                        self.tomo.parameters.exposure_time,
                        self.tomo.tomo_scan.in_pars["scan_step_size"],
                        self.tomo.tomo_scan.in_pars["scan_point_time"],
                        title,
                        scan_info=scan_info,
                        save=True,
                        run=False,
                    )

                if self.tomo.parameters.scan_type == ScanType.SWEEP:
                    proj_scan = self.tomo.tomo_scan.sweep_scan(
                        start_pos,
                        end_pos,
                        nb_points,
                        self.tomo.parameters.exposure_time,
                        self.tomo.tomo_scan.in_pars["scan_step_size"],
                        self.tomo.tomo_scan.in_pars["scan_point_time"],
                        title,
                        scan_info=scan_info,
                        save=True,
                        run=False,
                    )

                # add common header preset
                header_preset = CommonHeaderPreset(self.tomo.tomo_ccd, header)
                proj_scan.add_preset(header_preset)

                # add shutter preset
                shutter_preset = FastShutterPreset(self.tomo.shutter)
                proj_scan.add_preset(shutter_preset)

                if setup_globals.machinfo.check == True:
                    if self.tomo.parameters.scan_type == ScanType.STEP:
                        proj_scan_time = self.tomo.tomo_tools.step_scan_time(
                            self.tomo.rotation_axis, start_pos, nb_points
                        )
                    if self.tomo.parameters.scan_type in [
                        ScanType.CONTINUOUS_HARD,
                        ScanType.CONTINUOUS_SOFT,
                    ]:
                        proj_scan_time = self.tomo.tomo_tools.continuous_scan_time(
                            self.tomo.rotation_axis, start_pos, end_pos
                        )
                    if self.tomo.parameters.scan_type == ScanType.SWEEP:
                        proj_scan_time = self.tomo.tomo_tools.sweep_scan_time(
                            self.tomo.rotation_axis, start_pos, end_pos, nb_points
                        )
                    refill_preset = WaitForRefillPreset(
                        setup_globals.machinfo, checktime=proj_scan_time
                    )
                    proj_scan.acq_chain.add_preset(refill_preset)

                # add to scan sequence when requested
                if scan_sequence != None:
                    scan_sequence.add(proj_scan)
                    self.tomo.list_proj_scans.append(proj_scan)

                if self.tomo.parameters.sinogram_active:
                    self.gscan = gevent.spawn(proj_scan.run)
                    while proj_scan.node is None:
                        gevent.sleep(0.1)
                    self.greemit = self.tomo.tomo_sino.create_data_reemitter(
                        proj_scan.node.db_name, scan_sequence.sequence
                    )
                    self.greemit.start()
                    try:
                        self.gscan.get()
                    except:
                        # stops scan and raises exception
                        self.gscan.kill()
                        self.greemit.stop()
                        raise
                    finally:
                        self.greemit.get()

                else:
                    # run the proj scan
                    proj_scan.run()

        log_info(self, "projection_scan() leaving")

    def scan_cleanup(self):
        """
        Restores all parameters that have been modified by the acquisition.
        """
        self.tomo.tomo_musst.set_timer_back()

    def run(self):
        """
        If tomo sinogram option is active, configures devices (flint plot and redis data reemission) 
        needed to build sinogram during fasttomo acquisition and saves final image of sinogram at the end 
        of acquisition.
        Runs fasttomo sequence.
        """
        with cleanup(self.scan_cleanup):
            scan_info = self.tomo.meta_data.tomo_scan_info()

            if self.tomo.parameters.sinogram_active:

                if (
                    not self.tomo.parameters.dark_images_at_start
                    or not self.tomo.parameters.ref_images_at_start
                ):
                    raise Exception(
                        "Error, cannot build sinogram for this sequence. Dark scan or/and reference scan at start is/are missing."
                    )
                npixels = self.tomo.tomo_ccd.detector.image.width
                self.tomo.tomo_sino.configure_plot(
                    scan_info,
                    0,
                    npixels - 1,
                    npixels,
                    self.tomo.parameters.start_pos,
                    self.tomo.parameters.end_pos,
                    self.tomo.parameters.tomo_n,
                )

            seq = Sequence(title=self.tomo.sequence, scan_info=scan_info)

            if self.tomo.parameters.sinogram_active:
                npixels = self.tomo.tomo_ccd.detector.image.width
                self.tomo.tomo_sino.configure_data_reemission(
                    seq,
                    self.tomo.rotation_axis,
                    self.tomo.tomo_ccd.detector.roi_profiles.counters,
                    npixels,
                )

            with seq.sequence_context() as scan_seq:
                self.tomo.run_sequence(scan_seq)

            if self.tomo.parameters.sinogram_active:
                self.tomo.tomo_sino.save(self.tomo, seq.scan.scan_number)


class HalfTomo(FastTomo, TomoParameters):
    """
    Class to handle halftomo acquisition
    
    Sequence similar to fasttomo except that sample does not fit the field-of-view of the camera but at least half of it can be seen
    
    A lateral alignment axis is used to shift sample before acquisition
    
    Additional parameters are then needed to perform tomo
    
    Parameters
    ----------
    full_frame_position : float
        Axis position before shifting sample
    acquisition_position : float
        Axis position after shifting sample
    shift_in_fov_factor : float
        lateral axis shift expressed in fov factor 
    shift_in_mm : float
        lateral axis shift expressed in mm
    shift_in_pixels : int
        lateral axis shift expressed in pixels
    return_to_full_frame_position : boolean
        flag to activate/deactivate return of lateral axis at full frame position at the end of acquisition
    
    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    lat_align_axis : Axis
        shifts sample before acquisition
    setup_done : boolean
        flag used to check if half acquisition has been configured by setup
    started_once : boolean
        flag used to detect if half acquisition has already been run
        
    """

    def __init__(self, tomo, *args, **kwargs):
        # init logging
        log_info(self, "__init__() entering")
        FastTomo.__init__(self, tomo)

        self.name = tomo.tomo_name + ".halftomo"
        global_map.register(self, tag=self.name)

        # get lateral alignment motor
        config = tomo.config["halftomo"]
        self.lat_align_axis = config["lat_align_axis"]
        print("Lateral align axis position: %f" % self.lat_align_axis.position)

        # Define the necessary set of persistant parameters.
        # Initialize the parameter set name and the necessary default values

        param_name = tomo.tomo_name + ":halftomo_parameters"
        half_defaults = {}
        half_defaults["full_frame_position"] = 0.0
        half_defaults["acquisition_position"] = 0.0
        half_defaults["shift_in_fov_factor"] = 0
        half_defaults["shift_in_mm"] = 0
        half_defaults["shift_in_pixels"] = 0
        half_defaults["return_to_full_frame_position"] = False

        # Initialise the TomoParameters class
        TomoParameters.__init__(self, param_name, half_defaults)

        self.setup_done = False
        self.started_once = False

        log_info(self, "__init__() leaving")

    def prepare(self):
        """
        Records full-acquisition axis position, determines half-acquisition axis position with user shift value, 
        determines number of images needed for reconstruction.
        Uses fasttomo sequence prepare to calculate scan parameters and construct acquisition chain.
        """

        self.tomo.parameters.end_pos = self.tomo.parameters.start_pos + 360

        pixel_size = self.tomo.optic.image_pixel_size / 1000
        ccd_cols = self.tomo.tomo_ccd.get_image_parameters()["size"][0]
        col_bin = self.tomo.tomo_ccd.get_image_parameters()["binning"][0]

        print(f"Current pixel size: {pixel_size*1000:.2f} um")
        print(f"Current horz. image pixels: {ccd_cols}")
        print(f"Current horz. image size: {pixel_size*ccd_cols:.2f} mm")

        if not self.started_once:
            print(
                f"Storing current {self.lat_align_axis.name} pos. {self.lat_align_axis.position} as full-frame reference\n"
            )
            print(f"Previous value was {self.parameters.full_frame_position}\n")
            self.parameters.full_frame_position = self.lat_align_axis.position
        else:
            print(
                f"Stored {self.lat_align_axis.name} full-frame pos. reference: {self.parameters.full_frame_position}\n"
            )

        print(
            f"{self.lat_align_axis.name} shift: {self.parameters.shift_in_mm:.3f} mm - {self.parameters.shift_in_pixels:.1f} pixels - {self.parameters.shift_in_fov_factor:.3f} FOV\n"
        )

        self.parameters.acquisition_position = (
            self.parameters.full_frame_position + self.parameters.shift_in_mm
        )

        fov_factor = (self.parameters.shift_in_fov_factor + 0.5) * 2
        slice_size = int(fov_factor * ccd_cols)
        print(
            f"Your reconstructed slice size will be approx. {slice_size}x{slice_size}\n"
        )

        # add lateral axis to scan display
        if self.lat_align_axis not in self.tomo.scan_motors:
            self.tomo.scan_motors.append(self.lat_align_axis)

        print("")
        # FastTomo.prepare()
        super().prepare()

        if self.lat_align_axis.position != self.parameters.acquisition_position:
            umv(self.lat_align_axis, self.parameters.acquisition_position)
        else:
            print(
                f"Motor {self.lat_align_axis.name} already in half acquisition pos.\n"
            )

        if self.lat_align_axis.position == self.parameters.full_frame_position:
            print("Warning: half acquisition pos is equal to full frame pos.\n")

    def run(self):
        """
        Runs halftomo sequence.
        """
        with cleanup(self.scan_cleanup):
            scan_info = self.tomo.meta_data.tomo_scan_info()

            if self.tomo.parameters.sinogram_active:

                if (
                    not self.tomo.parameters.dark_images_at_start
                    or not self.tomo.parameters.ref_images_at_start
                ):
                    raise Exception(
                        "Error, cannot build sinogram for this sequence. Dark scan or/and reference scan at start is/are missing."
                    )
                npixels = self.tomo.tomo_ccd.detector.image.width
                self.tomo.tomo_sino.configure_plot(
                    scan_info,
                    0,
                    npixels - 1,
                    npixels,
                    self.tomo.parameters.start_pos,
                    self.tomo.parameters.end_pos,
                    self.tomo.parameters.tomo_n,
                )

            seq = Sequence(title=self.tomo.sequence, scan_info=scan_info)

            if self.tomo.parameters.sinogram_active:
                npixels = self.tomo.tomo_ccd.detector.image.width
                self.tomo.tomo_sino.configure_data_reemission(
                    seq,
                    self.tomo.rotation_axis,
                    self.tomo.tomo_ccd.detector.roi_profiles.counters,
                    npixels,
                )

            with seq.sequence_context() as scan_seq:
                self.tomo.run_sequence(scan_seq)

            if self.tomo.parameters.sinogram_active:
                self.tomo.tomo_sino.save(self.tomo, seq.scan.scan_number)

    def scan_cleanup(self):
        """
        Restores all parameters that have been modified by the acquisition.
        Restores previous musst timer clock value.
        If return_to_full_frame_position option is active, moves lateral axis to recorded full frame position.
        """
        self.tomo.tomo_musst.set_timer_back()
        if self.parameters.return_to_full_frame_position:
            umv(self.lat_align_axis, self.parameters.full_frame_position)
        self.tomo.scan_motors.remove(self.lat_align_axis)

    def configure_sample_size(self, ccd_cols, pixel_size):
        """
        Gets lateral axis shift value defined by the user and deduces other shift type values from it.
        The shift values are set to 0 each time half acquisition setup is launched so the active shift 
        differs from 0.
        """
        if self.parameters.shift_in_mm != 0:
            self.parameters.shift_in_pixels = self.parameters.shift_in_mm / pixel_size
            self.parameters.shift_in_fov_factor = (
                self.parameters.shift_in_pixels / ccd_cols
            )
            return True
        elif self.parameters.shift_in_pixels != 0:
            self.parameters.shift_in_fov_factor = (
                self.parameters.shift_in_pixels / ccd_cols
            )
            self.parameters.shift_in_mm = self.parameters.shift_in_pixels * pixel_size
            return True
        elif self.parameters.shift_in_fov_factor != 0:
            self.parameters.shift_in_pixels = (
                self.parameters.shift_in_fov_factor * ccd_cols
            )
            self.parameters.shift_in_mm = self.parameters.shift_in_pixels * pixel_size
            return True


class ZSeries(FastTomo):
    """
    Class to handle z series tomo acquisition
    
    Z axis moves from one specified step between half or full turn scans

    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    start_pos : float
        z axis start position 
    delta_pos : float
        z axis step value
    nb_scans : int
        number or half/full turn scans to perform
    sleep : float
        sleep time between each scan
    start_nb : int
        number of first scan, used to know from which position to start
    """

    def __init__(self, tomo, *args, **kwargs):
        # init logging
        self.name = tomo.tomo_name + ".zseries"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")

        self.tomo = tomo

        # get Z motor
        config = tomo.config["zseries"]
        self.z_axis = config["z_axis"]
        print("z_axis = %f" % self.z_axis.position)

        self.start_pos = 0.0
        self.delta_pos = 0.0
        self.nb_scans = 0.0
        self.sleep = 0.0
        self.start_nb = 1

        log_info(self, "__init__() leaving")

    def prepare(self, delta_pos, nb_scans, start_pos, start_nb, sleep):
        """
        Sets z_series tomo parameters.
        Uses fasttomo prepare to calculate scan parameters and construct acquisition chain.
        """
        log_info(self, "prepare() entering")

        if nb_scans < 2:
            raise ValueError("The minimum number of scans is 2!")
        self.nb_scans = nb_scans

        if start_nb < 1 or start_nb > nb_scans:
            raise ValueError("Invalid start scan value!")
        self.start_nb = start_nb

        self.delta_pos = delta_pos
        self.sleep = sleep

        # get the start position
        self.original_pos = self.z_axis.position
        if start_pos != None:
            self.start_pos = start_pos
        else:
            self.start_pos = self.z_axis.position

        # go to end position to have first scan at sample bottom
        if delta_pos <= 0:
            self.start_pos = self.start_pos + (self.nb_scans - 1) * delta_pos
            self.delta_pos = -self.delta_pos

        # Calculate start position and number of scans when not starting from the first one
        if start_nb > 1:
            self.start_pos = self.start_pos + (start_nb - 1) * delta_pos
            self.nb_scans = self.nb_scans - (start_nb - 1)

        if self.start_pos + (self.nb_scans - 1) * delta_pos < self.z_axis.low_limit:
            raise ValueError(
                f"{self.z_axis.name} start position is beyond axis low limit ({self.z_axis.low_limit})! \nPlease change one of those parameters: start_pos, nb_scans or delta_pos"
            )
        if self.start_pos + (self.nb_scans - 1) * delta_pos > self.z_axis.high_limit:
            raise ValueError(
                f"{self.z_axis.name} end position is beyond axis high limit ({self.z_axis.high_limit})! \nPlease change one of those parameters: start_pos, nb_scans or delta_pos"
            )

        # add z axis to scan display
        self.tomo.scan_motors.append(self.z_axis)

        log_debug(self, "Prepare tomo scan")
        self.tomo.prepare()

        log_info(self, "prepare() leaving")

    def run(self):
        """
        Runs z_series tomo acquisition.
        """
        log_info(self, "run() entering")

        with error_cleanup(self.z_axis, restore_list=(cleanup_axis.POS,)):

            with cleanup(self.scan_cleanup):

                # move Z motor to start position
                umv(self.z_axis, self.start_pos)

                # sleep when requested
                time.sleep(self.sleep)

                scan_info = self.tomo.meta_data.tomo_scan_info()
                scan_info["technique"]["scan"]["nb_scans"] = self.nb_scans
                scan_info["technique"]["scan"]["start_nb"] = self.start_nb

                nb_scan_per_series = self.tomo.in_pars["nb_groups"] * 2 - 1
                if self.tomo.parameters.ref_images_at_start:
                    nb_scan_per_series += 1
                if self.tomo.parameters.dark_images_at_start:
                    nb_scan_per_series += 1
                if self.tomo.parameters.ref_images_at_end:
                    nb_scan_per_series += 1
                if self.tomo.parameters.dark_images_at_end:
                    nb_scan_per_series += 1
                if self.tomo.parameters.return_images:
                    nb_scan_per_series += 1
                ScanDisplay.NB_SCANS = self.nb_scans * nb_scan_per_series

                if self.tomo.parameters.ref_images_at_start:
                    ScanDisplay.NB_SCANS -= 1
                if self.tomo.parameters.dark_images_at_start:
                    ScanDisplay.NB_SCANS -= 1

                ScanDisplay.SCAN_NB = 0
                dataset_name = setup_globals.SCAN_SAVING.dataset_name
                setup_globals.newdataset(dataset_name + "_stage" + str(0).zfill(2))
                # prepare scan sequence
                seq = Sequence(
                    title=self.tomo.sequence + "_stage" + str(0).zfill(2),
                    scan_info=scan_info,
                )
                with seq.sequence_context() as scan_seq:
                    ScanDisplay.SCAN_NB += 1
                    self.tomo.run_sequence(scan_seq)

                for i in range(1, self.nb_scans):

                    # move Z motor
                    print("%d move Z motor\n" % i)
                    umvr(self.z_axis, self.delta_pos)

                    # sleep when requested
                    time.sleep(self.sleep)

                    setup_globals.newdataset(dataset_name + "_stage" + str(i).zfill(2))
                    # prepare scan sequence
                    seq = Sequence(
                        title=self.tomo.sequence + "_stage" + str(i).zfill(2),
                        scan_info=scan_info,
                    )
                    with seq.sequence_context() as scan_seq:
                        ScanDisplay.SCAN_NB += 1
                        self.tomo.run_sequence(scan_seq)

                # move back to start position
                umv(self.z_axis, self.original_pos)

        log_info(self, "run() leaving")

    def scan_cleanup(self):
        """
        Restores all parameters that have been modified by the zseries acquisition.
        """
        self.sequence = "tomo:basic"
        self.tomo.scan_motors.remove(self.z_axis)
        self.tomo.tomo_musst.set_timer_back()
        setup_globals.newdataset(
            "_".join(setup_globals.SCAN_SAVING.dataset_name.split("_")[:-1])
        )


class TopoTomo(FastTomo):
    """
    Class to handle topotomo acquisition
    
    Compare to fasttomo, this sequence uses a step motor to trigger at each projection angle a nested motor that will be scanned
    
    Because this sequence does not handle reference groups as fasttomo, a different object is used to construct acquisition chain: TopoScan
    
    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    nested_axis : Axis object for topo scans
        scanning motor
    topo_start_pos : float
        nested motor start position
    topo_end_pos : float
        nested motor end position
    topo_nb_points : int
        number of images to acquire
    topo_musst : Tomo musst object
        contains methods to configure musst program
    topo_tools : Tomo tools object
        contains methods to estime scan time
    """

    def __init__(self, tomo, topo_start_pos, topo_end_pos, topo_nb_points):

        # init logging
        log_info(self, "__init__() entering")

        self.name = tomo.tomo_name + ".topotomo"
        global_map.register(self, tag=self.name)

        self.tomo = tomo
        self.topo_start_pos = topo_start_pos
        self.topo_end_pos = topo_end_pos
        self.topo_nb_points = topo_nb_points

        # get nested motor
        config = tomo.config["topotomo"]
        self.nested_axis = config["nested_axis"]
        print("nested_axis = %f" % self.nested_axis.position)

        # scanning musst configuration for topo
        self.topo_musst = TomoMusst(
            self.tomo.tomo_name, self.tomo.musst_card, self.nested_axis
        )
        print("Musst channel used: %s" % self.topo_musst.mot_chan)

        self.topo_tools = TopoTools(self.tomo)

        tomo.topo_scan = None

        log_info(self, "__init__() leaving")

    def estimate_scan_time(
        self, topo_motor, topo_start_pos, topo_end_pos, topo_nb_points
    ):
        """
        Estimates scan duration according to topo scan type.
        Total scan duration is composed of:
        - reference scans duration
        - dark scans duration
        - nested scans duration
        - rotation displacement time
        - return scan duration
        """
        tomo_tools = self.tomo.tomo_tools

        ref_scan_time = tomo_tools.estimate_ref_scan_duration()

        dark_scan_time = tomo_tools.estimate_dark_scan_duration()

        if self.tomo.parameters.scan_type == ScanType.STEP:
            nested_scan_time = self.topo_tools.step_scan_time(
                topo_motor, topo_start_pos, topo_nb_points
            )
        if self.tomo.parameters.scan_type in [
            ScanType.CONTINUOUS_HARD,
            ScanType.CONTINUOUS_SOFT,
        ]:
            nested_scan_time = self.topo_tools.continuous_scan_time(
                topo_motor, topo_start_pos, topo_end_pos
            )
        if self.tomo.parameters.scan_type == ScanType.SWEEP:
            nested_scan_time = self.topo_tools.sweep_scan_time(
                topo_motor, topo_start_pos, topo_end_pos, topo_nb_points
            )

        nested_scan_time *= self.tomo.parameters.tomo_n

        undershoot = self.tomo.tomo_scan.in_pars["scan_speed"] ** 2 / (
            2 * topo_motor.acceleration
        )
        acc_margin = self.tomo.tomo_scan.acc_margin
        if self.tomo.tomo_scan.use_step_size:
            acc_margin = abs(self.tomo.tomo_scan.in_pars["scan_step_size"])
        back_disp = (topo_end_pos + undershoot + acc_margin) - (
            topo_start_pos + undershoot + acc_margin
        )
        back_to_start_time = self.tomo.tomo_tools.mot_disp_time(
            topo_motor, back_disp, topo_motor.velocity
        )
        back_to_start_time *= self.tomo.parameters.tomo_n

        nested_scan_time += back_to_start_time

        step_disp = (
            self.tomo.parameters.end_pos - self.tomo.parameters.start_pos
        ) / self.tomo.parameters.tomo_n
        one_step_time = tomo_tools.mot_disp_time(
            self.tomo.rotation_axis, step_disp, self.tomo.rotation_axis.velocity
        )
        step_scan_time = one_step_time * (self.tomo.parameters.tomo_n - 1)

        return_scan_time = tomo_tools.estimate_return_scan_duration(
            self.tomo.rotation_axis
        )

        return (
            ref_scan_time
            + dark_scan_time
            + nested_scan_time
            + step_scan_time
            + return_scan_time
        )

    def prepare(self):
        """
        Calculates parameters and contructs acquisition chain corresponding to scan type.
        """
        super().calculate_parameters()

        self.tomo.tomo_scan.calculate_parameters(
            self.topo_start_pos,
            self.topo_end_pos,
            self.topo_nb_points,
            self.tomo.parameters.exposure_time,
            self.tomo.parameters.latency_time,
        )

        self.tomo.tomo_tools.check_params(self.topo_musst)

        est_time_scan = self.estimate_scan_time(
            self.topo_musst.motor,
            self.topo_start_pos,
            self.topo_end_pos,
            self.topo_nb_points,
        )
        self.tomo.in_pars["estimated_time_scan"] = str(
            datetime.timedelta(seconds=est_time_scan)
        )

        self.tomo.topo_scan = TopoScan(
            self.tomo,
            self.nested_axis,
            self.topo_start_pos,
            self.topo_end_pos,
            self.topo_nb_points,
            self.topo_musst,
        )
        self.tomo.topo_scan.acc_margin = self.tomo.tomo_scan.acc_margin
        self.tomo.topo_scan.use_step_size = self.tomo.tomo_scan.use_step_size

        if (
            self.tomo.parameters.scan_type == ScanType.CONTINUOUS_HARD
            or self.tomo.parameters.scan_type == ScanType.SWEEP
        ):
            self.topo_musst.sync_motor()

        # add nested axis to scan display
        self.tomo.scan_motors.append(self.nested_axis)

    def projection_scan(
        self, start_pos, end_pos, nb_points, title, scan_sequence=None, header={}
    ):
        """
        Topo tomo scan for one reference group.
        """
        log_info(self, "projection_scan() entering")

        # add image identification to the common image header
        header["image_key"] = "0"  # projection NXtomo definition

        # add meta data
        meta_data = self.tomo.meta_data.tomo_scan_info()
        scan_info = {}
        scan_info["scan"] = meta_data["technique"]["scan"]
        scan_info["detector"] = meta_data["technique"]["detector"]
        scan_info = {"type": "tomo", "technique": scan_info}

        restore_list = (cleanup_axis.POS, cleanup_axis.VEL)

        # do not return to initial position when no error is detected
        if self.tomo.parameters.return_to_start_pos == False:
            restore_list = list()

        with cleanup(
            self.nested_axis,
            self.tomo.rotation_axis,
            self.tomo.tomo_ccd.detector,
            restore_list=restore_list,
        ):

            proj_scan = None
            if self.tomo.parameters.scan_type == ScanType.STEP:
                proj_scan = self.tomo.topo_scan.step_scan(
                    start_pos,
                    end_pos,
                    nb_points,
                    self.tomo.parameters.exposure_time,
                    title,
                    scan_info=scan_info,
                    save=True,
                    run=False,
                )

            if self.tomo.parameters.scan_type == ScanType.CONTINUOUS_HARD:
                proj_scan = self.tomo.topo_scan.continuous_scan(
                    start_pos,
                    end_pos,
                    nb_points,
                    self.tomo.parameters.exposure_time,
                    self.tomo.parameters.latency_time,
                    self.tomo.tomo_scan.in_pars["scan_step_size"],
                    self.tomo.tomo_scan.in_pars["scan_point_time"],
                    self.tomo.parameters.trigger_type,
                    title,
                    scan_info=scan_info,
                    save=True,
                    run=False,
                )

            if self.tomo.parameters.scan_type == ScanType.CONTINUOUS_SOFT:
                proj_scan = self.tomo.topo_scan.soft_scan(
                    start_pos,
                    end_pos,
                    nb_points,
                    self.tomo.parameters.exposure_time,
                    self.tomo.tomo_scan.in_pars["scan_step_size"],
                    self.tomo.tomo_scan.in_pars["scan_point_time"],
                    title,
                    scan_info=scan_info,
                    save=True,
                    run=False,
                )

            if self.tomo.parameters.scan_type == ScanType.SWEEP:
                proj_scan = self.tomo.topo_scan.sweep_scan(
                    start_pos,
                    end_pos,
                    nb_points,
                    self.tomo.parameters.exposure_time,
                    self.tomo.tomo_scan.in_pars["scan_step_size"],
                    self.tomo.tomo_scan.in_pars["scan_point_time"],
                    title,
                    scan_info=scan_info,
                    save=True,
                    run=False,
                )

            # add common header preset
            header_preset = CommonHeaderPreset(self.tomo.tomo_ccd, header)
            proj_scan.add_preset(header_preset)

            # add shutter preset
            shutter_preset = FastShutterPreset(self.tomo.shutter)
            proj_scan.add_preset(shutter_preset)

            # add to scan sequence when requested
            if scan_sequence != None:
                scan_sequence.add(proj_scan)
                self.tomo.list_proj_scans.append(proj_scan)

            # run the proj scan
            proj_scan.run()

        log_info(self, "projection_scan() leaving")

    def scan_cleanup(self):
        """
        Restores all parameters that have been modified by the topotomo acquisition.
        """
        self.tomo.scan_motors.remove(self.nested_axis)
        self.topo_musst.set_timer_back()

    def run(self):
        """
        Runs topotomo acquisition sequence.
        """
        log_info(self, "run() Topo tomo entering")

        with cleanup(
            self.scan_cleanup, self.nested_axis, restore_list=(cleanup_axis.POS,)
        ):
            seq = Sequence(
                title=self.tomo.sequence, scan_info=self.tomo.meta_data.tomo_scan_info()
            )
            with seq.sequence_context() as scan_seq:
                self.tomo.run_sequence(scan_seq)

        log_info(self, "run() Topo tomo leaving")


class Mtomo(FastTomo):
    """
    Class to handle multiple tomo acquisition
    
    Rotation axis is stopped and restarted at each turn 
    
    Dark images are recorded on the 1st tomo only
    
    Reference images are recorded before the 1st tomo and after the last one
    
    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    nb_turns : int
        number of full turns 
    start_turn : int
        number of turns from which to start
    init_start_pos : float
        start position of the 1st turn
    start_pos : float
        start position calculated from initial start position and start turn
    end_pos : float
        scan end position
    dark_images_at_start : boolean
        option to take dark images before scan
    dark_images_at_end : boolean
        option to take dark images after scan
    ref_images_at_start : boolean
        option to take reference images before scan
    ref_images_at_end : boolean
        option to take reference images after scan
    return_images : boolean
        option to take static images on scan return
    return_to_start_pos : boolean
        option to go to start position at the end of scan
    """

    def __init__(self, tomo, *args, **kwargs):
        # init logging
        self.name = tomo.tomo_name + ".mtomo"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")

        self.tomo = tomo

        self.nb_turns = 0
        self.start_turn = 1
        self.init_start_pos = 0.0

        self.start_pos = 0
        self.end_pos = 0

        self.dark_images_at_start = True
        self.ref_images_at_start = True
        self.dark_images_at_end = False
        self.ref_images_at_end = False
        self.return_images = False
        self.return_to_start_pos = True

        log_info(self, "__init__() leaving")

    def prepare(self, nb_turns, start_pos, end_pos, start_turn):
        """
        Sets mtomo parameters.
        Uses fasttomo prepare to calculate scan parameters and construct acquisition chain.
        """

        log_info(self, "prepare() entering")

        if nb_turns < 2:
            raise ValueError("The minimum number of turns is 2!")
        self.nb_turns = nb_turns

        if start_turn < 1 or start_turn > nb_turns:
            raise ValueError("Invalid start turn value!")
        self.start_turn = start_turn

        # The initial scan start position
        self.init_start_pos = self.tomo.parameters.start_pos
        if start_pos != None:
            self.init_start_pos = start_pos

        # Save all parameters that will be modified for the mtomo scan
        self.start_pos = self.tomo.parameters.start_pos
        self.end_pos = end_pos
        self.dark_images_at_start = self.tomo.parameters.dark_images_at_start
        self.ref_images_at_start = self.tomo.parameters.ref_images_at_start
        self.dark_images_at_end = self.tomo.parameters.dark_images_at_end
        self.ref_images_at_end = self.tomo.parameters.ref_images_at_end
        self.return_images = self.tomo.parameters.return_images
        self.return_to_start_pos = self.tomo.parameters.return_to_start_pos

        self.tomo.parameters.return_to_start_pos = False
        self.tomo.parameters.dark_images_at_end = False
        self.tomo.parameters.ref_images_at_end = False
        # Configure the scan

        self.tomo.parameters.start_pos = self.init_start_pos + (
            (self.start_turn - 1) * (self.end_pos - self.start_pos)
        )
        self.tomo.parameters.end_pos = self.tomo.parameters.start_pos + (
            self.end_pos - self.start_pos
        )

        log_debug(self, "Prepare tomo scan")
        self.tomo.prepare()

        log_info(self, "prepare() leaving")

    def run(self):
        """
        Runs mtomo acquisition.
        """
        log_info(self, "run() entering")

        with cleanup(self.scan_cleanup):

            # prepare scan sequence
            seq = Sequence(
                title=self.tomo.sequence, scan_info=self.tomo.meta_data.tomo_scan_info()
            )
            with seq.sequence_context() as scan_seq:

                for turn_idx in range((self.start_turn - 1), self.nb_turns):
                    if turn_idx > 0:
                        self.tomo.parameters.dark_images_at_start = False
                        self.tomo.parameters.ref_images_at_start = False

                        self.tomo.parameters.start_pos = self.init_start_pos + (
                            turn_idx * (self.end_pos - self.start_pos)
                        )
                        self.tomo.parameters.end_pos = (
                            self.tomo.parameters.start_pos
                            + (self.end_pos - self.start_pos)
                        )

                    if turn_idx == (self.nb_turns - 1):
                        self.tomo.parameters.dark_images_at_end = (
                            self.dark_images_at_end
                        )
                        self.tomo.parameters.ref_images_at_end = self.ref_images_at_end
                        self.tomo.parameters.return_images = self.return_images

                    self.calculate_parameters()
                    print("%d turn" % (turn_idx + 1))
                    self.tomo.run_sequence(scan_seq)

            log_info(self, "run() leaving")

    def scan_cleanup(self):
        """
        Restores all parameters that have been modified by the mtomo acquisition.
        """
        self.tomo.parameters.start_pos = self.start_pos
        self.tomo.parameters.end_pos = self.end_pos
        self.tomo.parameters.dark_images_at_start = self.dark_images_at_start
        self.tomo.parameters.ref_images_at_start = self.ref_images_at_start
        self.tomo.parameters.dark_images_at_end = self.dark_images_at_end
        self.tomo.parameters.ref_images_at_end = self.ref_images_at_end
        self.tomo.parameters.return_images = self.return_images
        self.tomo.parameters.return_to_start_pos = self.return_to_start_pos
        self.tomo.tomo_musst.set_timer_back()
        pos = self.tomo.rotation_axis.position / (self.end_pos - self.start_pos)
        pos = (pos - int(pos)) * (self.end_pos - self.start_pos)
        self.tomo.rotation_axis.dial = self.tomo.rotation_axis.sign * pos
        self.tomo.rotation_axis.position = (
            self.tomo.rotation_axis.sign * self.tomo.rotation_axis.dial
        )


class Progressive(FastTomo):
    """
    Class to handle progressive tomo acquisition
    
    Rotation axis is moving continuously until the end of acquisition 
    
    1/nb_turns of the total images will be recorded at each turn
    
    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrTomo) 
        contains all info about tomo (hardware, parameters)
    nb_turns : int
        number of full turns 
    start_turn : int
        number of turns from which to start
    init_start_pos : float
        start position of the 1st turn
    start_pos : float
        start position calculated from initial start position and start turn
    end_pos : float
        scan end position
    tomo_n : int
        number of projections per turn
    dark_images_at_start : boolean
        option to take dark images before scan
    dark_images_at_end : boolean
        option to take dark images after scan
    ref_images_at_start : boolean
        option to take reference images before scan
    ref_images_at_end : boolean
        option to take reference images after scan
    return_images : boolean
        option to take static images on scan return
    return_to_start_pos : boolean
        option to go to start position at the end of scan
    """

    def __init__(self, tomo, *args, **kwargs):
        # init logging
        self.name = tomo.tomo_name + ".progressive"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")

        self.tomo = tomo

        self.nb_turns = 0.0
        self.start_turn = 1
        self.init_start_pos = 0.0

        self.start_pos = 0.0
        self.end_pos = 0.0
        self.tomo_n = 0

        self.dark_images_at_start = True
        self.ref_images_at_start = True
        self.dark_images_at_end = False
        self.ref_images_at_end = False
        self.return_images = False
        self.return_to_start_pos = True

        log_info(self, "__init__() leaving")

    def prepare(self, nb_turns, start_pos, start_turn):
        """
        Sets progressive tomo parameters.
        Uses fasttomo prepare to calculate scan parameters and construct acquisition chain.
        """
        log_info(self, "prepare() entering")

        if nb_turns < 2:
            raise ValueError("The minimum number of turns is 2!")
        self.nb_turns = nb_turns

        if start_turn < 1 or start_turn > nb_turns:
            raise ValueError("Invalid start turn value!")
        self.start_turn = start_turn

        # The initial scan start position
        self.init_start_pos = self.tomo.parameters.start_pos
        if start_pos != None:
            self.init_start_pos = start_pos

        # Save all parameters that will be modified for the progressive tomo scan
        self.start_pos = self.tomo.parameters.start_pos
        self.end_pos = self.tomo.parameters.end_pos
        self.tomo_n = self.tomo.parameters.tomo_n

        self.dark_images_at_start = self.tomo.parameters.dark_images_at_start
        self.ref_images_at_start = self.tomo.parameters.ref_images_at_start
        self.dark_images_at_end = self.tomo.parameters.dark_images_at_end
        self.ref_images_at_end = self.tomo.parameters.ref_images_at_end
        self.return_images = self.tomo.parameters.return_images
        self.return_to_start_pos = self.tomo.parameters.return_to_start_pos

        # get the correct start position when not starting from the first turn
        if self.start_turn == 1:
            self.tomo.parameters.start_pos = self.init_start_pos
        else:
            proj_factor = pow(2, self.start_turn - 2)
            tomo_n = self.tomo_n * proj_factor
            shift_angle = 360 / tomo_n / 2
            self.tomo.parameters.start_pos = (
                self.init_start_pos + shift_angle + ((self.start_turn - 1) * 360)
            )
            self.tomo.parameters.tomo_n = tomo_n

        self.tomo.parameters.end_pos = self.tomo.parameters.start_pos + 360
        self.tomo.parameters.return_to_start_pos = False

        log_debug(self, "Prepare tomo scan")
        self.tomo.prepare()

        log_info(self, "prepare() leaving")

    def run(self):
        """
        Runs progressive tomo acquisition.
        """
        log_info(self, "run() entering")

        with cleanup(self.scan_cleanup):

            # prepare scan sequence
            seq = Sequence(
                title=self.tomo.sequence, scan_info=self.tomo.meta_data.tomo_scan_info()
            )
            with seq.sequence_context() as scan_seq:

                for turn_idx in range((self.start_turn - 1), self.nb_turns):
                    if turn_idx == 0:
                        tomo_n = self.tomo_n
                        shift_angle = 0
                    else:
                        proj_factor = pow(2, turn_idx - 1)
                        tomo_n = self.tomo_n * proj_factor
                        shift_angle = 360 / tomo_n / 2

                        self.tomo.parameters.dark_images_at_start = False
                        self.tomo.parameters.ref_images_at_start = False

                    self.tomo.parameters.start_pos = (
                        self.init_start_pos + shift_angle + (turn_idx * 360)
                    )
                    self.tomo.parameters.end_pos = self.tomo.parameters.start_pos + 360
                    self.tomo.parameters.tomo_n = tomo_n

                    skip_end_imgages = turn_idx < (self.nb_turns - 1)
                    if skip_end_imgages:
                        self.tomo.parameters.dark_images_at_end = False
                        self.tomo.parameters.ref_images_at_end = False
                        self.tomo.parameters.return_images = True
                    else:
                        self.tomo.parameters.dark_images_at_end = (
                            self.dark_images_at_end
                        )
                        self.tomo.parameters.ref_images_at_end = self.ref_images_at_end
                        self.tomo.parameters.return_images = self.return_images

                    print("%d turn" % (turn_idx + 1))
                    self.tomo.run_sequence(scan_seq)

            log_info(self, "run() leaving")

    def scan_cleanup(self):
        """
        Restores all parameters that have been modified by the progressive tomo acquisition.
        """
        self.tomo.parameters.start_pos = self.start_pos
        self.tomo.parameters.end_pos = self.end_pos
        self.tomo.parameters.tomo_n = self.tomo_n

        self.tomo.parameters.dark_images_at_start = self.dark_images_at_start
        self.tomo.parameters.ref_images_at_start = self.ref_images_at_start
        self.tomo.parameters.dark_images_at_end = self.dark_images_at_end
        self.tomo.parameters.ref_images_at_end = self.ref_images_at_end
        self.tomo.parameters.return_images = self.return_images
        self.tomo.parameters.return_to_start_pos = self.return_to_start_pos
        self.tomo.tomo_musst.set_timer_back()


class PcoTomo(FastTomo, TomoParameters):
    """
    Class to handle pcotomo acquisition
    
    Use PcoTomoScan object to create scan

    **Attributes**
    name : str
        The Bliss object name
    tomo : Tomo object (ex: HrPcoTomo)
        contains all info about tomo (hardware, parameters)
    tomo_scan : TomoScan object
        contains acquisition chain corresponding to scan type
    tomo_musst : Tomo musst object
        contains methods to configure musst program
    tomo_tools : Tomo tools object
        contains methods to estimate scan time

    Parameters
    ----------
    mode : str
        type of acquisition (consecutive, multiple, endless)
    ntomo : int
        number of consecutive tomo
    nloop : int
        number of time ( ntomo + waiting turns ) will be executed
    nwait : int
        number of waiting turns
    noread : int
        activate or not image transfer between loop
    start_turn : int
        number of turns taken by the rotation to speed up
    user_output: int
        either the image indicator or the loop indicator used to alert user
    """

    def __init__(self, tomo, *args, **kwargs):

        # init logging
        self.name = tomo.tomo_name + ".pcotomo"
        global_map.register(self, tag=self.name)
        log_info(self, "__init__() entering")

        param_name = tomo.tomo_name + ":pcotomo_parameters"
        pcotomo_defaults = {}
        pcotomo_defaults["mode"] = "consecutive"
        pcotomo_defaults["ntomo"] = 1
        pcotomo_defaults["nloop"] = 1
        pcotomo_defaults["nwait"] = 1
        pcotomo_defaults["noread"] = 1
        pcotomo_defaults["start_turn"] = 1
        pcotomo_defaults["user_output"] = 0

        # Initialise the TomoParameters class
        TomoParameters.__init__(self, param_name, pcotomo_defaults)

        self.tomo = tomo
        tomo.tomo_scan = PcoTomoScan(tomo)
        self.tomo_tools = PcoTomoTools(tomo)
        self.tomo_musst = PcoTomoMusst(
            self.name,
            self.tomo.musst_card,
            self.tomo.rotation_axis,
            self.tomo.shutter.sync_shutter_time,
        )
        self.in_pars = {}
        print("Musst channel used: %s" % self.tomo_musst.mot_chan)
        log_info(self, "__init__() leaving")

    def scan_info(self):

        numberofscans = self.in_pars["numberofscans"]
        sizeofonescan = self.in_pars["sizeofonescan"]
        totalcontinuoustime = self.in_pars["totalcontinuoustime"]
        transferrate = self.in_pars["transferrate"]
        time = self.in_pars["time"]
        frequency = self.in_pars["frequency"]
        totaltime = self.in_pars["totaltime"]
        unlimitwaitingturns = self.in_pars["unlimitwaitingturns"]
        unlimitfrequency = self.in_pars["unlimitfrequency"]

        if self.parameters.mode == "consecutive":
            print("\n\nConsecutive Scans Without Waiting Turns")
            print(f"You can collect a maximum of {numberofscans} scans in the memory")
            print(
                f"One scan is {round(sizeofonescan * 1024 * 1024 * 1024, 2)} B or {round(sizeofonescan, 2)} GB"
            )
            print(
                f"It means a film of {totalcontinuoustime} s ({round(totalcontinuoustime / 60, 2)} min)\n"
            )
            print(
                f"Time to download film: {round((sizeofonescan * numberofscans) / (transferrate / 1024), 2)} s ({round((sizeofonescan * numberofscans) / (transferrate / 1024) / 60, 2)} min)\n"
            )

        elif self.parameters.mode == "multiple":
            print("\n\nWaiting Turns Between Each Scan")
            print(f"You will collect a total of {numberofscans} scans\n")
            print(f"You will wait {self.parameters.nwait} turns between each scan")
            print(
                f"One scan of {time} s every {frequency} s during {totaltime} s ({round(totaltime / 60, 2)} min) \n"
            )
            print(
                f"Time to download film: {round((sizeofonescan * numberofscans) / (transferrate / 1024), 2)} s ({round((sizeofonescan * numberofscans) / (transferrate / 1024) / 60, 2)} min)\n"
            )

        elif self.parameters.mode == "endless":
            print(
                f"\n\nYou will wait {unlimitwaitingturns} turns between each scan, during this time, scan is downloaded"
            )
            print(f"One scan of {time} s every {unlimitfrequency} s FOREVER \n")
            print(
                f"Time to download one scan: {round(sizeofonescan / (transferrate / 1024), 2)} s ({round(sizeofonescan / (transferrate / 1024) / 60, 2)} min) \n"
            )

    def calculate_parameters(self):

        super().calculate_parameters()

        self.tomo.in_pars["end_pos"] = self.tomo.parameters.start_pos + (self.tomo.parameters.end_pos - self.tomo.parameters.start_pos) * self.parameters.ntomo
        self.tomo.in_pars["nb_group_points"] = [nb_points*self.parameters.ntomo for nb_points in self.tomo.in_pars["nb_group_points"]]
        
        depth = self.tomo.tomo_ccd.detector.image.depth
        width = self.tomo.tomo_ccd.detector.image.width
        height = self.tomo.tomo_ccd.detector.image.height
        sizeofoneimage = depth * width * height / 1024 / 1024
        sizeofonescan = sizeofoneimage * self.tomo.parameters.tomo_n / 1024
        self.tomo.tomo_ccd.estimate_transfer_rate(self.tomo.parameters.exposure_time)
        numberofscans = int(
            self.tomo.tomo_ccd.detector.camera.max_nb_images
            / self.tomo.parameters.tomo_n
        )
        readout = self.tomo.tomo_ccd.calibrate_ccd_readout_time(
            self.tomo.parameters.exposure_time
        )
        time = (
            self.tomo.parameters.exposure_time
            + readout
            + self.tomo.parameters.latency_time
        )
        totalcontinuoustime = numberofscans * time
        trange = self.tomo.parameters.end_pos - self.tomo.parameters.start_pos
        waitingtime = (
            (360 / trange) * self.parameters.nwait
            + int(180 / trange)
        ) * time
        frequency = waitingtime + time
        totaltime = frequency * numberofscans
        transferrate = round(
            self.tomo.tomo_ccd.detector.proxy.saving_statistics[-1] / 1024 / 1024, 2
        )
        unlimitwaitingturns = (
            int(
                ((sizeofonescan / (transferrate / 1024)) / time)
                / (360 / trange)
            )
            + 2
        )
        unlimitwaitingtime = (
            (360 / trange) * unlimitwaitingturns
            + int(180 / trange)
        ) * time
        unlimitfrequency = unlimitwaitingtime + time

        self.in_pars["numberofscans"] = numberofscans
        self.in_pars["sizeofonescan"] = sizeofonescan
        self.in_pars["totalcontinuoustime"] = totalcontinuoustime
        self.in_pars["transferrate"] = transferrate
        self.in_pars["totaltime"] = totaltime
        self.in_pars["frequency"] = frequency
        self.in_pars["time"] = time
        self.in_pars["unlimitfrequency"] = unlimitfrequency
        self.in_pars["unlimitwaitingturns"] = unlimitwaitingturns

    def estimate_scan_time(self, motor, start_pos, end_pos, nb_points):
        """
        Estimates scan duration
        """

        tomo_tools = self.tomo_tools

        ref_scan_time = tomo_tools.estimate_ref_scan_duration()

        dark_scan_time = tomo_tools.estimate_dark_scan_duration()

        proj_scan_time = tomo_tools.continuous_scan_time(motor, start_pos)

        return ref_scan_time + dark_scan_time + proj_scan_time

    def prepare(self):
        """
        Calculates scan parameters and contructs acquisition chain corresponding to scan type
        """
        self.calculate_parameters()
        
        self.tomo.tomo_scan.calculate_parameters(
            self.tomo.parameters.start_pos,
            self.tomo.parameters.end_pos - self.tomo.parameters.start_pos,
            self.tomo.parameters.tomo_n,
            self.tomo.parameters.exposure_time,
            self.tomo.parameters.latency_time,
            self.parameters.ntomo,
        )

        self.tomo_tools.check_params()

        self.tomo.saving.check_lbs_free_space()

        est_time_scan = self.estimate_scan_time(
            self.tomo.tomo_musst.motor,
            self.tomo.parameters.start_pos,
            self.tomo.tomo_scan.in_pars["end_pos"],
            self.tomo.tomo_scan.in_pars["tomo_n"],
        )

        self.tomo.in_pars["estimated_time_scan"] = str(
            datetime.timedelta(seconds=est_time_scan)
        )

        self.tomo_musst.sync_motor()

    def projection_scan(
        self, start_pos, end_pos, nb_trig, title, scan_sequence=None, header={}
    ):
        """
        Pcotomo scan for one reference group.
        """
        log_info(self, "projection_scan() entering")

        # add image identification to the common image header
        header["image_key"] = "projection"

        # add meta data
        meta_data = self.tomo.meta_data.tomo_scan_info()
        scan_info = {}
        scan_info["scan"] = meta_data["technique"]["scan"]
        scan_info["scan"]["nb_tomo"] = self.parameters.ntomo
        scan_info["scan"]["nb_loop"] = self.parameters.nloop
        scan_info["scan"]["user_output"] = self.parameters.user_output
        scan_info["scan"]["start_turn"] = self.parameters.start_turn
        scan_info["scan"]["waiting_turns"] = self.parameters.nwait
        scan_info["scan"]["no_saving_between_tomo"] = self.parameters.noread

        scan_info["detector"] = meta_data["technique"]["detector"]
        scan_info = {"type": "tomo", "technique": scan_info}

        restore_list = (cleanup_axis.POS, cleanup_axis.VEL)

        # do not return to initial position when no error is detected
        if self.tomo.parameters.return_to_start_pos == False:
            restore_list = list()

        with error_cleanup(self.tomo.rotation_axis, restore_list=restore_list):

            with cleanup(
                self.tomo.rotation_axis,
                self.tomo.tomo_ccd.detector,
                restore_list=restore_list,
            ):

                proj_scan = None
                proj_scan = self.tomo.tomo_scan.continuous_scan(
                    start_pos,
                    end_pos,
                    nb_trig,
                    self.tomo.parameters.exposure_time,
                    self.tomo.tomo_scan.in_pars["time"],
                    self.parameters.ntomo,
                    self.parameters.nloop,
                    self.parameters.nwait,
                    self.parameters.noread,
                    self.parameters.start_turn,
                    self.tomo.tomo_scan.in_pars["sync_shut_angle"],
                    self.parameters.user_output,
                    self.tomo.parameters.trigger_type,
                    title,
                    scan_info,
                    save=True,
                    run=False,
                )

                # add common header preset
                header_preset = CommonHeaderPreset(self.tomo.tomo_ccd, header)
                proj_scan.add_preset(header_preset)

                # add shutter preset
                #shutter_preset = PcoTomoShutterPreset(
                #    self.tomo.shutter,
                #    self.tomo_musst,
                #    self.tomo.sequence_preset[0].mux,
                #)
                #proj_scan.add_preset(shutter_preset)

                # add to scan sequence when requested
                if scan_sequence != None:
                    scan_sequence.add(proj_scan)
                    self.tomo.list_proj_scans.append(proj_scan)

                if self.tomo.parameters.sinogram_active:
                    self.gscan = gevent.spawn(proj_scan.run)
                    while proj_scan.node is None:
                        gevent.sleep(0.1)
                    self.greemit = self.tomo.tomo_sino.create_data_reemitter(
                        proj_scan.node.db_name, scan_sequence.sequence
                    )
                    self.greemit.start()
                    try:
                        self.gscan.get()
                    except:
                        # stops scan and raises exception
                        self.gscan.kill()
                        self.greemit.stop()
                        raise
                    finally:
                        self.greemit.get()

                else:
                    # run the proj scan
                    proj_scan.run()

        log_info(self, "projection_scan() leaving")

    def scan_cleanup(self):
        """
        Restores all parameters that have been modified by the acquisition.
        """
        self.tomo_musst.set_timer_back()

    def run(self):
        """
        Runs pcotomo tomo acquisition
        """

        with cleanup(self.scan_cleanup):

            scan_info = self.tomo.meta_data.tomo_scan_info()

            if self.tomo.parameters.sinogram_active:
                if (
                    not self.tomo.parameters.dark_images_at_start
                    or not self.tomo.parameters.ref_images_at_start
                ):
                    log_warning(
                        self,
                        "Dark scan and reference scans at start are missing and are needed for sinogram. They will be activated",
                    )
                npixels = self.tomo.tomo_ccd.detector.image.width
                self.tomo.tomo_sino.configure_plot(
                    scan_info,
                    0,
                    npixels - 1,
                    npixels,
                    self.tomo.parameters.start_pos,
                    self.tomo.tomo_scan.in_pars["end_pos"],
                    self.tomo.tomo_scan.in_pars["tomo_n"],
                )

            seq = Sequence(title=self.tomo.sequence, scan_info=scan_info)

            if self.tomo.parameters.sinogram_active:
                npixels = self.tomo.tomo_ccd.detector.image.width
                self.tomo.tomo_sino.configure_data_reemission(
                    seq,
                    self.tomo.rotation_axis,
                    self.tomo.tomo_ccd.detector.roi_profiles.counters,
                    npixels,
                )

            with seq.sequence_context() as scan_seq:
                self.tomo.run_sequence(scan_seq)

            if self.tomo.parameters.sinogram_active:
                self.tomo.tomo_sino.save(self.tomo, seq.scan.scan_number)
