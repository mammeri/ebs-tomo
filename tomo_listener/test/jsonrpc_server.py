from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple

from jsonrpc import JSONRPCResponseManager, dispatcher


def scan_started(scan_number):
	print('scan {} started'.format(scan_number))


def scan_ended(scan_number):
	print('scan {} ended'.format(scan_number))


def sequence_started(saving_file, entry_name, scan_title, proposal_file, sample_file):
	print('sequence {}@{} started. Title is {}, proposal file {}, sample file {} '.format(entry_name, saving_file, scan_title, proposal_file, sample_file))


def sequence_ended(saving_file, entry_name, suceed):
	print('sequence {}@{} ended. Suceed: {}'.format(entry_name, saving_file, suceed))


@Request.application
def application(request):
    # Dispatcher is dictionary {<method_name>: callable}
    dispatcher["scan_started"] = scan_started
    dispatcher["scan_ended"] = scan_ended
    dispatcher["sequence_started"] = sequence_started
    dispatcher["sequence_ended"] = sequence_ended

    response = JSONRPCResponseManager.handle(
        request.data, dispatcher)
    return Response(response.json, mimetype='application/json')


if __name__ == '__main__':
    #run_simple('localhost', 4000, application)
    run_simple('lid19europa', 4000, application)
