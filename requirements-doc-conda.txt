# Conda requirement file
# Documentation requirements
sphinx
graphviz
pygments
markdown-inline-graphviz
markdown>=3.2
mkdocs>=1.1
mkdocs-material==5.5.14
mkdocs-material-extensions>=1.0
pymdown-extensions>=6.3